import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/profitrule/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/profitrule',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/profitrule/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/profitrule/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {  //修改状态
    return request({
        url: '/yjsh/profitrule/updateProfitByStauts',
        method: 'put',
        data: obj
    })
}
export function editObj(obj) {  //修改收益规则记录表包含佣金规则和分润规则
    return request({
        url: '/yjsh/profitrule',
        method: 'put',
        data: obj
    })
}

