import request from '@/router/axios'

export function storedrule(query) { //分页查询
    return request({
        url: '/yjsh/storedrule/page', 
        method: 'get',
        params: query
    })
}


export function putObj(obj) { //修改储值规则
    return request({
        url: '/yjsh/storedrule/updateStoredRule', 
        method: 'put',
        data: obj
    })
}


export function addObj(obj) { //新增储值规则
    return request({
        url: '/yjsh/storedrule', 
        method: 'post',
        data: obj
    })
}

export function getStoreDetail(query) { //储值明细列表
    return request({
        url: '/yjsh/storedrule/getStoreDetail', 
        method: 'get',
        params: query
    })
}

export function updateStatus(obj) { //更改业态类型状态
    return request({
        url: '/yjsh/storedrule/updateStatus', 
        method: 'put',
        data: obj
    })
}

