import request from '@/router/axios'


export function dataStatisticalByGroup(query) {  //数据中心——门店数据报表——按照组织架构统计
    return request({
        url: '/yjsh/dataCenter/dataStatisticalByGroup',
        method: 'get',
        params:query
    })
}

export function dataStatisticalByAdress(query) {  //数据中心——门店数据报表——按照区域统计
    return request({
        url: '/yjsh/dataCenter/dataStatisticalByAdress',
        method: 'get',
        params:query
    })
}


export function shopinfopage() {  //获取门店
    return request({
        url: '/yjsh/shopinfo/page',
        method: 'get',
    })
}

export function dataCenterShopInfo(query) {  //数据中心-门店数据报表
    return request({
        url: '/yjsh/dataCenter/dataCenterShopInfo',
        method: 'get',
        params:query
    })
}

export function dataStatisticalPayDetail(query) {  //数据中心-门店数据报表
    return request({
        url: '/yjsh/dataCenter/dataStatisticalPayDetail',
        method: 'get',
        params:query
    })
}


export function dataCenterShopInfotrend(query) {  //数据中心-门店数据报表 按组织架构/区域趋势图
    return request({
        url: '/yjsh/dataCenter/dataCenterShopInfo/trend',
        method: 'get',
        params: query
    })
}


export function dataStatisticalConsumeDetail(query) {  //数据中心——门店数据报表——储值卡消费概况
    return request({
        url: '/yjsh/dataCenter/dataStatisticalConsumeDetail',
        method: 'get',
        params: query
    })
}


export function dataStatisticalRechargeDetail(query) {  //数据中心——门店数据报表——储值卡充值概况
    return request({
        url: '/yjsh/dataCenter/dataStatisticalRechargeDetail',
        method: 'get',
        params: query
    })
}