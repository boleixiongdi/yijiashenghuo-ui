import request from '@/router/axios'


export function platformgoodscategorytree(query) {  //商品类目
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}

export function dataCenterGoodsInfosale(query) {  //数据中心-商品数据报表 门店销售明细
    return request({
        url: '/yjsh/dataCenter/dataCenterGoodsInfo/sale',
        method: 'get',
        params: query
    })
}

export function dataCenterGoodsInfo(query) {  //数据中心-商品数据报表
    return request({
        url: '/yjsh/dataCenter/dataCenterGoodsInfo', 
        method: 'get',
        params: query
    })
}

export function dataCenterGoodsInfotrend(query) {  //数据中心-商品数据报表 店门销售明细趋势
    return request({
        url: '/yjsh/dataCenter/dataCenterGoodsInfo/trend',
        method: 'get',
        params: query
    })
}

