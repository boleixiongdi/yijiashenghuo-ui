import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/merchantinfo/channel/page',
        method: 'get',
        params: query
    })
}
export function getFenrunList(query) {
    return request({
        url: '/yjsh/profitrule/page',
        method: 'get',
        params: query
    })
}
export function addObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/channel/add',
        method: 'post',
        data: obj
    })
}
export function editObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/channel/update',
        method: 'post',
        data: obj
    })
}
export function getObj(id) {
    return request({
        url: '/yjsh/merchantinfo/channel/' + id,
        method: 'get'
    })
}

export function getGoods(query) {      //查询供应商商品列表
    return request({
        url: '/yjsh/merchantinfo/channel/goods',
        method: 'get',
        params: query
    })
}

export function statusObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/channel/status',
        method: 'post',
        data: obj
    })
}
