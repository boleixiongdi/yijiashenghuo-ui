import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/payorder/ld/page',
        method: 'get',
        params: query
    })
}
export function detail(id) {
    return request({
        url: '/yjsh/payorder/ld/sub/' + id,
        method: 'get',
    })
}
export function addObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'put',
        data: obj
    })
}

export function refundorder(id) {  //退货订单
    return request({
        url: '/yjsh/refundorder/sub/' + id,
        method: 'get'
    })
}


export function refund(obj) {
    return request({
        url: '/yjsh/refundorder/refund',
        method: 'post',
        data: obj
    })
}