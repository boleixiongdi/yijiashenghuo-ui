import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/payorder/hk/page',
        method: 'get',
        params: query
    })
}

export function fuwuList(query) {
    return request({
        url: '/yjsh/shopperson/service/page',
        method: 'get',
        params: query
    })
}
export function detail(id) {
    return request({
        url: '/yjsh/payorder/hk/sub/' + id,
        method: 'get',
    })
}
export function addObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'post',
        data: obj
    })
}
export function paidanObj(obj) {  //派单
    return request({
        url: '/yjsh/payorder/hk/deploy',
        method: 'post',
        data: obj
    })
}
export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'put',
        data: obj
    })
}

export function refundorder(id) {  //退货订单
    return request({
        url: '/yjsh/refundorder/sub/' + id,
        method: 'get'
    })
}

export function refund(obj) {
    return request({
        url: '/yjsh/refundorder/refund',
        method: 'post',
        data: obj
    })
}


export function shoppersonservice(query) {//分页查询家政服务人员(merchantId status 必传)
    return request({
        url: '/yjsh/shopperson/service/page',
        method: 'get',
        params: query
    })
}


