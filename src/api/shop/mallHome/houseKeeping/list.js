import request from '@/router/axios'

export function mallfrontpageinfoHouseList(query) {  //家政首页列表
    return request({
        url: '/yjsh/mallfrontpageinfo/houseList',
        method: 'get',
        params: query
    })
}

export function mallfrontpageinfoUpdateEnable(obj) { //修改列表状态
    return request({
        url: '/yjsh/mallfrontpageinfo/updateEnable',
        method: 'post',
        data: obj
    })
}