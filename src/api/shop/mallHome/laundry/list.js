import request from '@/router/axios'

export function mallfrontpageinfoLaundryList(query) {  //洗衣首页列表
    return request({
        url: '/yjsh/mallfrontpageinfo/laundryList',
        method: 'get',
        params: query
    })
}

export function mallfrontpageinfoUpdateEnable(obj) { //修改列表状态
    return request({
        url: '/yjsh/mallfrontpageinfo/updateEnable',
        method: 'post',
        data: obj
    })
}