import request from '@/router/axios'

export function mallfrontpageinfo(query) {  //商城首页列表
    return request({
        url: '/yjsh/mallfrontpageinfo/list',
        method: 'get',
        params: query
    })
}

export function mallfrontpageconfig(query) {  //顶部中部banner区列表分页查询
    return request({
        url: '/yjsh/mallfrontpageconfig/page',
        method: 'get',
        params: query
    })
}

export function delMallfrontpageconfig(id) {  //删除顶部中部banner区
    return request({
        url: '/yjsh/mallfrontpageconfig/' + id,
        method: 'delete'
    })
}

export function getMallfrontpageconfig(id) { //查询顶部中部banner区详情
    return request({
        url: '/yjsh/mallfrontpageconfig/' + id,
        method: 'get'
    })
}

export function detailMallfrontpageconfig(query) { //获取区域配置详情（仅供banner、四宫格、底部商品区编辑使用！！！）
    return request({
        url: '/yjsh/mallfrontpageconfig/detail',
        method: 'get',
        params: query
    })
}

export function addMallfrontpageconfig(obj) { //新增
    return request({
        url: '/yjsh/mallfrontpageconfig',
        method: 'post',
        data: obj
    })
}

export function putMallfrontpageconfig(obj) { //编辑
    return request({
        url: '/yjsh/mallfrontpageconfig',
        method: 'put',
        data: obj
    })
}


export function putMallfrontpageinfo(obj) { //新增优惠券
    return request({
        url: '/yjsh/mallfrontpageinfo',
        method: 'put',
        data: obj
    })
}
export function mallfrontpageinfoUpdatePic(obj) { //修改优惠券 积分兑换区图片
    return request({
        url: '/yjsh/mallfrontpageinfo/updatePic',
        method: 'post',
        data: obj
    })
}


export function mallfrontpageinfoUpdateEnable(obj) { //修改列表状态
    return request({
        url: '/yjsh/mallfrontpageinfo/updateEnable',
        method: 'post',
        data: obj
    })
}


export function getMallfrontpageinfo(id) { //优惠券详情
    return request({
        url: '/yjsh/mallfrontpageinfo/' + id,
        method: 'get'
    })
}
