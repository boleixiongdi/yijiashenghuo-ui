import request from '@/router/axios'


export function dataStatistical(query) {
  return request({
    url: '/yjsh/dataCenter/dataStatistical',
    method: 'get',
    params: query
  })
}
