import request from '@/router/axios'

export function goodsattributefetchList(query) {
    return request({
        url: '/yjsh/goodsattribute/page',
        method: 'get',
        params: query
    })
}

export function platformgoodscategory(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/goodsattribute',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/goodsattribute' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/goodsattribute/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/goodsattribute',
        method: 'put',
        data: obj
    })
}

