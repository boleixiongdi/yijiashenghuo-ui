import request from '@/router/axios'

export function platformgoodscategoryfetchList(query) {
    return request({
        url: '/yjsh/platformgoodscategory/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/platformgoodscategory/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/platformgoodscategory',
        method: 'put',
        data: obj
    })
}


export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}