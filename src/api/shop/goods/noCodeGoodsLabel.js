import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/platformgoodslabel/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/platformgoodslabel',  //新增无码商品标签信息表
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodslabel' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/platformgoodslabel/' + id,
        method: 'delete',
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/platformgoodslabel',
        method: 'put',
        data: obj
    })
}