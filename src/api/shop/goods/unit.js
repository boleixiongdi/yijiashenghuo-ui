import request from '@/router/axios'

export function goodstockfetchList(query) {
    return request({
        url: '/yjsh/goodstock/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/goodstock',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/goodstock' + id,
        method: 'get'
    })
}

export function delObj(obj) {
    return request({
        url: '/yjsh/goodstock/updateByStatus',
        method: 'put',
        data: obj
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/goodstock/updateById',
        method: 'put',
        data: obj
    })
}