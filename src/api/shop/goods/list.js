import request from '@/router/axios'

export function platformgoodsinfofetchList(query) {
    return request({
        url: '/yjsh/platformgoodsinfo/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/platformgoodsinfo',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodsinfo' + id,
        method: 'get'
    })
}

export function delObj(id) {  //通过id删除平台商品信息表
    return request({
        url: '/yjsh/platformgoodsinfo/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/platformgoodsinfo',
        method: 'put',
        data: obj
    })
}

export function goodstockfetchList(query) {  //库存单位
    return axios({
      url: '/yjsh/goodstock/page',
      method: 'get',
      params: query
    })
  }

  export function goodstockfetchListAll(query) {  //库存单位
    return axios({
      url: '/yjsh/goodstock/getGoodstockList',
      method: 'get',
      params: query
    })
  }
  
export function platformgoodscategorytree(query) {  //商品类目
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}


export function goodstype(query) {  //商品类型
    return axios({
        url: '/yjsh/goodstype/page',
        method: 'get',
        params: query
    })
}

export function getTpyeId(TpyeId) {  //商品类型
    return axios({
        url: '/yjsh/goodstype/updateGoodsType/' + TpyeId,
        method: 'get',
    })
}


export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}


export function getPlatformShopInfo(query) {  //获取平台下所有的门店列表信息
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}

export function platformgoodsinfodetail(id) {  //商品详情
    return request({
        url: '/yjsh/platformgoodsinfo/detail/'+ id,
        method: 'get',
    })
}






export function changeSaleState(obj) {  //平台商品停售、在售
    return request({
        url: '/yjsh/platformgoodsinfo/changeSaleState',
        method: 'put',
        data: obj
    })
}

export function pushToShop(obj) {//平台商品推送
    return request({
        url: '/yjsh/platformgoodsinfo/pushToShop',
        method: 'post',
        data: obj
    })
}

export function doAudit(obj) {  //平台商品审核   auditState 审核状态：00 表示待提交 01 表示待审核 02 表示审核通过（正常状态） 03 表示已驳回
    return request({
        url: '/yjsh/platformgoodsinfo/doAudit',
        method: 'put',
        data: obj
    })
}


export function submitApprove(obj) {  //提交值审核
    return request({
        url: '/yjsh/platformgoodsinfo/submitApprove',
        method: 'put',
        data: obj
    })
}