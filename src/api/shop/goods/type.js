import request from '@/router/axios'

export function goodstypefetchList(query) {
    return request({
        url: '/yjsh/goodstype/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/goodstype',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/goodstype' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/goodstype/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/goodstype',
        method: 'put',
        data: obj
    })
}

export function updateGoodsgoodsTpyeId(goodsTpyeId) {  //修改商品类型
    return request({
        url: '/yjsh/goodstype/updateGoodsType/' + goodsTpyeId,
        method: 'get'
    })
}

export function addgoodstype(goodsTpyeId) {  //新增时商品类型
    return request({
        url: '/yjsh/goodstype/' + goodsTpyeId,
        method: 'get'
    })
}
