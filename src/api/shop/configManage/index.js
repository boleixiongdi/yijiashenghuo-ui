import request from '@/router/axios'

export function feedbackPage(query) {  //意见反馈
    return request({ 
        url: '/yjsh/feedback/page',
        method: 'get',
        params: query
    })
}
export function delObj(id) {
    return request({
        url: '/yjsh/feedback/' + id,
        method: 'delete'
    })
}


export function replyFeedback(obj) {
    return request({
        url: '/yjsh/feedback/replyFeedback',
        method: 'post',
        data: obj
    })
}



//退款原因
export function refundreasonPage(query) { 
    return request({ 
        url: '/yjsh/refundreason/page',
        method: 'get',
        params: query
    })
}

export function addRefundreason(obj) { //新增退款原因
    return request({
        url: '/yjsh/refundreason',
        method: 'post',
        data: obj
    })
}
export function putRefundreason(obj) { //修改退款原因
    return request({
        url: '/yjsh/refundreason',
        method: 'put',
        data: obj
    })
}
export function delRefundreason(id) {
    return request({
        url: '/yjsh/refundreason/' + id,
        method: 'delete'
    })
}







//消息通知
export function messagenoticePage(query) { 
    return request({ 
        url: '/yjsh/messagenotice/page',
        method: 'get',
        params: query
    })
}
export function delMessagenotice(id) {
    return request({
        url: '/yjsh/messagenotice/' + id,
        method: 'delete'
    })
}
export function recall(id) {   //撤回
    return request({ 
        url: '/yjsh/recall/' + id,
        method: 'get',
    })
}
export function addMessagenotice(obj) { //新增消息通知
    return request({
        url: '/yjsh/messagenotice',
        method: 'post',
        data: obj
    })
}
export function putMessagenotice(obj) { //修改消息通知
    return request({
        url: '/yjsh/messagenotice',
        method: 'put',
        data: obj
    })
}
