import request from '@/router/axios'

export function shoppersonfetchList(query) {
    return request({
        url: '/yjsh/shopperson/page',
        method: 'get',
        params: query
    })
}
export function getFenrunList(query) {
    return request({
        url: '/yjsh/profitrule/page',
        method: 'get',
        params: query
    })
}


export function addObj(obj) {
    return request({
        url: '/yjsh/shopperson',
        method: 'post',
        data: obj
    })
}
export function editObj(obj) {
    return request({
        url: '/yjsh/shopperson',
        method: 'put',
        data: obj
    })
}

export function statusObj(obj) {  //服务人状态
    return request({
        url: '/yjsh/shopperson/updateState',
        method: 'put',
        data: obj
    })
}

export function pageByyt(query) {  //根据业态查询门店
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}

export function getShopList(query) {  //新增服务人员时根据区域获取门店列表
    return request({
        url: '/yjsh/shopperson/getShopList',
        method: 'get',
        params: query
    })
}
export function getPlatformShopInfo(query) {  //获取平台下所有的门店列表信息
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}



