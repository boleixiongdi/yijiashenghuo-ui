import request from '@/router/axios'


export function getGoodsStockRecordPage(query) {  //门店库存列表
    return request({
        url: '/yjsh/goodsstockrecord/getGoodsStockRecordPage',
        method: 'get',
        params: query
    })
}

export function getStockRecordDetailList(query) {  //门店采购库存管理-门店库存流水、平台库存列表下的门店库存明细-门店库存流水
    return request({
        url: '/yjsh/goodsstockrecord/getStockRecordDetailList',
        method: 'get',
        params: query
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}

export function shopTree(obj) {  //返回门店下拉树
    return request({
        url: '/yjsh/goodsstockrecord/shopTree',
        method: 'post',
        data: obj
    })
}