import request from '@/router/axios'


export function platformOutStockList(query) {  //调拨出库
    return request({
        url: '/yjsh/outstock/platformOutStockList',
        method: 'get',
        params: query
    })
}

export function platformTransferOutStockConfirm(query) {  //调拨出库完成操作
    return request({
        url: '/yjsh/outstock/platformTransferOutStockConfirm',
        method: 'get',
        params: query
    })
}
export function updateShopPurchaseOutStockComplete(query) {  //门店采购 平台出库完成操作
    return request({
        url: '/yjsh/outstock/updateShopPurchaseOutStockComplete',
        method: 'post',
        data: query
    })
}

export function platformTransferOutStockDetail(query) {  //调拨出库详情
    return request({
        url: '/yjsh/outstock/platformTransferOutStockDetail',
        method: 'get',
        params: query
    })
}
export function platformTransferOutStockOperate(query) {  //调拨出库操作
    return request({
        url: '/yjsh/outstock/platformTransferOutStockOperate',
        method: 'post',
        data: query
    })
}
export function updateShopPurchaseOutStockOperate(query) {  //门店采购 平台出库出库操作
    return request({
        url: '/yjsh/outstock/updateShopPurchaseOutStockOperate',
        method: 'post',
        data: query
    })
}






export function platformToShopOutStockAdd(obj) {//新增平台主动出库到门店
    return request({
        url: '/yjsh/outstock/platformToShopOutStockAdd', 
        method: 'post',
        data: obj
    })
}

export function platformRefundOutStockList(query) {  //获取平台退货出库列表信息查询
    return request({
        url: '/yjsh/outstock/platformRefundOutStockList',
        method: 'get',
        params: query
    })
}

export function platformRefundOutStockDetail(query) {  //平台退货出库详情
    return request({
        url: '/yjsh/outstock/platformRefundOutStockDetail',
        method: 'get',
        params: query
    })
}
export function platformRefundOutStockOperate(obj) {//平台退货出库操作
    return request({
        url: '/yjsh/outstock/platformRefundOutStockOperate', 
        method: 'post',
        data: obj
    })
}
export function platformRefundOutStockConfirm(obj) {//平台退货出库完成操作
    return request({
        url: '/yjsh/outstock/platformRefundOutStockConfirm', 
        method: 'post',
        data: obj
    })
}

export function platformRefundOutStockAdd(obj) {//新增平台退货出库单
    return request({
        url: '/yjsh/outstock/platformRefundOutStockAdd', 
        method: 'post',
        data: obj
    })
}

export function getPlatformShopInfo(query) {  //平台下门店列表
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}





export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}

export function savePlatformPurchaseinfo(obj) {//新增平台采购单
    return request({
        url: '/yjsh/purchaseinfo/savePlatformPurchaseinfo', 
        method: 'post',
        data: obj
    })
}
export function platformGoodsInfoList(query) {  //根据门店获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/shopGoodsInfoList',
        method: 'get',
        params: query
    })
}
export function shopGoodsInfoPage(query) {  //2021-1-25修改根据门店获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/shopGoodsInfoPage',
        method: 'get',
        params: query
    })
}


export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}


export function rejectedreason(inOutType) {  //通过inOutType查询 00表示出库01表示入库
    return request({
        url: '/yjsh/rejectedreason/'+ inOutType,
        method: 'get',
    })
}

export function merchantinfosupplygoods(query) {  //根据供应商获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoList',
        method: 'get',
        params: query
    })
}

export function platformGoodsInfoPage(query) {  //2021-1-25修改 根据供应商获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoPage',
        method: 'get',
        params: query
    })
}

