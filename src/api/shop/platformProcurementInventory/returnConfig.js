import request from '@/router/axios'

export function rejectedreasontchList(query) {
    return request({
        url: '/yjsh/rejectedreason/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/rejectedreason',
        method: 'post',
        data: obj
    })
}
export function putObj(obj) {  //修改权益规则表状态
    return request({
        url: '/yjsh/rejectedreason',
        method: 'put',
        data: obj
    })
}