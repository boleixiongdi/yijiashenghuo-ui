import request from '@/router/axios'


export function platformPurchaseinfoList(query) {  //获取平台采购库存下的平台采购单列表及条件查询
    return request({
        url: '/yjsh/purchaseinfo/platform/platformPurchaseinfoList',
        method: 'get',
        params: query
    })
}
export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}

export function savePlatformPurchaseinfo(obj) {//新增平台采购单
    return request({
        url: '/yjsh/purchaseinfo/savePlatformPurchaseinfo', 
        method: 'post',
        data: obj
    })
}
export function platformGoodsInfoList(query) {  //根据平台类目获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoList',
        method: 'get',
        params: query
    })
}

export function platformGoodsInfoPage(query) {  //2021-1-25修改 根据平台类目获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoPage',
        method: 'get',
        params: query
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}


export function platformPurchaseDetail(purchaseInfoId) {  //获取平台采购库存下平台采购单详情
    return request({
        url: '/yjsh/purchaseinfo/platform/platformPurchaseDetail/' + purchaseInfoId,
        method: 'get',
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/purchaseinfo/deletePlatPurchase/' + id,
        method: 'delete'
    })
}