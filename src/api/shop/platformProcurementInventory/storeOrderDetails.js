import request from '@/router/axios'


export function shopPurchaseinfoList(query) {  //获取平台采购库存下的门店采购单列表及条件查询
    return request({
        url: '/yjsh/purchaseinfo/platform/shopPurchaseinfoList',
        method: 'get',
        params: query
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}

export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}

export function shopPurchaseDetail(purchaseInfoId) {  //获取平台采购库存下的门店采购单详情
    return request({
        url: '/yjsh/purchaseinfo/platform/shopPurchaseDetail/' + purchaseInfoId,
        method: 'get',
    })
}
