import request from '@/router/axios'


export function platformInstockList(query) {  //获取平台采购库存下的平台采购单列表及条件查询
    return request({
        url: '/yjsh/instock/platformInstockList',
        method: 'get',
        params: query
    })
}


export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}

export function savePlatformPurchaseinfo(obj) {//新增平台采购单
    return request({
        url: '/yjsh/purchaseinfo/savePlatformPurchaseinfo', 
        method: 'post',
        data: obj
    })
}
export function platformGoodsInfoList(query) {  //根据供应商获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoList',
        method: 'get',
        params: query
    })
}

export function platformGoodsInfoListForNewAddInstock(query) {  //获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoListForNewAddInstock',
        method: 'get',
        params: query
    })
}

export function platformGoodsInfoPageForNewAddInstock(query) {  //2021-1-25修改  获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoPageForNewAddInstock',
        method: 'get',
        params: query
    })
}


export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}


export function platformRefundInStockInfoDetail(obj) {  //平台采购库存-平台入库列表-门店退货入库单详情
    return request({
        url: '/yjsh/instock/platformRefundInStockInfoDetail',
        method: 'post',
        data: obj
    })
}


export function platformPurchaseInStockInfoDetail(obj) {  //平台采购库存-平台入库列表-采购入库单详情
    return request({
        url: '/yjsh/instock/platformPurchaseInStockInfoDetail',
        method: 'post',
        data: obj
    })
}


export function platformOtherInStockInfoDetail(obj) {  //平台采购库存-平台入库列表-其它入库单详情
    return request({
        url: '/yjsh/instock/platformOtherInStockInfoDetail',
        method: 'post',
        data: obj
    })
}

export function platFormInstockGoodsNewAdd(obj) {  //新建平台入库单
    return request({
        url: '/yjsh/instock/platFormInstockGoodsNewAdd',
        method: 'put',
        data: obj
    })
}

export function instockPlatformGoods(obj) {  //平台操作入库
    return request({
        url: '/yjsh/instock/instockPlatformGoods',
        method: 'put',
        data: obj
    })
}

export function alreadyInstockPlatform(obj) {  //平台操作入库已完成
    return request({
        url: '/yjsh/instock/alreadyInstockPlatform',
        method: 'put',
        data: obj
    })
}


export function shopPurchaseRefundPlatformInStockConfirm(inStockId) {  //门店采购退货平台入库单 instock 入库单id
    return request({
        url: '/yjsh/instock/shopPurchaseRefundPlatformInStockConfirm/'+inStockId,
        method: 'get',
    })
}



export function rejectedreason(inOutType) {  //通过inOutType查询 00表示出库01表示入库
    return request({
        url: '/yjsh/rejectedreason/'+ inOutType,
        method: 'get',
    })
}
