import request from '@/router/axios'


export function getPlatformStockRecordPage(query) {  //平台库存列表
    return request({
        url: '/yjsh/goodsstockrecord/getPlatformStockRecordPage',
        method: 'get',
        params: query
    })
}

export function getPlatformGoodsStockList(query) {  //平台库存流水
    return request({
        url: '/yjsh/goodsstockrecord/getPlatformGoodsStockList',
        method: 'get',
        params: query
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}