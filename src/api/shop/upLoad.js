import request from '@/router/axios'

export function upLoad(obj) {
    const files = { file: obj }
    const formData = new FormData();
    Object.keys(files).forEach((key) => {
        formData.append(key, files[key]);
    });
    return request({
        url: '/admin/sys-file/upload',
        method: 'post',
        data: formData
    })
}

export function handleImg(url) {
    return request({
        url: url,
        method: 'get',
        responseType: 'blob'
    })
}

