import request from '@/router/axios'


export function workorderinfointegralpage(query) { //积分工单信息分页查询
    return request({
        url: '/yjsh/workorderinfo/integral/page',
        method: 'get',
        params: query
    })
}


export function workorderinforecord(query) { //工单记录查询
    return request({
        url: '/yjsh/workorderinfo/integral/record',
        method: 'get',
        params: query
    })
}


export function workorderinforeDeploy(query) { //工单重新派单
    return request({
        url: '/yjsh/workorderinfo/reDeploy',
        method: 'get',
        params: query
    })
}



export function workorderinfodeploy(query) { //工单派单
    return request({
        url: '/yjsh/workorderinfo/deploy',
        method: 'get',
        params: query
    })
}


export function workorderinfochangeStauts(query) { //积分工单修改状态
    return request({
        url: '/yjsh/workorderinfo/changeStauts',
        method: 'post',
        data: query
    })
}


