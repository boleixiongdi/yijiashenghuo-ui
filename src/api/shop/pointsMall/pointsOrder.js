import request from '@/router/axios'


export function integralpage(query) { //积分订单
    return request({
        url: '/yjsh/payorder/integral/page',
        method: 'get',
        params: query
    })
}


export function integralsub(id) { //积分订单详情
    return request({
        url: '/yjsh/payorder/integral/sub/' + id,
        method: 'get',
    })
}