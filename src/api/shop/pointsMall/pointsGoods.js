import request from '@/router/axios'


export function integralPage(query) { //获取积分库存商品信息列表
    return request({
        url: '/yjsh/platformgoodsinfo/integralPage',
        method: 'get',
        params: query
    })
}

export function changeShelf(query) { //积分商品上架、下架
    return request({
        url: '/yjsh/platformgoodsinfo/changeShelf',
        method: 'put',
        data: query
    })
}

export function updateIntegralInfo(query) { //修改平台积分商品信息表
    return request({
        url: '/yjsh/platformgoodsinfo/updateIntegralInfo',
        method: 'put',
        data: query
    })
}

export function addIntegralGoods(query) { //新增积分商品
    return request({
        url: '/yjsh/platformgoodsinfo/addIntegralGoods',
        method: 'post',
        data: query
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/platformgoodsinfo/' + id,
        method: 'delete'
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/platformgoodsinfo/' + id,
        method: 'get'
    })
}
export function goodstockfetchList(query) { //库存单位
    return axios({
        url: '/yjsh/goodstock/page',
        method: 'get',
        params: query
    })
}

export function platformgoodscategorytree(query) { //商品类目
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}


export function goodstype(query) { //商品类型
    return axios({
        url: '/yjsh/goodstype/page',
        method: 'get',
        params: query
    })
}

export function getTpyeId(TpyeId) { //商品类型
    return axios({
        url: '/yjsh/goodstype/updateGoodsType/' + TpyeId,
        method: 'get',
    })
}


export function getPlatformShopInfo(query) { //获取平台下所有的门店列表信息
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}

export function getPersonGradeInfoList(query) { //获取会员等级列表信息
    return request({
        url: '/yjsh/platformgoodsinfo/getPersonGradeInfoList',
        method: 'get',
        params: query
    })
}
export function detail(id) { //编辑平台商品获取的商品详情信息id
    return request({
        url: '/yjsh/platformgoodsinfo/' + id,
        method: 'get',

    })
}