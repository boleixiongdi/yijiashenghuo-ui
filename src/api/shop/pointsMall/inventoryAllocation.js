import request from '@/router/axios'


export function getIntgralGoodsList(query) {  //获取积分库存商品信息列表
    return request({
        url: '/yjsh/platformgoodsinfo/getIntgralGoodsList',
        method: 'get',
        params: query
    })
}


export function getIntgralGoodsUnitList(query) {  //根据商品id获取积分商城下面的积分库存分配列表
    return request({
        url: '/yjsh/platformgoodsinfo/getIntgralGoodsUnitList',
        method: 'get',
        params: query
    })
}

export function doStockDistribution(query) {  //平台积分商品库存分配
    return request({
        url: '/yjsh/platformgoodsinfo/doStockDistribution',
        method: 'put',
        data: query
    })
}
