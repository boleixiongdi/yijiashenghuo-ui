import request from '@/router/axios'

export function appApplyRefund(obj) { //同意退款
    return request({
        url: '/yjsh/subrefundorder/appApplyRefund',
        method: 'put',
        data: obj
    })
}

export function rejectRefund(obj) { //拒绝退款
    return request({
        url: '/yjsh/subrefundorder/rejectRefund',
        method: 'put',
        data: obj
    })
}