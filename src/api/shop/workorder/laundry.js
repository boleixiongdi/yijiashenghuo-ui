import request from '@/router/axios'

export function workorderinfoLaundryPage(query) { //工单列表
    return request({
        url: '/yjsh/workorderinfoLaundry/page',
        method: 'get',
        params: query
    })
}


export function workorderList(query) {  //工单记录
    return request({
        url: '/yjsh/workorderinfoMall/workorderList',
        method: 'get',
        params: query
    })
}


export function workorderinfoLaundryQueryLaundryPerson(query) { //洗衣工单派单-查询接单服务人员列表
    return request({
        url: '/yjsh/workorderinfoLaundry/queryLaundryPerson',
        method: 'get',
        params: query
    })
}
export function workorderinfoLaundryDispatchLaundry(obj) { //洗衣工单派单
    return request({
        url: '/yjsh/workorderinfoLaundry/dispatchLaundry',
        method: 'put',
        data: obj
    })
}

export function workorderinfoLaundryChangeLaundryStauts(obj) { // 00 待派单  01 带取衣  02 门店收衣  03 待工厂取衣  04 洗涤中  05 待送衣  06 自提取衣  07 送衣中  08 已完成  09 退款审核 10 退款关闭  11 待接单
    return request({
        url: '/yjsh/workorderinfoLaundry/changeLaundryStauts',
        method: 'put',
        data: obj
    })
}
