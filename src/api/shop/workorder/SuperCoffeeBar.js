import request from '@/router/axios'

export function workorderinfoMallPage(query) { //工单列表
    return request({
        url: '/yjsh/workorderinfoMall/page',
        method: 'get',
        params: query
    })
}

export function workorderList(query) {  //工单记录
    return request({
        url: '/yjsh/workorderinfoMall/workorderList',
        method: 'get',
        params: query
    })
}

export function workorderinfoMallQueryWallPerson(query) { //商超和啡吧工单派单-查询接单服务人员列表
    return request({
        url: '/yjsh/workorderinfoMall/queryWallPerson',
        method: 'get',
        params: query
    })
}
export function workorderinfoMallDispatchWall(obj) { //商超和啡吧工单派单
    return request({
        url: '/yjsh/workorderinfoMall/dispatchWall',
        method: 'put',
        data: obj
    })
}

export function workorderinfoMallPickUpWall(obj) { //00待派单  01待自提  02待取货  03服务中  04已完成  05退款审核  06退款关闭 07待接单
    return request({
        url: '/yjsh/workorderinfoMall/pickUpWall',
        method: 'put',
        data: obj
    })
}

