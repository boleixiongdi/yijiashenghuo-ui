import request from '@/router/axios'

export function workorderinfoHousePage(query) { //工单列表
    return request({
        url: '/yjsh/workorderinfoHouse/page',
        method: 'get',
        params: query
    })
}


export function workorderList(query) {  //工单记录
    return request({
        url: '/yjsh/workorderinfoMall/workorderList',
        method: 'get',
        params: query
    })
}


export function workorderinfoHouseQueryHousePerson(query) { //家政工单派单-查询接单服务人员列表
    return request({
        url: '/yjsh/workorderinfoHouse/queryHousePerson',          
        method: 'get',
        params: query
    })
}
export function workorderinfoMallDispatchWall(obj) { //家政工单派单
    return request({
        url: '/yjsh/workorderinfoHouse/dispatchWall',
        method: 'put',
        data: obj
    })
}

export function workorderinfoHouseChangeHouseStauts(obj) { //00 待派单  01 待服务 02 服务中  03 已完成  04 退款审核  05 退款关闭  06 待接单
    return request({
        url: '/yjsh/workorderinfoHouse/changeHouseStauts',
        method: 'put',
        data: obj
    })
}
