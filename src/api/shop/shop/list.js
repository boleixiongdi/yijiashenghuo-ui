import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/shopinfo/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/shopinfo',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/shopinfo/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/shopinfo/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/shopinfo',
        method: 'put',
        data: obj
    })
}

export function changeOnlineStatus(obj) {
    return request({
        url: '/yjsh/shopinfo/changeOnlineStatus',
        method: 'put',
        data: obj
    })
}


export function supplierMerchant(query) { //供应商列表
    return request({
        url: '/yjsh/platformgoodscategory/page',
        method: 'get',
        params: query
    })
}

export function platformgoodscategory(query) { //商品类目
    return request({
        url: '/yjsh/platformgoodscategory/page',
        method: 'get',
        params: query
    })
}

export function typeIdListFn(query) { //商品类型
    return request({
        url: '/yjsh/platformgoodscategory/page',
        method: 'get',
        params: query
    })
}

export function pageByAddress(query) { //通过地理信息分页查询门店
    return request({
        url: '/yjsh/shopinfo/pageByAddress',
        method: 'get',
        params: query
    })
}
