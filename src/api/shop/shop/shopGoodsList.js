import request from '@/router/axios'

export function getShopGoodsInfoList(query) {  //门店商品关联关系维护——平台推送门店商品列表
    return request({
        url: '/yjsh/platformgoodsinfo/getShopGoodsInfoList',
        method: 'get',
        params: query
    })
}

export function pustShopGoodsList(query) {      //门店商品关联关系维护——获取平台未推送到门店的商品列表
    return request({
        url: '/yjsh/platformgoodsinfo/pustShopGoodsList',
        method: 'get',
        params: query
    })
}
export function pustShopGoodsListPage(query) {      //门店商品关联关系维护——获取平台未推送到门店的商品列表
    return request({
        url: '/yjsh/platformgoodsinfo/pustShopGoodsPage',
        method: 'get',
        params: query
    })
}
export function pushGoodsToShop(obj) {   //平台商品推送
    return request({
        url: '/yjsh/platformgoodsinfo/pushGoodsToShop',
        method: 'post',
        data: obj
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}