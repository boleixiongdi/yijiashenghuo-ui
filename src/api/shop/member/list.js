import request from '@/router/axios'

export function fetchList(query) {  //会员基本信息列表分页查询
    return request({
        url: '/yjsh/personinfo/page',
        method: 'get',
        params: query
    })
}
export function personBasicInfoList(query) {  //通过会员id查询基本信息
    return request({
        url: '/yjsh/personinfo/personBasicInfoList',
        method: 'get',
        params: query
    })
}

export function storedDetailedList(query) {  //通过会员id储值明细
    return request({
        url: '/yjsh/personinfo/storedDetailedList',
        method: 'get',
        params: query
    })
}


export function integralDetailedLists(query) {  //通过会员id积分兑换订单列表信息
    return request({
        url: '/yjsh/personinfo/integralDetailedLists',
        method: 'get',
        params: query
    })
}


export function integralDetailedList(query) {  //通过会员id查询积分明细
    return request({
        url: '/yjsh/personinfo/integralDetailedList',
        method: 'get',
        params: query
    })
}


export function couponRecList(obj) {  //通过会员id获取用户领取的优惠券信息
    return request({
        url: '/yjsh/personinfo/couponRecList',
        method: 'post',
        data: obj
    })
}


export function integralDetailedUpdate(query) {  //会员积分调整
    return request({
        url: '/yjsh/personinfo/integralDetailedUpdate',
        method: 'get',
        params: query
    })
}

export function updatePhone(obj) {  //更改手机号
    return request({
        url: '/yjsh/personinfo/updatePhone',
        method: 'post',
        data: obj
    })
}
export function updateStoredbyPhone(obj) {  //更改储值密码
    return request({
        url: '/yjsh/personinfo/updateStoredbyPhone',
        method: 'post',
        data: obj
    })
}
export function sendMessage(obj) {  //发送短信验证码
    return request({
        url: '/yjsh/personinfo/sendMessage',
        method: 'post',
        data: obj
    })
}

