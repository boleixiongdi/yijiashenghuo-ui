import request from '@/router/axios'

export function equityrulefetchList(query) {
    return request({
        url: '/yjsh/equityrule/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/equityrule',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/equityrule/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/equityrule/' + id,
        method: 'delete',
    })
}

export function updateById(obj) {  //修改权益规则表状态
    return request({
        url: '/yjsh/equityrule/updateById',
        method: 'put',
        data: obj
    })
}