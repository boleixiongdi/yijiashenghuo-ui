import request from '@/router/axios'

export function persongradefetchList(query) {
    return request({
        url: '/yjsh/persongrade/page',
        method: 'get',
        params: query
    })
}

export function getEquityRuleList(query) {
    return request({
        url: '/yjsh/persongrade/getEquityRuleList',
        method: 'get',
        params: query
    })
}
export function addObj(obj) {
    return request({
        url: '/yjsh/persongrade',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/persongrade/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/persongrade/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/yjsh/persongrade',
        method: 'put',
        data: obj
    })
}
export function updateStatusById(obj) {
    return request({
        url: '/yjsh/persongrade/updateStatusById',
        method: 'put',
        data: obj
    })
}