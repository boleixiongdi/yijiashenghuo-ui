import request from '@/router/axios'

export function integralrulefetchList(query) {
    return request({
        url: '/yjsh/integralrule/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/integralrule',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/yjsh/integralrule/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/integralrule/' + id,
        method: 'delete',
    })
}

export function updateById(obj) {  //修改权益规则表状态
    return request({
        url: '/yjsh/integralrule/updateById',
        method: 'put',
        data: obj
    })
}

export function updateByIsEnabled(obj) {  //修改权益规则表
    return request({
        url: '/yjsh/integralrule/updateByIsEnabled',
        method: 'put',
        data: obj
    })
}