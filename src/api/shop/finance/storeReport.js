import request from '@/router/axios'

export function shopCheck(query) {//门店对账报表分页查询
    return request({
        url: '/yjsh/shopCheck/page',
        method: 'get',
        params: query
    })
}

export function shopsale(query) {  //门店对账报表门店销售明细分页查询 shopId必传
    return request({
        url: '/yjsh/shopCheck/shop/sale',
        method: 'get',
        params: query
    })
}

export function shopCheckcharge(query) {  //门店对账报表门店充值明细分页查询 shopId必传
    return request({
        url: '/yjsh/shopCheck/shop/charge',
        method: 'get',
        params: query
    })
}

export function shopstock(query) {  //门店对账报表门店出入单明细分页查询 shopId必传
    return request({
        url: '/yjsh/shopCheck/shop/stock',
        method: 'get',
        params: query
    })
}



