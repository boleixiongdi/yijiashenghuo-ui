import request from '@/router/axios'

export function merchantinfofetchList(query) {  //供应商分页查询
    return request({
        url: '/yjsh/merchantinfo/supply/list',
        method: 'get',
        params: query
    })
}


export function platformgoodscategorytree(query) {  //商品类目
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}




export function getSupplierPurchaseDtail(query) {  //供应商对账报表 采购数据对账明细
    return request({
        url: '/yjsh/distributioninfo/getSupplierPurchaseDtail',
        method: 'get',
        params: query
    })
}

export function getSupplierOutStockDetail(query) {  //供应商对账报表 退货数据对账明细
    return request({
        url: '/yjsh/distributioninfo/getSupplierOutStockDetail',
        method: 'get',
        params: query
    })
}

export function getDistributeListBySupplierId(query) {  //供应商对账报表 下面的获取配送单明细
    return request({
        url: '/yjsh/distributioninfo/getDistributeListBySupplierId',
        method: 'get',
        params: query
    })
}
export function getSupplierPurchaseRejected(query) {  //供应商对账报表 下面的采购退货明细
    return request({
        url: '/yjsh/distributioninfo/getSupplierPurchaseRejected',
        method: 'get',
        params: query
    })
}


