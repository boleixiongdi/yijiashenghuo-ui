import request from '@/router/axios'
//销售数据明细
export function getSalesOutStockDetail(query) {  //销售数据明细
    return request({
        url: '/yjsh/distributioninfo/getSalesOutStockDetail',
        method: 'get',
        params: query
    })
}