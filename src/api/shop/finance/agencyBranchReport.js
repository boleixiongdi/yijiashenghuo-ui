import request from '@/router/axios'

export function agentBranchReport(query) {//代理商/分公司报表
    return request({
        url: '/yjsh/companyReport/agentBranchReport/page',
        method: 'get',
        params: query
    })
}

export function company(query) {  //分公司/代理商明细分页 代理商merchantId 必传
    return request({
        url: '/yjsh/companyReport/company/page',
        method: 'get',
        params: query
    })
}

export function shopsale(query) {  //门店销售明细分页查询 shopId 必传
    return request({
        url: '/yjsh/companyReport/shopsale/page',
        method: 'get',
        params: query
    })
}


