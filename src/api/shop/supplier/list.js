import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/yjsh/merchantinfo/supply/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/supply/add',
        method: 'post',
        data: obj
    })
}
export function editObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/supply/update',
        method: 'post',
        data: obj
    })
}
export function getObj(id) {
    return request({
        url: '/yjsh/merchantinfo/supply/' + id,
        method: 'get'
    })
}

export function getGoods(query) {      //查询供应商商品列表
    return request({
        url: '/yjsh/merchantinfo/supply/goods',
        method: 'get',
        params: query
    })
}

export function statusObj(obj) {
    return request({
        url: '/yjsh/merchantinfo/supply/status',
        method: 'post',
        data: obj
    })
}

export function platformgoodscategorytree(query) {  //类目树
    return request({
        url: '/yjsh/platformgoodscategory/tree',
        method: 'get',
        params: query
    })
}

export function platformGoodsInfoList(query) {  //根据供应商获取平台商品
    return request({
        url: '/yjsh/purchaseinfo/platformGoodsInfoList',
        method: 'get',
        params: query
    })
}

export function pustMerchantGoodsList(query) {  //供应商商品关联关系维护——获取未推送供应商商品列表
    return request({
        url: '/yjsh/platformgoodsinfo/pustMerchantGoodsList',
        method: 'get',
        params: query
    })
}
export function pustMerchantGoodsPage(query) {  //供应商商品关联关系维护——获取未推送供应商商品列表
    return request({
        url: '/yjsh/platformgoodsinfo/pustMerchantGoodsPage',
        method: 'get',
        params: query
    })
}
export function saveBatch(obj) {   //批量新增商品供应商对应关系
    return request({
        url: '/yjsh/goodssupliermapper/saveBatch',
        method: 'post',
        data: obj
    })
}
