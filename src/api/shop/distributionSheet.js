import request from '@/router/axios'


export function deliveryinfofetchList(query) {
  return request({
    url: '/yjsh/deliveryinfo/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/yjsh/deliveryinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/yjsh/deliveryinfo/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/yjsh/deliveryinfo/' + id,
    method: 'delete'
  }) 
}

export function putObj(obj) {
  return request({
    url: '/yjsh/deliveryinfo',
    method: 'put',
    data: obj
  })
}


