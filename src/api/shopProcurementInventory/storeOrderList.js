
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/yjsh/purchaseinfo/shopPurchaseinfoList',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/yjsh/purchaseinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/yjsh/purchaseinfo/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/yjsh/purchaseinfo/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/purchaseinfo',
    method: 'put',
    data: obj
  })
}
