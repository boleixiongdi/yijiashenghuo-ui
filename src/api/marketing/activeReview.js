
import request from '@/router/axios'

export function couponinfopage(query) {
    return request({
        url: '/yjsh/couponinfo/page',
        method: 'get',
        params: query
    })
}


export function doAudit(obj) {  //审核
    return request({
        url: '/yjsh/couponinfo/doAudit',
        method: 'put',
        data: obj
    })
}