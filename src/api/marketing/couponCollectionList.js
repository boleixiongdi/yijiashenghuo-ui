
import request from '@/router/axios'

export function couponrecpage(query) {
    return request({
        url: '/yjsh/couponrec/page',
        method: 'get',
        params: query
    })
}

export function getCountInfo(obj) {
    return request({
        url: '/yjsh/couponinfo/getCountInfo',  //查询优惠券数量
        method: 'post',
        data: obj
    })
    
}
export function couponrecdetail(query) { //获取优惠券领券详情
    return request({
        url: '/yjsh/couponrec/detail',
        method: 'get',
        params: query
    })
}

