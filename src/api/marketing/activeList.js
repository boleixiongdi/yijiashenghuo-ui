
import request from '@/router/axios'

export function couponinfopage(query) {
    return request({
        url: '/yjsh/couponinfo/page',
        method: 'get',
        params: query
    })
}

export function delObj(id) {
    return request({
        url: '/yjsh/couponinfo/' + id,
        method: 'delete'
    })
}


export function doAudit(obj) {
    return request({
        url: '/yjsh/couponinfo/doAudit',
        method: 'put',
        data: obj
    })
}

export function addcouponinfo(obj) {  //新增优惠券
    return request({
        url: '/yjsh/couponinfo',
        method: 'post',
        data: obj
    })
}

export function changeActStatus(obj) {  //更改状态
    return request({
        url: '/yjsh/couponinfo/changeActStatus',
        method: 'put',
        data: obj
    })
}


export function persongrade() {  //等级会员
    return request({
        url: '/yjsh/persongrade/page',
        method: 'get',
    })
}


export function pageByyt(query) {  //通过业务类型分页查询查询门店
    return request({
        url: '/yjsh/platformgoodsinfo/getPlatformShopInfo',
        method: 'get',
        params: query
    })
}
export function pageByShopId(query) {  //通过shopId分页查询
    return request({
        url: '/yjsh/shopgoodsinfo/pageByShopIdList',
        method: 'get',
        params: query
    })
}

export function couponInfoDetail(obj) {  //优惠券详情
    return request({
        url: '/yjsh/couponinfo/couponInfoDetail',
        method: 'post',
        data: obj
    })
}
export function putObj(obj) {  //修改优惠券表
    return request({
        url: '/yjsh/couponinfo',
        method: 'put',
        data: obj
    })
}

export function goodstockfetchList(query) {  //库存单位
    return axios({
      url: '/yjsh/goodstock/page',
      method: 'get',
      params: query
    })
  }
