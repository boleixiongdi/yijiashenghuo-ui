import { getStore, removeStore, setStore } from '@/util/store'

const common = {

    state: {
        //商品管理
        goodsBusiness: getStore({ name: 'goodsBusiness' }) || '02',
        goodsTab: getStore({ name: 'goodsTab' }) || '02',
        indexTab: getStore({ name: 'indexTab' }) || '01',
        
        //平台入库列表
        platformStockInList: getStore({ name: 'platformStockInList' }) || '1',
        //平台出库列表
        platformStockOutList: getStore({ name: 'platformStockOutList' }) || '1',

        memberInfo: getStore({ name: 'memberInfo' }) || {},

        // 业态
        businessTypeList: [
            {
                label: "商超",
                value: '02'
            },
            {
                label: "咖吧",
                value: "05"
            },
            {
                label: "洗衣",
                value: "03"
            },
            {
                label: "家政",
                value: "04"
            }
        ]
    },
    actions: {},
    mutations: {
        SET_GOODSBUSINESS: (state, goodsBusiness) => {
            state.goodsBusiness = goodsBusiness
            setStore({
                name: 'goodsBusiness',
                content: state.goodsBusiness,
                type: 'session'
            })
        },
        SET_GOODSTAB: (state, goodsTab) => {
            state.goodsTab = goodsTab
            setStore({
                name: 'goodsTab',
                content: state.goodsTab,
                type: 'session'
            })
        },

        SET_PLATFORMSTOCKINLIST: (state, platformStockInList) => {
            state.platformStockInList = platformStockInList
            setStore({
                name: 'platformStockInList',
                content: state.platformStockInList,
                type: 'session'
            })
        },
        SET_PLATFORMSTOCKOUTLIST: (state, platformStockOutList) => {
            state.platformStockOutList = platformStockOutList
            setStore({
                name: 'platformStockOutList',
                content: state.platformStockOutList,
                type: 'session'
            })
        },
        SET_MEMBERINFO: (state, memberInfo) => {
            state.memberInfo = memberInfo
            setStore({
                name: 'memberInfo',
                content: state.memberInfo,
                type: 'session'
            })
        },


        SET_INDEXTAB: (state, indexTab) => {  //首页tab
            state.indexTab = indexTab
            setStore({
                name: 'indexTab',
                content: state.indexTab,
                type: 'session'
            })
        },
        SET_BUSINESSTYPELIST :(state, businessTypeList)=>{
            state.businessTypeList = businessTypeList
        }
    }
}
export default common
