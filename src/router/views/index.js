import Layout from '@/page/index/'
export default [{
  path: '/wel',
  component: Layout,
  redirect: '/wel/index',
  children: [{
    path: 'index',
    name: '首页',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/page/wel'),
  }, {
    path: '/wel/mallHome/shangChao/banner',
    name: '列表',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/views/mallHome/shangChao/banner/index.vue')
  }, {
    path: '/wel/mallHome/shangChao/banner/addBanner',
    name: '',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/views/mallHome/shangChao/banner/addBanner/index.vue')
  }, {
    path: '/wel/mallHome/shangChao/coupon',
    name: '优惠券区',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/views/mallHome/shangChao/coupon/index.vue')
  }, {
    path: '/wel/mallHome/shangChao/pointsExchange',
    name: '积分兑换区',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/views/mallHome/shangChao/pointsExchange/index.vue')
  }]
}, {
  path: '/info',
  component: Layout,
  redirect: '/info/index',
  children: [{
    path: 'index',
    name: '个人信息',
    component: () =>
      import(/* webpackChunkName: "page" */ '@/views/admin/user/info')
  }]
}, {
  path: '/activti',
  component: Layout,
  redirect: '/activti/detail',
  children: [{
    path: 'detail/:id',
    component: () =>
      import(/* webpackChunkName: "views" */ '@/views/activiti/detail')
  }]

}]
