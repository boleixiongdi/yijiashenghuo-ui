import Layout from '@/page/index/'

export default [{
    path: '/order/SuperCoffeeBar',
    component: Layout,
    redirect: '/order/SuperCoffeeBar/index',
    children: [{
        path: '/order/SuperCoffeeBar/details',
        name: '订单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/order/SuperCoffeeBar/details/index.vue')
    }]
}, {
    path: '/order/housekeeping',
    component: Layout,
    redirect: '/order/housekeeping/index',
    children: [{
        path: '/order/housekeeping/details',
        name: '订单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/order/housekeeping/details/index.vue')
    }]
}, {
    path: '/order/laundry',
    component: Layout,
    redirect: '/order/laundry/index',
    children: [{
        path: '/order/laundry/details',
        name: '订单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/order/laundry/details/index.vue')
    }],

}, {
    path: '/goods/list',
    component: Layout,
    redirect: '/goods/list/index',
    children: [{
        path: '/goods/list/addGoods',
        name: '商品新增/编辑',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/goods/list/addGoods/index.vue')
    }]
}, {
    path: '/member/list',
    component: Layout,
    redirect: '/member/list/index',
    children: [{
        path: '/member/list/details',
        name: '会员详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/member/list/details/index.vue')
    }]
}, {
    path: '/member/level',
    component: Layout,
    redirect: '/member/level/index',
    children: [{
        path: '/member/level/addLevel',
        name: '新增会员等级',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/member/level/addLevel/index.vue')
    }]
}, {
    path: '/shop',
    component: Layout,
    redirect: '/shop/index',
    children: [{
        path: '/shop/addShop',
        name: '新增门店',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shop/addShop/index.vue')
    }]
},{
    path: '/shop',
    component: Layout,
    redirect: '/shop/index',
    children: [{
        path: '/shop/shopGoodsList',
        name: '门店商品列表',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shop/shopGoodsList/index.vue')
    }]
}, {
    path: '/pointsMall',
    component: Layout,
    redirect: '/pointsMall/pointsGoods/index',
    children: [{
        path: '/pointsMall/pointsGoods/addGoods',
        name: '积分商品列表/新增商品',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/pointsMall/pointsGoods/addGoods/index.vue')
    }]
}, {
    path: '/platformProcurementInventory/platformOrderList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformOrderList/index',
    children: [{
        path: '/platformProcurementInventory/platformOrderList/addOrder',
        name: '新增采购单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformOrderList/list_table/addOrder/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockInList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockInList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockInList/storeRuturnDetails',
        name: '门店退货入库详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockInList/storeRuturnDetails/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockInList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockInList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockInList/purchaseStockDetails',
        name: '采购入库单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockInList/purchaseStockDetails/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockInList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockInList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockInList/otherStockDetails',
        name: '其他入库单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockInList/otherStockDetails/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/transferStockOutDetails',
        name: '调拨出库单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/transferStockOutDetails/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/transferStockOutOperation',
        name: '调拨出库操作',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/transferStockOutOperation/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/transferStockOutAdd',
        name: '新增调拨出库单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/transferStockOutAdd/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/generalWarehouseReturnDetails',
        name: '总仓退货出库单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/generalWarehouseReturnDetails/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/generalWarehouseReturnOperation',
        name: '总仓退货出库操作',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/generalWarehouseReturnOperation/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockOutList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockOutList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockOutList/generalWarehouseReturnAdd',
        name: '新增总仓退货出库单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockOutList/generalWarehouseReturnAdd/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/platformStockInList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockInList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockInList/addInOrder',
        name: '新增平台入库单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockInList/addInOrder/index.vue')
    }]
}, {
    path: '/platformProcurementInventory/platformStockInList',
    component: Layout,
    redirect: '/platformProcurementInventory/platformStockInList/index',
    children: [{
        path: '/platformProcurementInventory/platformStockInList/goinOrder',
        name: '平台入库操作',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformStockInList/goinOrder/index.vue')
    }]
}, {
    path: '/platformProcurementInventory',
    component: Layout,
    redirect: '/platformProcurementInventory/platformOrderList/index',
    children: [{
        path: '/platformProcurementInventory/platformOrderList/orderDetails',
        name: '平台采购单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/platformOrderList/list_table/orderDetails/index.vue')
    }]
},
{
    path: '/shopProcurementInventory/storeOrderList',
    component: Layout,
    redirect: '/shopProcurementInventory/storeOrderList/index',
    children: [{
        path: '/shopProcurementInventory/storeOrderList/addOrder',
        name: '新增门店采购单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shopProcurementInventory/storeOrderList/list_table/addOrder/index.vue')
    }]
},
{
    path: '/platformProcurementInventory/storeOrderDetails',
    component: Layout,
    redirect: '/platformProcurementInventory/storeOrderDetails/index',
    children: [{
        path: '/platformProcurementInventory/storeOrderDetails/orderDetails',
        name: '门店采购单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/platformProcurementInventory/storeOrderDetails/orderDetails/index.vue')
    }]
},
{
    path: '/shopProcurementInventory/storeInList',
    component: Layout,
    redirect: '/shopProcurementInventory/storeInList/index',
    children: [{
        path: '/shopProcurementInventory/storeInList/addInOrder',
        name: '新增门店入库单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shopProcurementInventory/storeInList/addInOrder/index.vue')
    }]
}, {
    path: '/shopProcurementInventory/storeInList',
    component: Layout,
    redirect: '/shopProcurementInventory/storeInList/index',
    children: [{
        path: '/shopProcurementInventory/storeInList/orderDetails',
        name: '门店入库详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shopProcurementInventory/storeInList/orderDetails/index.vue')
    }]
}, {
    path: '//shopProcurementInventory/storeInList',
    component: Layout,
    redirect: '//shopProcurementInventory/storeInList/index',
    children: [{
        path: '//shopProcurementInventory/storeInList/goinOrder',
        name: '门店入库操作',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views//shopProcurementInventory/storeInList/goinOrder/index.vue')
    }]
}, {
    path: '/shopProcurementInventory/storeOutList',
    component: Layout,
    redirect: '/shopProcurementInventory/storeOutList/index',
    children: [{
        path: '/shopProcurementInventory/storeOutList/addOutOrder',
        name: '门店新增退货出库单',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/shopProcurementInventory/storeOutList/addOutOrder/index.vue')
    }]
},
{
    path: '/',
    component: Layout,
    redirect: '/',
    children: [{
        path: '/supplier/goods/index',
        name: '供应商品列表',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/supplier/goods/index.vue')
    }]
}, {
    path: '/',
    component: Layout,
    redirect: '/',
    children: [{
        path: '/finance/agencyBranchReport/details',
        name: '分公司/代理商明细',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/finance/agencyBranchReport/details/index.vue')
    }]
},
{
    path: '/',
    component: Layout,
    redirect: '/',
    children: [{
        path: '/finance/agencyBranchReport/salesDetail',
        name: '分公司/代理商明细',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/finance/agencyBranchReport/salesDetail/index.vue')
    }]
},

{
    path: '/pointsMall/pointsOrder',
    component: Layout,
    redirect: '/pointsMall/pointsOrder/index',
    children: [{
        path: '/pointsMall/pointsOrder/details',
        name: '积分订单详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/pointsMall/pointsOrder/details/index.vue')
    }]
},

{
    path: '/marketing/couponCollectionList',
    component: Layout,
    redirect: '/marketing/couponCollectionList/index',
    children: [{
        path: '/marketing/couponCollectionList/securitiesDetails',
        name: '领券明细',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/marketing/couponCollectionList/securitiesDetails/index.vue')
    }]
},
{
    path: '/marketing/activeList',
    component: Layout,
    redirect: '/marketing/activeList/index',
    children: [{
        path: '/marketing/activeList/addCoupon',
        name: '新增优惠券',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/marketing/activeList/addCoupon/index.vue')
    }]
},
{
    path: '/marketing/activeList',
    component: Layout,
    redirect: '/marketing/activeList/index',
    children: [{
        path: '/marketing/activeList/couponDetails',
        name: '优惠券详情',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/marketing/activeList/couponDetails/index.vue')
    }]
},

{
    path: '/finance/SupplierReport',
    component: Layout,
    redirect: '/finance/SupplierReport/index',
    children: [{
        path: '/finance/SupplierReport/SupplierReportDetails',
        name: '供应商对账报表明细',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/finance/SupplierReport/SupplierReportDetails/index.vue')
    }]
},
{
    path: '/finance/storeReport',
    component: Layout,
    redirect: '/finance/storeReport/index',
    children: [{
        path: '/finance/storeReport/storeReportDetails',
        name: '门店对账报表明细',
        component: () =>
            import(/* webpackChunkName: "views" */ '@/views/finance/storeReport/storeReportDetails/index.vue')
    }]
},
] 
