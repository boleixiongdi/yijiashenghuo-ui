export const tableOption = {
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  editBtn: true,
  delBtn: true,
  addBtn: true,
  excelBtn: true,
  "column": [
    {
      label: 'id',
      prop: 'id',
      hide: true,
      editDisabled: true,
      editDisplay: false,
      addDisplay: false
    },
	  {
      "type": "input",
      "label": "产品名称",
      "prop": "productName",
      "width":120,
      search: true,
    },	  {
      "type": "input",
      "label": " 数量",
      "width":50,
      "prop": "amount"
    },	  {
      "type": "input",
      "label": "用户名",
      "prop": "username"
    },	  {
      "type": "input",
      "label": " 手机号",
      "prop": "phone",
      search: true,
    },	  {
      "type": "input",
      "label": "省份",
      "prop": "province",
      search: true,
    },	  {
      "type": "input",
      "label": "城市",
      "prop": "city",
      search: true,
    },	  {
      "type": "input",
      "label": "区",
      "prop": "district"
    },	  {
      "type": "input",
      "label": "地址",
      "prop": "address"
    },	  {
      "type": "input",
      "label": "单价",
      "width":50,
      "prop": "price"
    },	  {
      "type": "input",
      "label": "订单状态",
      "prop": "status"
    },	  {
      "type": "datetime",
      "label": "订单创建时间",
      "valueFormat": "yyyy-MM-dd",
      "width":140,
      search: true,
      "prop": "createTime"
    },	  {
      "type": "input",
      "label": "留言",
      "prop": "message"
    },	  {
      "type": "input",
      "label": "快递单号",
      "prop": "orderd"
    },	  {
      "type": "input",
      "label": "快递单号",
      "prop": "expressNumber"
    },	  {
      "type": "input",
      "label": "物流名称",
      "prop": "expressName"
    },	  {
      "type": "input",
      "label": "订单更新时间",
      "prop": "updateTime"
    },	  {
      "type": "input",
      "label": "付款方式",
      "prop": "payType"
    },	  {
      "type": "input",
      "label": "备注",
      "prop": "remark"
    } ]
}
