export const tableOption = {
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  "searchMenuSpan": 6,
  "column": [
	  {
      "type": "input",
      "disabled":true,
      "label": "模块id",
      "prop": "moduleId"
    },	  {
      "type": "input",
      "label": "模块编号",
      "prop": "moduleCode"
    },	  {
      "type": "input",
      "label": "模块名称",
      "prop": "moduleName"
    },	  {
      "type": "input",
      "label": "模块地址",
      "prop": "moduleUrl"
    },	  {
      "type": "input",
      "label": "图标",
      "prop": "moduleIcon"
    },	  {
      "type": "input",
      "label": "描述",
      "prop": "moduleDescription"
    },	  {
      "type": "input",
      "label": "是否新窗口中打开",
      "prop": "isNewTab"
    }  ]
}
