/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  viewBtn: true,
  column: [ 
    {
      label: '主键',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addDisplay: false
    },
    {
      label: '文章分类ID',
      prop: 'CategoryID',
      rules: [{
        required: true,
        message: '文章分类',
        trigger: 'blur'
      }]
    },
    {
      label: '文章tagID',
      prop: 'TagID',
      rules: [{
        required: true,
        message: '文章tag',
        trigger: 'blur'
      }]
    },
    {
      label: '文章标题',
      prop: 'Title',
      rules: [{
        required: true,
        message: '文章标题',
        trigger: 'blur'
      }]
    },
    {
      label: '封面图片',
      prop: 'CoverImageUrl',
      placeholder: '封面图片',
      slot:true,
      rules: [{
        required: true,
        message: '封面图片',
        trigger: 'blur'
      }]
    },
    {
      label: '备注',
      prop: 'Desc'
    },
    {
      width: 150,
      label: '创建时间',
      prop: 'CreateTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      editDisplay: false,
      addDisplay: false
    }
  ]
}
