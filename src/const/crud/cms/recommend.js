/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  viewBtn: true,
  column: [ 
    {
      label: '主键',
      prop: 'id',
      hide: true,
      editDisabled: true,
      addDisplay: false
    },
    {
      label: '名称',
      prop: 'Name',
      rules: [{
        required: true,
        message: '名称',
        trigger: 'blur'
      }]
    },
    {
      label: '描述',
      prop: 'Desc',
      rules: [{
        required: true,
        message: '描述',
        trigger: 'blur'
      }]
    },
    {
      label: '图片',
      prop: 'CoverImageUrl',
      rules: [{
        required: true,
        message: '图片',
        trigger: 'blur'
      }]
    },
    {
      label: '跳转链接',
      prop: 'LinkUrl',
      rules: [{
        required: true,
        message: '跳转链接',
        trigger: 'blur'
      }]
    },
    {
      width: 150,
      label: '创建时间',
      prop: 'CreateTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      editDisplay: false,
      addDisplay: false
    }
  ]
}
