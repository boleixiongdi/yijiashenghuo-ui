import search from "./search/search.js";
import listTable from "./list_table/list_table.vue";

export default {
    mixins: [search],
    components: {
        listTable,
    },
    data() {
        return {

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.listTable.getList()       //会员列表
        },
    }
}