import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import {fetchList} from '@/api/shopProcurementInventory/storeOrderList.js'
export default {
    name: "list_table",
    mixins: [search],
    components: {
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{
                zt: 0
            }, {
                zt: 1
            }],
            page: {
                currentPage: 1,
                total: 0,
                pageSize: 10
            },
            btn: [
                {
                    title: "新增采购单",
                    isShow: true
                },
                {
                    title: "导出",
                    isShow: true
                }
            ],

        }
    },
    mounted(){
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            fetchList().then((val)=>{
                console.log(888,val)
            })
        },
        sizeChange(currentPage) {
            this.page.currentPage = currentPage
        },
        currentChange(pageSize) {
            this.page.pageSize = pageSize
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        del(flag) {         // 作废
            this.$router.push({path:"/platformProcurementInventory/platformOrderList/addOrder"})
        },
        details(item) {  //编辑
            
        },
        addSupplier() {  //新增
            this.$router.push({path:"/shopProcurementInventory/storeOrderList/addOrder"})
        },
    }

} 