import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                lx: 1,
                gysmc: '供应商A'
            },
            rules: {
                gysmc: [
                    { required: true, },
                ],
                date1: [
                    { required: true },
                ],
                lx: [
                    { required: true },
                ]
            },
            gysmc: [                //供应商名称
                {
                    label: "供应商A",
                    value: '供应商A'
                },
                {
                    label: "供应商B",
                    value: '供应商B'
                }
            ],

            mendianList: [  //门店list
                {
                    value: 1,
                    label: '东南',
                    children: [{
                        value: 2,
                        label: '上海',
                        children: [
                            { value: 3, label: '普陀' },
                            { value: 4, label: '黄埔' },
                            { value: 5, label: '徐汇' }
                        ]
                    }, {
                        value: 7,
                        label: '江苏',
                        children: [
                            { value: 8, label: '南京' },
                            { value: 9, label: '苏州' },
                            { value: 10, label: '无锡' }
                        ]
                    }, {
                        value: 12,
                        label: '浙江',
                        children: [
                            { value: 13, label: '杭州' },
                            { value: 14, label: '宁波' },
                            { value: 15, label: '嘉兴' }
                        ]
                    }]
                }, {
                    value: 17,
                    label: '西北',
                    children: [{
                        value: 18,
                        label: '陕西',
                        children: [
                            { value: 19, label: '西安' },
                            { value: 20, label: '延安' }
                        ]
                    }, {
                        value: 21,
                        label: '新疆维吾尔族自治区',
                        children: [
                            { value: 22, label: '乌鲁木齐' },
                            { value: 23, label: '克拉玛依' }
                        ]
                    }]
                }
            ],
        }
    },
    methods: {
        submitSave() {   //提交并保存
            console.log(this.ruleForm)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '添加直送门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            this.$refs.buyingIfor.dataList = []
            data.chioseVal.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    mendian: i
                })
            });
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        }
    },

} 