import { tableOption,tableOption1 } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
export default {
    name: "list_table",
    mixins: [search],
    components: {
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{
                zt: 0
            }, {
                zt: 1
            }],
            page: {
                currentPage: 1,
                total: 0,
                pageSize: 10
            },
            btn: [
                // {
                //     title: "新增采购单",
                //     isShow: true
                // },
                {
                    title: "导出",
                    isShow: true
                }
            ],
            selectedRows:[],  //选中的数组

            dialogVisible:false,   //库存流水
            tableOption1: tableOption1,
            loading1: false,
            loadData:[{}]
        } 
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            setTimeout(() => {
                this.page.total = 200
            }, 500)
        },
        sizeChange(currentPage) {
            this.page.currentPage = currentPage
        },
        currentChange(pageSize) {
            this.page.pageSize = pageSize
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.export()
            }
        },
        kucunLs(item) {  //编辑
            this.dialogVisible = true
        },
        export(){

        }
    }

} 