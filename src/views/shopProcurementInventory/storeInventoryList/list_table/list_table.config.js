export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: true,
    reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'name',
            solt: true,
        },
        {
            label: '库存单位',
            prop: 'mc',

        },
        {
            label: '商品类目',
            prop: 'phone',


        },
        {
            label: '实物库存',
            prop: 'kc',

        },
        {
            label: '占用库存',
            prop: 'fs',


        }, {
            label: '可用库存',
            prop: 'dh',
        }, {
            label: '采购待入库存',
            prop: 'zt',

        }, {
            label: '成本单价/元',
            prop: 'ly',

        }, {
            label: '总成本/元',
            prop: 'pslx',

        },
    ]
}



export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '关联单号',
            prop: 'name',
            solt: true,
        },
        {
            label: '创建时间',
            prop: 'mc',

        },
        {
            label: '单据类型',
            prop: 'phone',


        },
        {
            label: '出入库',
            prop: 'kc',

        },
        {
            label: '剩余量',
            prop: 'fs',


        }, {
            label: '成本单价/元',
            prop: 'dh',
        }, {
            label: '操作人',
            prop: 'zt',

        },
    ]
}
