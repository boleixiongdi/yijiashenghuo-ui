import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";
import cropperModal from "@/components/cropper/index.vue";
import { fileBase64 } from "@/util/util";

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop,
        cropperModal
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                lx: 1,
                gysmc: '供应商A'
            },
            rules: {
                gysmc: [
                    { required: true, },
                ],
                date1: [
                    { required: true },
                ],
                lx: [
                    { required: true },
                ]
            },
            gysmc: [                //供应商名称
                {
                    label: "供应商A",
                    value: '供应商A'
                },
                {
                    label: "供应商B",
                    value: '供应商B'
                }
            ],

            mendianList: [  //门店list
                {
                    value: 1,
                    label: '东南',
                    children: [{
                        value: 2,
                        label: '上海',
                        children: [
                            { value: 3, label: '普陀' },
                            { value: 4, label: '黄埔' },
                            { value: 5, label: '徐汇' }
                        ]
                    }, {
                        value: 7,
                        label: '江苏',
                        children: [
                            { value: 8, label: '南京' },
                            { value: 9, label: '苏州' },
                            { value: 10, label: '无锡' }
                        ]
                    }, {
                        value: 12,
                        label: '浙江',
                        children: [
                            { value: 13, label: '杭州' },
                            { value: 14, label: '宁波' },
                            { value: 15, label: '嘉兴' }
                        ]
                    }]
                }, {
                    value: 17,
                    label: '西北',
                    children: [{
                        value: 18,
                        label: '陕西',
                        children: [
                            { value: 19, label: '西安' },
                            { value: 20, label: '延安' }
                        ]
                    }, {
                        value: 21,
                        label: '新疆维吾尔族自治区',
                        children: [
                            { value: 22, label: '乌鲁木齐' },
                            { value: 23, label: '克拉玛依' }
                        ]
                    }]
                }
            ],

            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false
        }
    },
    methods: {
        submitSave() {   //提交并保存
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '添加直送门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            this.$refs.buyingIfor.dataList = []
            data.chioseVal.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    mendian: i
                })
            });
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleCropperSuccess(data) {
            //图片裁切完成保存
            fileBase64(data).then(result => {
                this.imageUrl = result.result;
                this.fileList.push({
                    uid: this.number++,
                    name: "image.png",
                    status: "done",
                    url: this.imageUrl
                });
            });
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 