export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '单据编号',
            prop: 'ID',

        },
        {
            label: '入库类型',
            prop: 'sl',


        },
        {
            label: '入库状态',
            prop: 'hy',

        },
        {
            label: '配送方',
            prop: 'fs'

        }, {
            label: '预计入库量',
            prop: 'name',

        }, {
            label: '已确认入库量',
            prop: 'dz'

        }, {
            label: '待确认入库量',
            prop: 'fsje',

        }, {
            label: '创单人',
            prop: 'je',

        },
        {
            label: '创单时间',
            prop: 'dqwe',

        },
    ]
}

