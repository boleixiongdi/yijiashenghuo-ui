import { tableOption } from './buyingIfor1.config.js'
import { toDecimal2 } from '@/util/util'
import choiceGoods from './../choiceGoods.vue'
export default {
    name: "buyingIfor1",
    components: {
        choiceGoods
    },
    props: {
        ShopOrGoods: {
            type: Boolean,
            default: true
        }
    },
    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            activeNames: '1',
            btn: [
                // {
                //     title: "选择商品",
                //     isShow: true,
                //     size: "mini"
                // },
                // {
                //     title: "批量导入",
                //     isShow: true,
                //     size: "mini"
                // }
            ],
            total: {
                number: 0, //商品数量
                price: 0 //总价格
            },
            dataList: [{}],
            _index1: 0,
            search:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.currentPage = page
            }
            setTimeout(() => {
                this.page.total = 100
            }, 500)
        },
        del() {

        },
        handler(index, index1) {
            if (index == 0) {
                this.choiceGoodsBtn(index1)
            } else if (index == 1) {
                this.plExport()
            }
        },
        choiceGoodsBtn(index1) {   //index1  某门店在数组位置
            this._index1 = index1
            this.$refs.choiceGoods[index1].dialogVisibleTile = '选择 ' + this.$parent.$parent.model.gysmc + ' 商品'
            this.$refs.choiceGoods[index1].dialogVisible = true
        },
        plExport() {

        },
        trigger(data) {   //选择完商品后触发
            this.dataList[this._index1].loadData = new Array()
            data.chioseVal.forEach(i => {
                this.dataList[this._index1].loadData.push({
                    bh: i
                })
            });
            this.$forceUpdate()
            console.log(this.dataList)
        }
    }

} 