export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu:false,
    column: [
        {
            label: '商品名称',
            prop: 'bh',
            slot:true

        },
        {
            label: '库存单位',
            prop: 'dw',
        },
        
        {
            label: '预计入库量',
            prop: 'qy',

        },
        {
            label: '箱数',
            prop: 'hy',
            // slot:true,
            width:240
        },
        {
            label: '已确认入库量',
            prop: 'fs',


        }, {
            label: '待确认入库量',
            prop: 'dh',
            // slot:true,
            width:240

        },
        {
            label: '实际入库量',
            prop: 'dh1',
            // slot:true,
            width:240

        },
    ]
}

