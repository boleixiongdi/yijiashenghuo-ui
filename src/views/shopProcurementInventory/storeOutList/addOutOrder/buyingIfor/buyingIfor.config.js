export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    column: [
        {
            label: '商品名称',
            prop: 'bh',
            slot:true

        },
        {
            label: '库存单位',
            prop: 'dw',



        },
        {
            label: '采购箱',
            prop: 'hy',
            slot:true,
            width:240
        },
        {
            label: '采购数量',
            prop: 'qy',

        },
        {
            label: '采购价/元/件',
            prop: 'fs',


        }, {
            label: '小计/元',
            prop: 'dh',
            slot:true,
            width:240

        },
    ]
}

