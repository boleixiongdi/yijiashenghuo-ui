import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import comTitle from "@/views/com/com_title.vue";

import { mapGetters } from 'vuex'
const tabName = [
    "待入库",
    "待入库",
    "待入库",
]
export default {
    mixins: [search],
    components: {
        allShop,
        comTitle,

    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "待入库",
                openShop: "待入库",
                stopShop: "待入库",
            },
            dialogVisible: false,
            loadingSure: false,

            ruleForm: {

            },
            mendian: [],  //选择的门店
            mendianList: [],   //门店list
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback(key) {
            this.keyName = key
            console.log(this.keyName)
            switch (this.keyName) {
                case '1':
                    this.$refs.allShop.getList() //待配单列表
                    break;
                case '2':
                    this.$refs.openShop.getList() //待配送列表
                    break;
                case '3':
                    this.$refs.stopShop.getList() //配送中列表
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allShop.getList()       //待配单列表
            this.$refs.openShop.getList()  //待配送列表
            this.$refs.stopShop.getList() //配送中列表
        },
        trigger() {
            this.tab.allShop = `${tabName[0]}    ${this.$refs.allShop.page.total}`
            this.tab.openShop = `${tabName[1]}    ${this.$refs.openShop.page.total}`
            this.tab.stopShop = `${tabName[2]}    ${this.$refs.stopShop.page.total}`
        },
        addMendian() {
            this.dialogVisible = true
            this.mendian = []   //取出门店
            this.mendianList = []
            if (this.keyName == "1") {
                //待配单列表
                this.$refs['allShop'].selectedRows.forEach(i => {
                    this.mendian.push(i.id)
                    this.mendianList.push({ label: i.name, value: i.id })
                })
                this.addStore()
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //待配送列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //配送中列表
            }
        },
        drShangpin() {
            if (this.keyName == "1") {
                this.$refs.allShop.getList()       //待配单列表
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //待配送列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //配送中列表
            }
        },
        handlClose() {
            this.dialogVisible = false
        },
        handlSure() {
            console.log('选择门店', this.mendian)
            console.log('门店采购商品明细', this.$refs['buyingIfor'].selectedRows)
        },
        addStore() {  //创建平台采购单
            this.$router.push({ path: "/shopProcurementInventory/storeOutList/addOutOrder" })
        },
    }
}