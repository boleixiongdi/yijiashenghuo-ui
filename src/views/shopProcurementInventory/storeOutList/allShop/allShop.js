import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{
                id: 1,
                name:"门店一"
            },{
                id: 2,
                name:"门店二"
            },{
                id: 3,
                name:"门店三"
            }],
            page: {
                currentPage: 1,
                total: 0,
                pageSize: 10
            },
            selectedRows: [],//选中的数组
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            setTimeout(() => {
                this.page.total = 200
                this.$emit("trigger")
            }, 500)
        },
        sizeChange(currentPage) {
            this.page.currentPage = currentPage
        },
        currentChange(pageSize) {
            this.page.pageSize = pageSize
        },
        // 删除
        del() {
            console.log(123321)
        },
        details(item) {
            item.flag = 1
            this.$router.push({ path: '/platformProcurementInventory/platformStockInList/orderDetails', query: item })
        },
        peidan(item){
            item.flag = 1
            // this.$router.push({ path: '/platformProcurementInventory/platformStockInList/goinOrder', query: item })
        }
    }

} 