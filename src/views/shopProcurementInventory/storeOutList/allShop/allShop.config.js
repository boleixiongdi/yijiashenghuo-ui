export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '出库单号',
            prop: 'ID',

        },
        {
            label: '出库状态',
            prop: 'sl',


        },
        {
            label: '预计出库量',
            prop: 'hy',

        },
        {
            label: '已确认出库量',
            prop: 'fs'

        }, {
            label: '待确认出库量',
            prop: 'name',

        }, {
            label: '接收方',
            prop: 'dz'

        },{
            label: '创单人',
            prop: 'je',

        },
        {
            label: '创单时间',
            prop: 'dqwe',

        },
    ]
}

