import { tableOption } from './closeTrade.config.js'
import search from './../search/search.js';

import { fetchList, refund } from '@/api/shop/order/SuperCoffeeBar.js'
export default {
    name: "closeTrade",
    mixins: [search],
    data() {
        return {
            loadingSure: false,

            dialogVisible: false,
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            ruleForm: {
                payOrderNo: '',
                paySerials: [],
                refundReason: []
            },
            rules: {
                refundReason:[
                    { required: true, message: '请选择退款原因', trigger: 'change' }
                ]
            },
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm

            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({ orderStatus: '04' },this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        tuihuo(item) {
            this.dialogVisible = true;
            this.ruleForm.refundAmt = item.orderAmt;
            this.ruleForm.payOrderNo = item.payOrderNo;
            this.ruleForm.subOrderId = item.subOrderId;
            this.ruleForm.paySerials = item.paySerials;
            this.ruleForm.refundReason = []
            console.log(666, item)
        },
        details(item) {

            this.$router.push({
                path: '/order/SuperCoffeeBar/details', query: { subOrderId: item.subOrderId }
            }
            )
        },
        handlSure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    let obj = { ...this.ruleForm }
                    obj.refundReason = obj.refundReason.join(',')
                    this.loadingSure = true
                    refund(obj).then(val => {
                        this.$message.success('退款成功');this.dialogVisible = false
                        this.loadingSure = false
                    }).catch(() => {
                        this.$message.warning('退款失败');this.dialogVisible = false
                        this.loadingSure = false
                    })
                } else {
                    return false;
                }
            });
        }
    }

} 