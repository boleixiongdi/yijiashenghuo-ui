import { tableOption } from './servering.config.js'
import search from './../search/search.js';
export default {
    name: "servering",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                current: 1,
                total: 0,
                pageSize: 10
            },
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            setTimeout(() => {
                this.page.total = 15
                this.$emit("trigger")
            }, 500)
        },
        sizeChange(current) {
            this.page.current = current
        },
        currentChange(pageSize) {
            this.page.pageSize = pageSize
        },
    }

} 