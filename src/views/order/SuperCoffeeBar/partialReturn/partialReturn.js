import { tableOption } from './partialReturn.config.js'
import search from './../search/search.js';
import { fetchList, refund } from '@/api/shop/order/SuperCoffeeBar.js'
export default {
    name: "partialReturn",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            dialogVisible: false,
            ruleForm: {
                payOrderNo: '',
                paySerials: [],
                refundReason: []
            },
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            rules: {},
            loadingSure: false
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({}, searchForm ? searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        tuihuo(item) {
            this.dialogVisible = true;
            this.ruleForm.refundAmt = item.orderAmt;
            this.ruleForm.payOrderNo = item.payOrderNo;
            this.ruleForm.subOrderId = item.subOrderId;
            this.ruleForm.paySerials = item.paySerials;
            this.ruleForm.refundReason = []
            console.log(666, item)
        },
        handlSure() {
            this.ruleForm.refundReason = this.ruleForm.refundReason.join(',')
            refund(this.ruleForm).then(val => {

            })
        }
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 