export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '订单编号',
            prop: 'bh',

            width: 120
        },
        {
            label: '商品数量',
            prop: 'sl',

            width: 120

        },
        {
            label: '会员',
            prop: 'hy',

            width: 120
        },
        {
            label: '配送方式',
            prop: 'fs',

            width: 120
        }, {
            label: '联系电话',
            prop: 'dh',

            width: 120
        }, {
            label: '收货地址',
            prop: 'dz',

            width: 120
        }, {
            label: '订单金额',
            prop: 'je',

            width: 120
        }, {
            label: '支付方式及金额',
            prop: 'fsje',

            width: 150
        }, {
            label: '订单来源',
            prop: 'ly',

            width: 120
        }, {
            label: '所属门店',
            prop: 'md',

            width: 120
        }, {
            label: '创建时间',
            prop: 'sj',

            width: 120
        }, {
            label: '订单状态',
            prop: 'zt',

            width: 120
        },
    ]
}

