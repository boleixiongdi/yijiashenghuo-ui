import comTitle from '@/views/com/com_title.vue'
import tables from './table.vue'
import { refundorder } from '@/api/shop/order/SuperCoffeeBar.js'
export default {
    components: {
        comTitle,
        tables
    },
    data() {
        return {
            info: {
                title: "基本信息",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "退货单编号：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "退货时间：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "操作人：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "退货备注：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    },
                ],
            },
            refund: {
                title: "小计与收款：",
                color: '#000',
                gutter: 0,
                formList: [{
                    label: "退款金额：",
                    value: "未填报",
                    smSpan: 24,
                    smOffset: 0
                }, {
                    label: "退款方式：",
                    value: '未填报',
                    children: [

                    ],
                    smSpan: 24,
                    smOffset: 0
                },]
            }
        }
    },
    watch: {
        $route() {
            this.getDetail()
        }
    },
    mounted() {
        this.getDetail()
    },
    methods: {
        getDetail() {
            refundorder(this.$route.query.subOrderId).then(val => {
                let data = val.data.data
                if (data) {
                    this.info = {
                        title: "基本信息",
                        color: '#000',
                        gutter: 0,
                        formList: [
                            {
                                label: "退货单编号：",
                                value: data.subRefundOrderId,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "退货时间：",
                                value: data.createTime,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "操作人：",
                                value: data.operatorId,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "退货备注：",
                                value: this.refundReason(data.refundReason),
                                smSpan: 24,
                                smOffset: 0
                            },
                        ],
                    }

                    this.refund = {
                        title: "退款信息：",
                        color: '#000',
                        gutter: 0,
                        formList: [{
                            label: "退款金额：",
                            value: data.subRefundAmt,
                            smSpan: 24,
                            smOffset: 0
                        }, {
                            label: "退款方式：",
                            children: [

                            ],
                            smSpan: 24,
                            smOffset: 0
                        },]
                    }
                    this.refund.formList[1].children = data.serials.map(item => {
                        return {
                            label: this.payChannelNo(item.payChannelNo),
                            value: item.rsRefundAmt,
                        }
                    })
                }

            })
        },
        payChannelNo(name) {
            if (name == '00') {
                return '支付宝'
            } else if (name == '01') {
                return '微信'
            } else if (name == '02') {
                return '储蓄卡'
            }
        },
        refundReason(name) {
            if (name == '00') {
                return '下错单'
            } else if (name == '01') {
                return '客户不想要了'
            } else if (name == '02') {
                return '商品库存不足'
            }
        }

    }

} 