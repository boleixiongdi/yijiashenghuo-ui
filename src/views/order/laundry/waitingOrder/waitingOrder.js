import { tableOption } from './waitingOrder.config.js'
import search from './../search/search.js';
import { fetchList, putObj } from '@/api/shop/order/laundry.js'
export default {
    name: "waitingOrder",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({orderStatus:'08'}, searchForm ? searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
              console.log( this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    }

} 