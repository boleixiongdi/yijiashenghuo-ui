const DIC = {
    status: [{
        label: "支付成功",
        value: "00"
    },
    {
        label: "支付失败",
        value: "01"
    },
    {
        label: "待支付",
        value: "02"
    }
        ,
    {
        label: "支付中",
        value: "03"
    }
        ,
    {
        label: "支付关闭、作废",
        value: "04"
    }
        ,
    {
        label: "退款中",
        value: "05"
    }
        ,
    {
        label: "退款完成",
        value: "06"
    }
        ,
    {
        label: "退款失败",
        value: "07"
    }
        ,
    {
        label: "待派单",
        value: "08"
    }
        ,
    {
        label: "服务中",
        value: "09"
    }
        ,
    {
        label: "已拒单",
        value: "10"
    },
    {
        label: "待接单",
        value: "11"
    }],
    workStatus: [
        {
            label: '待派单',
            value: '00'
        }, {
            label: '待取衣',
            value: '01'
        }, {
            label: '门店收衣',
            value: '02'
        }, {
            label: '待工厂取衣',
            value: '03'
        },
        {
            label: '洗涤中',
            value: '04'
        }, {
            label: '待送衣',
            value: '05'
        }, {
            label: '自提取衣',
            value: '06'
        }, {
            label: '送衣中',
            value: '07'
        }, {
            label: '已完成',
            value: '08'
        }, {
            label: '退款审核',
            value: '09'
        }, {
            label: '退款关闭',
            value: '10'
        }, {
            label: '待接单',
            value: '11'
        }
    ]

}

export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 200,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '总订单号',
            prop: 'payOrderNo',
            width: 180,
            overHidden: true,
        },
        {
            label: '订单编号',
            prop: 'subOrderId',
            width: 180,
            overHidden: true,
        },
        {
            label: '商品数量',
            prop: 'goodsNum',
            width: 150,
            overHidden: true,

        },
        // {
        //     label: '会员',
        //     prop: 'hy',
        //     width: 120
        // },
        // {
        //     label: '取衣方式',
        //     prop: 'qy',
        //     width: 120
        // },
        // {
        //     label: '配送方式',
        //     prop: 'fs',

        //     width: 120
        // }, {
        //     label: '联系电话',
        //     prop: 'dh',

        //     width: 120
        // }, {
        //     label: '服务地址',
        //     prop: 'dz',

        //     width: 120
        // }, 
        {
            label: '订单金额（元）',
            prop: 'orderAmt',
            width: 150,
            overHidden: true,
        }, 
        // {
        //     label: '支付方式',
        //     prop: 'paySerials',
        //     slot: true,
        //     width: 150,
        //     overHidden: true,
        // }, 
        {
            label: '订单来源',
            prop: 'orderSource',
            width: 150,
            overHidden: true,
            dicData: [
                {
                    label: '小程序',
                    value: '00'
                }, {
                    label: 'APP',
                    value: '01'
                },
                {
                    label: '线下',
                    value: '02'
                }
            ]
        },
        {
            label: '所属门店',
            prop: 'companyName',
            width: 150,
            overHidden: true,
        }, {
            label: '创建时间',
            prop: 'orderTime',
            width: 150,
            overHidden: true,
        }, {
            label: '订单状态',
            prop: 'orderStatus',
            dicData: DIC.status,
            width: 150,
            overHidden: true,
        },
        {
            label: '工单状态',
            prop: 'workStatus',
            dicData: DIC.workStatus,
            width: 120
        },
    ]
}

