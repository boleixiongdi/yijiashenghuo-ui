import search from "./search/search.js";
import allOrder from "./allOrder/allOrder.vue";
import waitingOrder from "./waitingOrder/waitingOrder.vue";
import servering from "./servering/servering.vue";
import successfulTrade from "./successfulTrade/successfulTrade.vue";
import partialReturn from "./partialReturn/partialReturn.vue";
import closeTrade from "./closeTrade/closeTrade.vue";

const tabName = [
    "全部订单",
    "待派单",
    "服务中",
    "交易成功",
    "部分退货",
    "交易关闭"
]
export default {
    mixins: [search],
    components: {
        allOrder,
        waitingOrder,
        servering,
        successfulTrade,
        partialReturn,
        closeTrade
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allOrder: "全部订单",
                waitingOrder: "待派单",
                servering: "服务中",
                successfulTrade: "交易成功",
                partialReturn: "部分退货",
                closeTrade: "交易关闭"
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList() //全部订单列表
                    break;
                case '2':
                    this.$refs.waitingOrder.getList() //待派单列表
                    break;
                case '3':
                    this.$refs.servering.getList() //服务中列表
                    break;
                case '4':
                    this.$refs.successfulTrade.getList() //交易成功列表
                    break;
                case '5':
                    this.$refs.partialReturn.getList() //部分退货列表
                    break;
                case '6':
                    this.$refs.closeTrade.getList() //部分退货列表
                    break;
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allOrder.getList()       //全部订单列表
            // this.$refs.waitingOrder.getList()  //待派单列表
            // this.$refs.servering.getList() //服务中列表
            this.$refs.successfulTrade.getList() //交易成功列表
            // this.$refs.partialReturn.getList() //部分退货列表
            this.$refs.closeTrade.getList() //交易关闭列表
        },
        trigger() {
            this.tab.allOrder = `${tabName[0]}(${this.$refs.allOrder.page.total})`
            // this.tab.waitingOrder = `${tabName[1]}(${this.$refs.waitingOrder.page.total})`
            // this.tab.servering = `${tabName[2]}(${this.$refs.servering.page.total})`
            this.tab.successfulTrade = `${tabName[3]}(${this.$refs.successfulTrade.page.total})`
            // this.tab.partialReturn = `${tabName[4]}(${this.$refs.partialReturn.page.total})`
            this.tab.closeTrade = `${tabName[5]}(${this.$refs.closeTrade.page.total})`
         }
    }
}