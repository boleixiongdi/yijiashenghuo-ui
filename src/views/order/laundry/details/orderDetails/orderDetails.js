import comTitle from '@/views/com/com_title.vue'
import tables from './table.vue'
import { detail } from '@/api/shop/order/laundry.js'
export default {

    components: {
        comTitle,
        tables
    },
    props: {
        isOnline: {
            type: String
        }
    },
    data() {
        return {
            Tabdata: [],
            info: {
                title: "基本信息",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "订单编号：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "创建时间：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "联系电话：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单来源：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "门店：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收银员：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单备注：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    },
                ],
            },
            server: {
                title: "服务信息",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "配送方式：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "联系电话：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收货地址：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                ]
            },
            discount: {
                title: "折扣信息：",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "优惠券：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },]
            },
            collection: {
                title: "小计与收款：",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "商品数量：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "订单金额（元）：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    },
                    // {
                    //     label: "配送费：",
                    //     value: "未填报",
                    //     smSpan: 24,
                    //     smOffset: 0
                    // }, 
                    {
                        label: "会员优惠金额：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "优惠券优惠金额：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "实收金额：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "支付详情：",
                        children: [
                            // {
                            //     label: "支付宝：",
                            //     value: "￥50.00",
                            // }, {
                            //     label: "储值卡：",
                            //     value: "￥50.00",
                            // }
                        ],
                        smSpan: 24,
                        smOffset: 0
                    },]
            },

        }
    },
    watch: {
        $route() {
            this.getDetail()
        }
    },
    mounted() {
        this.getDetail()
    },
    methods: {
        getDetail() {
            console.log(362222222222222)
            detail(this.$route.query.subOrderId).then(val => {
                let data = val.data.data
                if (data) {
                    data.paySerialsList = []
                    var list = []
                    if (data.paySerials) {
                        data.paySerials.forEach(item => {
                            if (item.payChannelNo != '03') {
                                data.paySerialsList.push({ label: this.payChannelNo(item.payChannelNo), value: '￥' + (item.payAmt ? item.payAmt : 0) + '元' })
                            } else {
                                list.push(item)
                            }
                        });
                        if (list.length) {
                            data.paySerialsList.push({
                                label: '储蓄卡：', value: '', children: list.map(items => {
                                    return {
                                        label: this.accountType(items.accountType) + '：',
                                        value: '￥' + (items.payAmt ? items.payAmt : 0) + '元'
                                    }
                                })
                            })
                        }
                    }
                    
                    this.info = {
                        title: "基本信息",
                        color: '#000',
                        gutter: 0,
                        formList: [
                            {
                                label: "订单编号：",
                                value: data.payOrderNo,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "创建时间：",
                                value: data.createTime,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "会员电话：",
                                value: data.phoneNum,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "成交时间：",
                                value: data.orderTime,
                                smSpan: 7,
                                smOffset: 0
                            },
                            // {
                            //     label: "交易流水号：",
                            //     value: data.subOrderId,
                            //     smSpan: 7,
                            //     smOffset: 0
                            // },
                            {
                                label: "订单来源：",
                                value: this.orderSource(data.orderSource),
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "门店：",
                                value: data.shopName,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "收银员：",
                                value: data.operatorId,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "交易流水号：",
                                children: data.paySerials ? data.paySerials.map(item => {
                                    return {
                                        value: item.paySerialNo
                                    }
                                }) : '',
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "订单备注：",
                                value: data.subOrderRemark,
                                smSpan: 24,
                                smOffset: 0
                            },
                        ],
                    },
                        this.Tabdata = data.goods ? data.goods : []
                    this.discount = {
                        title: "折扣信息：",
                        color: '#000',
                        gutter: 0,
                        formList: [
                            {
                                label: "优惠券：",
                                value: data.couponName,
                                smSpan: 7,
                                smOffset: 0
                            },]
                    },

                        this.collection = {
                            title: "小计与收款：",
                            color: '#000',
                            gutter: 0,
                            formList: [
                                {
                                    label: "商品数量：",
                                    value: this.Tabdata.length,
                                    smSpan: 24,
                                    smOffset: 0
                                }, {
                                    label: "订单金额（元）：",
                                    value: data.subOrderAmt ? '￥' + data.subOrderAmt : '',
                                    smSpan: 24,
                                    smOffset: 0
                                },
                                {
                                    label: "配送费：",
                                    value: data.deliveryFee ? '￥' + data.deliveryFee : '',
                                    smSpan: 24,
                                    smOffset: 0
                                },
                                {
                                    label: "会员优惠金额：",
                                    value: data.subDiscountAmt ? '-￥' + data.subDiscountAmt : '',
                                    smSpan: 24,
                                    smOffset: 0
                                }, {
                                    label: "优惠券优惠金额：",
                                    value: data.discountAmount ? '-￥' + data.discountAmount : '',
                                    smSpan: 24,
                                    smOffset: 0
                                }, {
                                    label: "实收金额：",
                                    value: data.subPayAmt ? '￥' + data.subPayAmt : '',
                                    smSpan: 24,
                                    smOffset: 0
                                }, 
                                // {
                                //     label: "支付详情：",
                                //     children:data.paySerialsList.length?data.paySerialsList:'',
                                //     smSpan: 24,
                                //     smOffset: 0
                                // },
                            ]
                        }
                }

            })
        },
        payChannelNo(name) {
            if (name == '01') {
                return '支付宝'
            } else if (name == '02') {
                return '微信'
            } else if (name == '03') {
                return '储值卡'
            } else if (name == '04') {
                return '现金'
            }
        },
        orderSource(name) {
            if (name == '00') {
                return '小程序'
            } else if (name == '01') {
                return 'APP'
            } else if (name == '02') {
                return '线下'
            }
        },
        accountType(name) {
            if (name == 'ACC001') {
                return '平台余额账户'
            } else if (name == 'ACC002') {
                return '积分账户'
            } else if (name == 'ACC003') {
                return '商超账户'
            } else if (name == 'ACC004') {
                return '咖啡账户'
            } else if (name == 'ACC005') {
                return '家政账户'
            } else if (name == 'ACC006') {
                return '洗衣账户'
            }
        }
    },
    created() {
        this.getDetail()

    }

} 