import allList from "./all_list/all_list.vue";
import sleeping from "./sleeping/sleeping.vue";
import servering from "./servering/servering.vue";
import { paidanObj } from '@/api/shop/order/housekeeping.js'
export default {
    components: {
        allList,
        sleeping,
        servering
    },
    data() {
        return {
            dialogVisible: false,
            dialogVisibleTile: "",
            loading: false,
            ruleForm: {},
            keyName: "1",
            tab: [
                {
                    label: "全部",
                    value: "1"
                },
                {
                    label: "休息中",
                    value: "2"
                }, {
                    label: "服务中",
                    value: "3"
                }
            ]
        };
    },
    methods: {
        getList(title, data) {
            this.dialogVisible = true
            this.dialogVisibleTile = title
            this.ruleForm = data
            console.log(this.ruleForm)
            this.$refs.allList.getList(this.ruleForm) //全部
            this.$refs.sleeping.getList(this.ruleForm) //全部
            this.$refs.servering.getList(this.ruleForm) //全部
        },
        callback(key) {
            this.keyName = key
            switch (this.keyName) {
                case '1':
                    this.$refs.allList.getList(this.ruleForm) //全部
                    break;
                case '2':
                    this.$refs.sleeping.getList(this.ruleForm) //休息中
                    break;
                case '3':
                    this.$refs.servering.getList(this.ruleForm) //服务中
                    break;
                default:
                    break;
            }
        },
        handleClose() {
            this.dialogVisible = false
            this.ruleForm = {}
        },
        handleSure() {
            this.loading = true
            this.dialogVisible = false
            paidanObj(Object.assign({ personId: '313' }, { subOrderId: this.ruleForm.subOrderId })).then(()=>{
                this.$message.success('派单成功')
                this.loading = false
            }).catch(()=>{
                this.$message.error('派单失败')
                this.loading = false
            })
        },

    }
};