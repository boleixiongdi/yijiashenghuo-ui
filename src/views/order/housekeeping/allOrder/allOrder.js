import { tableOption } from './allOrder.config.js'
import search from './../search/search.js';
import dispatch from './../dispatch/index.vue';  //派单
import { fetchList, refund } from '@/api/shop/order/housekeeping.js'
export default {
    name: "allOrder",
    mixins: [search],
    components: {
        dispatch
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            dialogVisible: false,
            ruleForm: {
                payOrderNo: '',
                paySerials: [],
                refundReason: []
            },
            rules: {
                refundReason: [
                    { required: true, message: '请选择退款原因', trigger: 'change' }
                ]
            },
            loadingSure: false,
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        details(item) {
            this.$router.push({ path: '/order/housekeeping/details', query: { subOrderId: item.subOrderId } })
        },
        dispatch(item) {
            this.$refs.dispatchRef.getList("派单", item)
        },
        tuihuo(item) {
            this.dialogVisible = true;
            // this.ruleForm.refundAmt = item.orderAmt;
            // let num = 0
            // item.paySerials.forEach(item => {
            //     num += item.payAmt
            // });
            // this.ruleForm.refundAmt = num
            // console.log(333, this.ruleForm.refundAmt)
            this.ruleForm.refundAmt = item.payAmt
            this.ruleForm.payOrderNo = item.payOrderNo;
            this.ruleForm.subOrderId = item.subOrderId;
            this.ruleForm.paySerials = item.paySerials;
            this.ruleForm.refundReason = []
            console.log(666, item)
        },
        handlSure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    let obj = { ...this.ruleForm }
                    obj.refundReason = obj.refundReason.join(',')
                    this.loadingSure = true
                    refund(obj).then(val => {
                        this.$message.success('退款成功'); this.dialogVisible = false
                        this.loadingSure = false

                    }).catch(() => {
                        this.$message.warning('退款失败');this.dialogVisible = false
                        this.loadingSure = false

                    })
                } else {
                    return false;
                }
            });
        }
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 