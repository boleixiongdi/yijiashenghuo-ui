const DIC = {
    status: [{
        label: '支付成功',
        value: '00'
    }, {
        label: '支付失败',
        value: '01'
    }, {
        label: '待支付',
        value: '02'
    }, {
        label: '支付中',
        value: '03'
    }, {
        label: '作废',
        value: '04'
    }],
    

}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth:300,
    column: [
        {
            label: '订单编号',
            prop: 'subOrderId',

            width: 120
        },
        {
            label: '商品数量',
            prop: 'sl',

            width: 120

        },
        {
            label: '会员',
            prop: 'personName',
            width: 120
        },
        {
            label: '服务时间',
            prop: 'serviceDate',
            width: 120
        },{
            label: '服务地址',
            prop: 'receiveAdress',
            width: 120
        },{
            label: '订单金额（元）',
            prop: 'orderAmt',
           

            width: 120
        }, {
            label: '支付方式及金额',
            prop: 'paySerials',
            slot:true,

            width: 150
        }, {
            label: '订单来源',
            prop: 'orderSource',

            width: 120
        }, {
            label: '所属门店',
            prop: 'companyName',

            width: 120
        }, {
            label: '创建时间',
            prop: 'orderTime',

            width: 120
        }, {
            label: '订单状态',
            prop: 'orderStatus',
            dicData: DIC.status,
            width: 120
        },
    ]
}

