import { tableOption } from './openShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import {
    getSupplierOutStockDetail
} from "@/api/shop/finance/SupplierReport.js";
export default {
    name: "openShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            daiRuKu: 0,
            selectedRows: [],//选中的数组
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            getSupplierOutStockDetail(Object.assign({},  this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        details(item) {
            let obj = { supplierId: item.supplierId }
            this.$router.push({ path: '/finance/SupplierReport/SupplierReportDetails', query: obj })
        },
    }

} 