import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import openShop from "./openShop/openShop.vue";
import comTitle from "@/views/com/com_title.vue";
import { mapGetters } from 'vuex'
const tabName = [
    "待入库",
    "待入库",
]
export default {
    mixins: [search],
    components: {
        allShop,
        openShop,
        comTitle,
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "待入库",
                openShop: "待入库",
            },
            dialogVisible: false,
            loadingSure: false,

            ruleForm: {

            },
            mendian: [],  //选择的门店
            mendianList: [],   //门店list
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback(key) {
            // this.keyName = key
            console.log(this.keyName)
            switch (this.keyName) {
                case '1':
                    this.$refs.allShop.getList() //待配单列表
                    break;
                case '2':
                    this.$refs.openShop.getList() //待配送列表
                    break;
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allShop.getList()       //待配单列表
            this.$refs.openShop.getList()  //待配送列表
        },
        trigger() {
            this.tab.allShop = `${tabName[0]}    ${this.$refs.allShop.daiRuKu}`
            this.tab.openShop = `${tabName[1]}    ${this.$refs.openShop.daiRuKu}`
        },
        addMendian() {
            this.dialogVisible = true
            this.mendian = []   //取出门店
            this.mendianList = []
            if (this.keyName == "1") {
                //采购入库
                this.$refs['allShop'].selectedRows.forEach(i => {
                    this.mendian.push(i.id)
                    this.mendianList.push({ label: i.name, value: i.id })
                })
                this.addStore()
            } else if (this.keyName == "2") {
                //门店退货入库
                this.$refs['openShop'].selectedRows.forEach(i => {
                    this.mendian.push(i.id)
                    this.mendianList.push({ label: i.name, value: i.id })
                })
                this.addStore()
            }
        },
        drShangpin() {
            if (this.keyName == "1") {
                this.$refs.allShop.getList()       //待配单列表
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //待配送列表
            }
        },
        handlClose() {
            this.dialogVisible = false
        },
        handlSure() {
            console.log('选择门店', this.mendian)
            console.log('门店采购商品明细', this.$refs['buyingIfor'].selectedRows)
        },
        addStore() {  //创建平台采购单
            this.$router.push({ path: "/platformProcurementInventory/platformStockInList/addInOrder", query: { keyName: this.keyName } })
        },
    }
}