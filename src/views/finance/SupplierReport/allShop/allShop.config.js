export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '供应商名称',
            prop: 'supplierName',

        },
        {
            label: '配送数量',
            prop: 'goodsNum',
        },
        {
            label: '采购金额/元',
            prop: 'purchaseInfoAmt',
        },
    ]
}

