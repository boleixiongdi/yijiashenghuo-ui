import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getSearchList() {
            if (this.keyName == '1') {
                this.$refs.allShop.getList(this.searchForm, 1)         //全部
            } else if (this.keyName == '2') {
                this.$refs.openShop.getList(this.searchForm, 1)     //营业中
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },

    }

} 