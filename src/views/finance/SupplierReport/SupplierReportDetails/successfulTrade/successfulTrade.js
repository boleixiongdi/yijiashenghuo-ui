import { tableOption } from './successfulTrade.config.js'
import search from './../search/search.js';
import {
    getSupplierPurchaseRejected
} from "@/api/shop/finance/SupplierReport.js";

export default {
    name: "successfulTrade",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            getSupplierPurchaseRejected(Object.assign({ supplierId: this.$route.query.supplierId }, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    }

} 