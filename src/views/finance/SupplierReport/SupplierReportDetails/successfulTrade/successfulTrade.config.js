export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu:false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '采购退货单号',
            prop: 'rejectedInfoId',
        },
        {
            label: '状态',
            prop: 'status',
            dicData: [
                {
                    label: "待确认退货",
                    value: "01"
                },
                {
                    label: "确认退货完成",
                    value: "02"
                }
            ]
        },
        {
            label: '退货时间',
            prop: 'confirmTime',
        },
        {
            label: '退货总金额(元)',
            prop: 'rejectedGoodsAmt',
        },
        {
            label: '退货数量',
            prop: 'rejectedNum',
        },
    ]
}

