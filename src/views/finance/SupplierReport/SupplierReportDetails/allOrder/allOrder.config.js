export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu:false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '配送单号',
            prop: 'distributionInfoId',
        },
        {
            label: '状态',
            prop: 'status',
            dicData: [
                {
                    label: "配送中",
                    value: "01"
                },
                {
                    label: "配送完成",
                    value: "02"
                }
            ]
        },
        {
            label: '配送时间',
            prop: 'distributionTime',
        },
        {
            label: '采购总金额(元)',
            prop: 'purchaseInfoAmt',
        },
        {
            label: '配送数量',
            prop: 'goodsNum',
        },
        {
            label: '关联采购单',
            prop: 'purchaseInfoId',
        },
    ]
}

