import { tableOption } from './allOrder.config.js'
import search from './../search/search.js';
import {
    getDistributeListBySupplierId
} from "@/api/shop/finance/SupplierReport.js";

export default {
    name: "allOrder",
    mixins: [search],
    components: {

    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            getDistributeListBySupplierId(Object.assign({supplierId:this.$route.query.supplierId},  this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = ''
                    break;
            }
            return obj
        }

    },

} 