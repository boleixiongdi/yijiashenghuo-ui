import search from "./search/search.js";
import allOrder from "./allOrder/allOrder.vue";
import successfulTrade from "./successfulTrade/successfulTrade.vue";

const tabName = [
    "配送单",
    "采购退货",
]
export default {
    mixins: [search],
    components: {
        allOrder,
        successfulTrade,
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allOrder: "配送单",
                successfulTrade: "采购退货",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList() //配送单
                    break;
                case '2':
                    this.$refs.successfulTrade.getList() //采购退货
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.allOrder.getList()       //配送单
            this.$refs.successfulTrade.getList() //采购退货
        },
    }
}