import { tableOption } from './list_table.config.js'
import search from '../search/search.js';
import { mapGetters } from 'vuex'
import { getSalesOutStockDetail } from '@/api/shop/finance/salesDataReport.js'
export default {
    name: "list_table",
    mixins: [search],

    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [{
                zt: 0
            }, {
                zt: 1
            }],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                // {
                //     title: "导出",
                //     isShow: true
                // }
            ],
            searchForm: {}
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            getSalesOutStockDetail(Object.assign({}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current,
                };
                this.loadData = data.records
                console.log(this.loadData)
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
    }

} 