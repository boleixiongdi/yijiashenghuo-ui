const DIC = {
    status: [
        {
            label: "支付成功",
            value: "00"
        },
        {
            label: "支付失败",
            value: "01"
        },
        {
            label: "待支付",
            value: "02"
        }
        ,
        {
            label: "支付中",
            value: "03"
        }
        ,
        {
            label: "支付关闭、作废",
            value: "04"
        }
        ,
        {
            label: "退款中",
            value: "05"
        }
        ,
        {
            label: "退款完成",
            value: "06"
        }
        ,
        {
            label: "退款失败",
            value: "07"
        }
        ,
        {
            label: "待派单",
            value: "08"
        }
        ,
        {
            label: "服务中",
            value: "09"
        }
        ,
        {
            label: "已拒单",
            value: "10"
        },
        {
            label: "待接单",
            value: "11"
        }
    ],
    businessType: [
        {
            label: '通用',
            value: '01'
        },
        {
            label: '商超',
            value: '02'
        }, {
            label: '洗衣',
            value: '03'
        }, {
            label: '家政',
            value: '04'
        }, {
            label: '咖吧',
            value: '05'
        }],

}

export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: '订单号',
            prop: 'subOrderId',

        },
        {
            label: '创建时间',
            prop: 'createTime',

        },
        {
            label: '销售渠道',
            prop: 'orderSource',
            dicData: [
                {
                    label: "小程序",
                    value: "00"
                },
                {
                    label: "APP",
                    value: "01"
                },
                {
                    label: "线下",
                    value: "02"
                }
            ]

        },
        {
            label: '所属业态',
            prop: 'businessType',
            dicData: DIC.businessType,

        },
        {
            label: '所属门店',
            prop: 'shopName',


        }, 
        // {
        //     label: '所属代理商/分公司',
        //     prop: 'supplier',
        // }, 
        {
            label: '订单总金额',
            prop: 'subOrderAmt',

        },
        {
            label: '订单实收金额',
            prop: 'subPayAmt',

        },
        {
            label: '优惠金额',
            prop: 'subDiscountAmt',

        },
        {
            label: '商品金额',
            prop: 'goodAmt',

        },
        // {
        //     label: '配送费',
        //     prop: 'businessType4',

        // },
        // {
        //     label: '服务人员',
        //     prop: 'personName',

        // },

        // {
        //     label: '支付流水号',
        //     prop: 'subOrderId',

        // },
        // {
        //     label: '支付明细',
        //     prop: 'businessType43',

        // },
        {
            label: '支付时间',
            prop: 'orderTime',

        }, {
            label: '支付状态',
            prop: 'subOrderStatus',
            dicData: DIC.status

        },
        //  {
        //     label: '通用储值卡消费',
        //     prop: 'platC',

        // }, {
        //     label: '业态储值卡消费',
        //     prop: 'otherC',

        // }, {
        //     label: '微信',
        //     prop: 'wx',

        // }, {
        //     label: '支付宝',
        //     prop: 'ali',

        // },
        // {
        //     label: '现金',
        //     prop: 'h',

        // },
        // {
        //     label: '订单状态',
        //     prop: 'subOrderStatus',
        //     dicData: DIC.status
        // },
        {
            label: '是否退货',
            prop: 'refundStatus',
            slot: true

        }, {
            label: '退款编号',
            prop: 'subRefundOrderId',

        }, {
            label: '退款时间',
            prop: 'subRefundTime',

        }, {
            label: '退款金额',
            prop: 'subRefundAmt',

        }



    ]
}

