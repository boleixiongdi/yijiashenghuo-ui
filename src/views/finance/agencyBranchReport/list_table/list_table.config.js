const DIC = {
    status: [{
        label: '启用',
        value: '00'
    }, {
        label: '停用',
        value: '01'
    }],
    businessType: [
    //     {
    //     label: '全部业务',
    //     value: '01'
    // },
     {
        label: '商超',
        value: '02'
    }, {
        label: '洗衣',
        value: '03'
    }, {
        label: '家政',
        value: '04'
    }, {
        label: '咖吧',
        value: '05'
    }],

}

export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '名称',
            prop: 'deptName',

        },
        {
            label: '管辖门店数',
            prop: 'shopNum',

        },
        {
            label: '实收金额/元',
            prop: 'actAmt',


        },
        {
            label: '商超分润比例',
            prop: 'marketProportion',

        },
        {
            label: '洗衣分润比例',
            prop: 'washProportion',

        },
        {
            label: '咖吧分润比例',
            prop: 'coffeeProportion',

        },
        {
            label: '家政分润比例',
            prop: 'houshProportion',

        },
        {
            label: '总分润/元',
            prop: 'totalAmt',
        },
        {
            label: '储值卡充值',
            prop: 'chargeAmt',
        },
        {
            label: '储值卡消费',
            prop: 'consumeAmt',

        },
        {
            label: '商超实收金额/元',
            prop: 'marketAmt',

        },
        {
            label: '洗衣实收金额/元',
            prop: 'washAmt',

        },
        {
            label: '咖吧实收金额/元',
            prop: 'coffeeAmt',

        },
        {
            label: '家政实收金额/元',
            prop: 'houseAmt',

        },


    ]
}

