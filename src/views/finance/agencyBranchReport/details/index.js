import { tableOption } from './details.config.js'
import search from '../search/search.js';
import { mapGetters } from 'vuex'
import { company } from '@/api/shop/finance/agencyBranchReport.js'

export default {
    name: "details",
    mixins: [search],

    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "导出",
                    isShow: true
                }
            ],
            searchForm:{}
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            company(Object.assign({ merchantId: this.$route.query.merchantId }, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                console.log(this.loadData)
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        edit(item) {  //编辑
            this.$router.push({
                path: '/finance/agencyBranchReport/salesDetail',
                query: { shopId: item.shopId }
            })

        },
    }

} 