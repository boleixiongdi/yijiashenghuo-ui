const DIC = {
    status: [{
        label: '启用',
        value: '00'
    }, {
        label: '停用',
        value: '01'
    }],
    businessType: [
    //     {
    //     label: '全部业务',
    //     value: '01'
    // },
     {
        label: '商超',
        value: '02'
    }, {
        label: '洗衣',
        value: '03'
    }, {
        label: '家政',
        value: '04'
    }, {
        label: '咖吧',
        value: '05'
    }],

}

export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    index: true,
    indexLabel: '序号',
    column: [
        {
            label: '',
            children: [{
                label: '门店名称',
                prop: 'shopName',

            },
            {
                label: '实收金额/元',
                prop: 'allAmt',

            },]
        },
        {
            label: '储值卡充值',
            children: [{
                label: '通用充值',
                prop: 'platC',


            },
            {
                label: '商超充值',
                prop: 'mallC',

            },
            {
                label: '洗衣充值',
                prop: 'launC',


            }, {
                label: '家政充值',
                prop: 'houseC',
            }, {
                label: '咖吧充值',
                prop: 'coffeeC',

            },]
        },
        {
            label: '商超',
            children: [{
                label: '实收金额/元',
                prop: 'mall',

            },
            {
                label: '通用消费',
                prop: 'mallPsnAmt',

            },
            {
                label: '储值卡消费',
                prop: 'mallPscAmt',

            },]
        },
        {
            label: '家政',
            children: [{
                label: '家政实收金额/元',
                prop: 'housekeep',

            },
            {
                label: '通用消费',
                prop: 'housekeepPsnAmt',

            }, {
                label: '家政储值卡消费',
                prop: 'housekeepPscAmt',

            }]
        },
        {
            label: '洗衣',
            children: [{
                label: '洗衣实收金额/元',
                prop: 'laundry',

            }, {
                label: '通用消费',
                prop: 'laundryPsnAmt',

            }, {
                label: '洗衣储值卡消费',
                prop: 'laundryPscAmt',

            },]
        },
        {
            label: '咖吧',
            children: [{
                label: '咖吧实收金额/元',
                prop: 'businessType433',
            },
            {
                label: '通用消费',
                prop: 'coffeePsnAmt',

            }, {
                label: '咖吧储值卡消费',
                prop: 'coffeePscAmt',

            },]
        },

    ]
}

