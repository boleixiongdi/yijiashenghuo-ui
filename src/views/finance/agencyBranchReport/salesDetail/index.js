import { tableOption } from './details.config.js'
import search from '../search/search.js';
import { mapGetters } from 'vuex'
import { shopsale } from '@/api/shop/finance/agencyBranchReport.js'
export default {
    name: "details",
    mixins: [search],

    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "导出",
                    isShow: true
                }
            ],
            searchForm: {},
            xsleList: [
                // {
                //     label: '全部业务',
                //     value: '01'
                // }, 
                {
                    label: '商超',
                    value: '02'
                }, {
                    label: '洗衣',
                    value: '03'
                }, {
                    label: '家政',
                    value: '04'
                }, {
                    label: '咖吧',
                    value: '05'
                }
            ]
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            shopsale(Object.assign({ shopId: this.$route.query.shopId }, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                })
                console.log(this.loadData)
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
    }

} 