const DIC = {
    status: [{
        label: '启用',
        value: '00'
    }, {
        label: '停用',
        value: '01'
    }],
    businessType: [
    //     {
    //     label: '全部业务',
    //     value: '01'
    // }, 
    {
        label: '商超',
        value: '02'
    }, {
        label: '洗衣',
        value: '03'
    }, {
        label: '家政',
        value: '04'
    }, {
        label: '咖吧',
        value: '05'
    }],

}

export const tableOption = {
    // header: false,
    refreshBtn:false,
    excelBtn:true,
    addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    index: true,
    menu:false,
    indexLabel: '序号',
    column: [
        {
            label: '订单号',
            prop: 'subOrderId',

        },
        {
            label: '创建时间',
            prop: 'createTime',

        },
        // {
        //     label: '销售渠道',
        //     prop: 'allAmt',
        // },
        {
            label: '所属业态',
            prop: 'businessType1',
        },
        {
            label: '所属门店',
            prop: 'shopName',

        }, {
            label: '所属代理商/分公司',
            prop: 'businessType33',
        }, {
            label: '订单总金额',
            prop: 'orderAmt',

        },
        {
            label: '订单实收金额',
            prop: 'businessType1',

        },
        {
            label: '优惠金额',
            prop: 'discountAmt',

        },
        {
            label: '商品金额',
            prop: 'goodsAmt',

        },
        // {
        //     label: '配送费',
        //     prop: 'businessType4',

        // },
        // {
        //     label: '服务人员',
        //     prop: 'operatorId',

        // },

        // {
        //     label: '支付流水号',
        //     prop: 'paySerialNo',

        // },
        {
            label: '支付明细',
            prop: 'businessType43',

        },
        {
            label: '支付时间',
            prop: 'orderTime',

        },{
            label: '支付状态',
            prop: 'payStatus',

        },{
            label: '通用储值卡消费',
            prop: 'otherC',

        },{
            label: '业态储值卡消费',
            prop: 'platC',

        },{
            label: '微信',
            prop: 'wx',

        },{
            label: '支付宝',
            prop: 'ali',

        },{
            label: '现金',
            prop: 'h',

        },{
            label: '订单状态',
            prop: 'orderStatus',

        },{
            label: '是否退货',
            prop: 'refundStatus',

        },{
            label: '退款编号',
            prop: 'refundOrderId',

        },{
            label: '退款时间',
            prop: 'refundCreateTime',

        },{
            label: '退款金额',
            prop: 'refundAmt',

        }

       

    ]
}

