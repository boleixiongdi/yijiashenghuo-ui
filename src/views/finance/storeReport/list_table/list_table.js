import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { shopCheck } from '@/api/shop/finance/storeReport.js'
export default {
    name: "list_table",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "导出",
                    isShow: true
                }
            ],
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            shopCheck(Object.assign({}, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.loadData = data.records
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current,
                };
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.export1()
            }
        },
        // 删除
        delete() {
            console.log(123321)
        },
        details(item) {
            this.$router.push({ path: '/finance/storeReport/storeReportDetails', query: { shopId: item.shopId, beginTime: this.searchForm.beginTime, endTime: this.searchForm.endTime } })
        },
        export1() {

        }
    }

} 