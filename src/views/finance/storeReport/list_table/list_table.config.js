export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: '',
            children: [{
                label: '门店名称',
                prop: 'shopName',

            },
            ]
        },
        {
            label: '订单销售',
            children: [{
                label: '总营收/元',
                prop: 'orderAmt',


            },
            {
                label: '实收金额',
                prop: 'payAmt',

            },
            {
                label: '微信实收',
                prop: 'wxAmt',

            },
            {
                label: '支付宝实收',
                prop: 'aliAmt',

            },
            {
                label: '现金实收',
                prop: 'cashAmt',
            },
            {
                label: '储值卡实收',
                prop: 'scAmt',
            },
            {
                label: '成本总计',
                prop: 'costAmt',


            }, {
                label: '优惠券费用/元',
                prop: 'discountAmt',
            },]
        },
        {
            label: '出入库数据',
            children: [{
                label: '入库采购总成本',
                prop: 'instockCostAmt',

            },
            {
                label: '退货出库成本',
                prop: 'outStockAmt',

            },
            ]
        },
        {
            label: '储值卡信息',
            children: [{
                label: '储值卡充值',
                prop: 'inAmt',

            },
            {
                label: '储值卡消费',
                prop: 'camt',

            }]
        },


    ]
}