export const tableOption = {
     // header: false,
     refreshBtn:false,
     excelBtn:true,
     addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '单据编号',
            prop: 'stockId',
        },
        {
            label: '创建时间',
            prop: 'createTime',
        },
        {
            label: '单据类型',
            prop: 'source',
            dicData: [
                {
                    label: "入库",
                    value: "01"
                },
                {
                    label: "出库",
                    value: "02"
                }
            ]
        },
        {
            label: '入库成本金额',
            prop: 'inCost',


        },
        {
            label: '出库成本金额',
            prop: 'outCost',


        },
        {
            label: '创单人',
            prop: 'operator',


        }, {
            label: '配送方',
            prop: 'supply',
            dicData: [
                {
                    label: "仓库",
                    value: "01"
                },
            ]

        }
    ]
}

