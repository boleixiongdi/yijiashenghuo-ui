import search from "./search/search.js";
import allOrder from "./allOrder/allOrder.vue";
import successfulTrade from "./successfulTrade/successfulTrade.vue";
import servering from "./servering/servering.vue";

const tabName = [
    "门店销售",
    "储值卡充值",
    "门店出入库"
]
export default {
    mixins: [search],
    components: {
        allOrder,
        successfulTrade,
        servering
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allOrder: "门店销售",
                successfulTrade: "储值卡充值",
                servering: "门店出入库",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList() //门店销售
                    break;
                case '2':
                    this.$refs.successfulTrade.getList() //储值卡充值
                    break;
                case '3':
                    this.$refs.servering.getList() //门店出入库
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.allOrder.getList()       //门店销售
            this.$refs.successfulTrade.getList() //储值卡充值
            this.$refs.servering.getList() //门店出入库
        },
    }
}