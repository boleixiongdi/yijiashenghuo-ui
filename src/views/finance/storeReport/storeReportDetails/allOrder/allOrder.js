import { tableOption } from './allOrder.config.js'
import search from './../search/search.js';
import { shopsale } from '@/api/shop/finance/storeReport.js'
export default {
    name: "allOrder",
    mixins: [search],
    components: {

    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            xsleList: [
                // {
                //     label: '全部业务',
                //     value: '01'
                // }, 
                {
                    label: '商超',
                    value: '02'
                }, {
                    label: '洗衣',
                    value: '03'
                }, {
                    label: '家政',
                    value: '04'
                }, {
                    label: '咖吧',
                    value: '05'
                }
            ]
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            shopsale(Object.assign({ shopId: this.$route.query.shopId,beginTime:this.$route.query.beginTime,endTime:this.$route.query.endTime }, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.loadData = data.records
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current,
                };
                this.loadData.forEach(item => {
                    if (item.businessType) {
                        item.businessType3 = []
                        item.businessType2 = item.businessType.split(',')
                        item.businessType2.forEach(i => {
                            this.$store.getters.businessTypeList.forEach(m => {
                                if (i == m.value) {
                                    item.businessType3.push(m.label)

                                }
                            })
                        })
                        item.businessType1 = item.businessType3.join(',')
                    }
                })
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 