const DIC = {
    status: [
        {
            label: "支付成功",
            value: "00"
        },
        {
            label: "支付失败",
            value: "01"
        },
        {
            label: "待支付",
            value: "02"
        }
        ,
        {
            label: "支付中",
            value: "03"
        }
        ,
        {
            label: "支付关闭、作废",
            value: "04"
        }
        ,
        {
            label: "退款中",
            value: "05"
        }
        ,
        {
            label: "退款完成",
            value: "06"
        }
        ,
        {
            label: "退款失败",
            value: "07"
        }
        ,
        {
            label: "待派单",
            value: "08"
        }
        ,
        {
            label: "服务中",
            value: "09"
        }
        ,
        {
            label: "已拒单",
            value: "10"
        },
        {
            label: "待接单",
            value: "11"
        }
    ],


}
export const tableOption = {
     // header: false,
     refreshBtn:false,
     excelBtn:true,
     addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 300,
    menu: false,
    column: [
        {
            label: '订单号',
            prop: 'subOrderId',
        },
        {
            label: '创建时间',
            prop: 'createTime',
        },
        {
            label: '所属业态',
            prop: 'businessType1',
        },
        {
            label: '订单总金额',
            prop: 'orderAmt',
        }, {
            label: '订单实收金额',
            prop: 'payAmt',
        },
        {
            label: '优惠金额',
            prop: 'discountAmt'
        },
        {
            label: '商品金额',
            prop: 'goodsAmt',
        }, {
            label: '配送费',
            prop: 'deliveryFee',
        }, {
            label: '服务人员',
            prop: 'operatorId',


        },
        // {
        //     label: '支付流水号',
        //     prop: 'companyName',


        // }, 
        {
            label: '支付状态',
            prop: 'orderStatus',
            dicData: DIC.status

        }, 
        // {
        //     label: '通用储值卡消费',
        //     prop: 'otherC',


        // },
        // {
        //     label: '业态储值卡消费',
        //     prop: 'platC',


        // },
        // {
        //     label: '微信',
        //     prop: 'wx',


        // },
        // {
        //     label: '支付宝',
        //     prop: 'ali',


        // },
        // {
        //     label: '现金',
        //     prop: 'order123Status',


        // },
        {
            label: '支付时间',
            prop: 'orderTime',


        },
        {
            label: '订单状态',
            prop: 'orderStatus',
            dicData: DIC.status

        },

    ]
}

