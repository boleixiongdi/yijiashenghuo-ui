const DIC = {
    status: [{
        label: '支付成功',
        value: '00'
    }, {
        label: '支付失败',
        value: '01'
    }, {
        label: '待支付',
        value: '02'
    }, {
        label: '支付中',
        value: '03'
    }, {
        label: '作废',
        value: '04'
    }],


}
export const tableOption = {
     // header: false,
     refreshBtn:false,
     excelBtn:true,
     addBtn:false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 300,
    menu:false,
    column: [
        {
            label: '流水号',
            prop: 'storedDetailedId',
        },
        {
            label: '支付时间',
            prop: 'createTime',
        },
        {
            label: '会员手机号',
            prop: 'phoneNum',
        },
        {
            label: '储值类别',
            prop: 'accountType',
            dicData: [
                {
                    label: "平台余额账户",
                    value: "ACC001"
                },
                {
                    label: "积分账户",
                    value: "ACC002"
                },
                {
                    label: "商超账户",
                    value: "ACC003"
                },
                {
                    label: "咖吧账户",
                    value: "ACC004"
                }, {
                    label: "家政账户",
                    value: "ACC005"
                },
                {
                    label: "洗衣账户",
                    value: "ACC006"
                }
            ]

        }, {
            label: '储值金额',
            prop: 'operationNum',
        },
        {
            label: '支付金额',
            prop: 'payAmt'
        },
        {
            label: '赠送金额',
            prop: 'givenAmout',
        }, {
            label: '支付方式',
            prop: 'payType',
            dicData: [
                {
                    label: "支付宝",
                    value: "01"
                },
                {
                    label: "微信",
                    value: "02"
                }
            ]
        }, {
            label: '充值渠道',
            prop: 'storedChannelType',
            dicData: [
                {
                    label: "线上",
                    value: "00"
                },
                {
                    label: "线下",
                    value: "01"
                }
            ]
        }
    ]
}

