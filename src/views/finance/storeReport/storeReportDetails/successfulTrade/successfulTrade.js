import { tableOption } from './successfulTrade.config.js'
import search from './../search/search.js';
import { shopCheckcharge } from '@/api/shop/finance/storeReport.js'
export default {
    name: "successfulTrade",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },

            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.currentPage = page
            }
            shopCheckcharge(Object.assign({ shopId: this.$route.query.shopId,beginTime:this.$route.query.beginTime,endTime:this.$route.query.endTime }, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.loadData = data.records
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current,
                };
                this.loadData.forEach(item => {
                    if (item.businessType) {
                        item.businessType3 = []
                        item.businessType2 = item.businessType.split(',')
                        item.businessType2.forEach(i => {
                            this.$store.getters.businessTypeList.forEach(m => {
                                if (i == m.value) {
                                    item.businessType3.push(m.label)

                                }
                            })
                        })
                        item.businessType1 = item.businessType3.join(',')
                    }

                })
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    }

} 