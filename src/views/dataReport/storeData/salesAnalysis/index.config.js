export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '门店名称',
            prop: 'shopName',
            overHidden:true,
        },
        {
            label: '支付金额/元',
            prop: 'orderAmt',
        },
        {
            label: '占比',
            prop: 'proportion',
        },
    ]
}
export const tableOption3 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '门店名称',
            prop: 'shopName',
            overHidden:true,

        },
        {
            label: '销售单量',
            prop: 'orderNum',
            overHidden:true,
        },
    ]
}
export const tableOption4 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '门店名称',
            prop: 'shopName',


        },
        {
            label: '营收/元',
            prop: 'revenueAmt',

        },
    ]
}

