export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '门店名称',
            prop: 'shopName',
        },
        {
            label: '所属管辖',
            prop: 'companyName',
        },
        {
            label: '总销售量',
            prop: 'orderNum',
        },
        {
            label: '支付营收/元',
            prop: 'orderPayAmt',
        }, 
        {
            label: '储值卡充值',
            prop: 'storedInNum',

        }, 
        {
            label: '储值卡消费',
            prop: 'storedOutNum',

        }, 
        {
            label: '商超营收',
            prop: 'scAmt'

        }, {
            label: '洗衣营收',
            prop: 'xyAmt'

        }, {
            label: '咖吧营收',
            prop: 'kfAmt'

        }, {
            label: '家政营收',
            prop: 'jzAmt'

        }, {
            label: '退款数',
            prop: 'refundOrderNum'

        }, {
            label: '退款金额',
            prop: 'refundAmt'

        }
    ]
}

