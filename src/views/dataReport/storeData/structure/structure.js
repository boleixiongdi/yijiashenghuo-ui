import { tableOption } from './structure.config.js'
import search from './../search/search.js';
import TrendChart from '@/components/dashboard/TrendChart.vue';
import { mapGetters } from 'vuex'
import { dataStatisticalByGroup, dataCenterShopInfotrend } from '@/api/shop/dataReport/storeData.js'
export default {
    name: "structure",
    mixins: [search],
    components: {
        TrendChart
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [

                // {
                //     title: "导出数据",
                //     isShow: true
                // },
            ],
            dialogVisible: false,   //趋势图
            dataList: ['销量', '支付营收', '支付均价'],
            dataX: [],
            dataValue: [
                {
                    name: '销量',
                    type: 'line',
                    stack: '总量',
                    data: []
                },
                {
                    name: '支付营收',
                    type: 'line',
                    stack: '总量',
                    data: []
                },
                {
                    name: '支付均价',
                    type: 'line',
                    stack: '总量',
                    data: []
                },
            ],
            searchForm: {},
            trendShow: false
        }
    },
    created() {
        // this.time()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            dataStatisticalByGroup(Object.assign({}, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.export()
            }
        },
        trend(item) {
            this.dialogVisible = true
            this.trendShow = false
            dataCenterShopInfotrend({shopId:item.shopId}).then(val => {
                console.log(val)
                let data = val.data.data
                this.dataValue[0].data = []
                this.dataValue[1].data = []
                this.dataValue[2].data = []
                this.dataX = []
                data.forEach(item => {
                    if (item.payGoodsNum == null) {
                        item.payGoodsNum = 0
                    }
                    if (item.subOrderAmt == null) {
                        item.subOrderAmt = 0
                    }
                    if (item.aver == null) {
                        item.aver = 0
                    }
                    this.dataValue[0].data.push(item.payGoodsNum)
                    this.dataValue[1].data.push(item.subOrderAmt)
                    this.dataValue[2].data.push(item.aver)
                    this.dataX.push(item.date.substring(0,11))
                })
                this.trendShow = true
            })
        },
        export() {

        },
        time() {
            var myDate = new Date(); //获取今天日期
            myDate.setDate(myDate.getDate() - 6);
            var dateArray = [];
            var dateTemp;
            var flag = 1;
            for (var i = 0; i < 7; i++) {
                dateTemp = myDate.getMonth() + 1 + "-" + myDate.getDate();
                dateArray.push(dateTemp);
                myDate.setDate(myDate.getDate() + flag);
            }
            this.dataX = dateArray;
        }
    }

} 