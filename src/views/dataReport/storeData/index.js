import search from "./search/search.js";
import salesAnalysis from "./salesAnalysis/index.vue";
import structure from "./structure/structure.vue";
import region from "./region/region.vue";

const tabName = [
    "销量分析",
    "按组织架构",
    "按区域"
]
export default {
    mixins: [search],
    components: {
        salesAnalysis,
        structure,
        region
    },
    data() {
        return {
            keyName: "1",
            tab: {
                salesAnalysis: "销量分析",
                structure: "按组织架构",
                region: "按区域"
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.salesAnalysis.getList() //销量分析列表
                    break;
                case '2':
                    this.$refs.structure.getList() //按组织架构列表
                    break;
                case '3':
                    this.$refs.region.getList() //按组织架构列表
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.salesAnalysis.getList()       //销量分析列表
            this.$refs.structure.getList()  //按组织架构列表
            this.$refs.region.getList()
        },
    }
}