import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getSearchList() {
            if (this.keyName == '1') {
                this.$refs.salesAnalysis.getList(this.searchForm, 1)         //审核通过列表
            } else if (this.keyName == '2') {
                this.$refs.structure.getList(this.searchForm, 1)     //待提交列表
            }else if (this.keyName == '3') {
                this.$refs.region.getList(this.searchForm, 1)     //待提交列表
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },

    }

} 