import search from "./search/search.js";
import salesAnalysis from "./salesAnalysis/index.vue";
import SalesDetails from "./SalesDetails/SalesDetails.vue";

const tabName = [
    "销量分析",
    "商品销售明细",
]
export default {
    mixins: [search],
    components: {
        salesAnalysis,
        SalesDetails
    },
    data() {
        return {
            keyName: "1",
            tab: {
                salesAnalysis: "销量分析",
                SalesDetails: "商品销售明细",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.salesAnalysis.getList() //销量分析列表
                    break;
                case '2':
                    this.$refs.SalesDetails.getList() //商品销售明细列表
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.salesAnalysis.getList()       //销量分析列表
            this.$refs.SalesDetails.getList()  //商品销售明细列表
        },
    }
}