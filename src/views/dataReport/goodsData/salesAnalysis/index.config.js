export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu:false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '商品类目',
            prop: 'category',
            overHidden:true,
        },
        {
            label: '支付金额/元',
            prop: 'amt',
            overHidden:true,
        },
        {
            label: '占比',
            prop: 'percent',
            overHidden:true,
        },
    ]
}
export const tableOption3 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu:false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            overHidden:true,

        },
        {
            label: '销售单量',
            prop: 'num',
            overHidden:true,
        },
    ]
}
export const tableOption4 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu:false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            overHidden:true,
        },
        {
            label: '营收/元',
            prop: 'amt',
            overHidden:true,
        },
    ]
}

