export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',

        },
        {
            label: '总销售量',
            prop: 'saleNum',


        },
        {
            label: '支付营收/元',
            prop: 'saleAmt',

        },
        {
            label: '支付均价',
            prop: 'aver',


        }, {
            label: '退款数',
            prop: 'refundNum',

        }, {
            label: '退款金额',
            prop: 'refundAmt'

        }
    ]
}

