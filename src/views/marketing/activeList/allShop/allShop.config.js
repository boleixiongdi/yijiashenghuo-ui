export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    menuWidth: 200,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '券活动ID',
            prop: 'couponId',
            width:150,
            overHidden: true,
        },
        {
            label: '优惠券名称',
            prop: 'couponName',
            width:150,
            overHidden: true,
        },
        {
            label: '活动时间',
            prop: 'activityTime',
            width:200,
            overHidden: true,

        },
        {
            label: '所属业务',
            prop: 'businessType1',
            width:150,
            overHidden: true,
        },
         {
            label: '作用范围',
            prop: 'saleScope',
            slot: true

        }, 
        {
            label: '优惠内容',
            prop: 'activityDec',
            width:150,
            overHidden: true,

        }, {
            label: '券有效期',
            prop: 'couponTime',
            // slot:true
            width:200,
            overHidden: true,
        },
        // {
        //     label: '活动状态',
        //     prop: 'createTime',
        //     dicData:[
        //         {
        //             label:"",
        //             value:""
        //         }
        //     ]
        // },
    ]
}

