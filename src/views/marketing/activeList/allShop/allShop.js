import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { couponinfopage, changeActStatus,delObj, } from '@/api/marketing/activeList.js'

export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            searchForm: {},
            xsleList: [  //业态
                {
                    label: "商超,咖吧,洗衣,家政",
                    value: "01"
                },
                {
                    label: "商超",
                    value: "02"
                },
                {
                    label: "咖吧",
                    value: "03"
                },
                {
                    label: "洗衣",
                    value: "04"
                },
                {
                    label: "家政",
                    value: "05"
                }
            ],
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            this.loading = true
            couponinfopage(Object.assign({ activityStatus: '04' }, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                
                data.records.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')

                    item.activityTime = (item.activityBegintime ? item.activityBegintime + ' - ' : '') + (item.activityEndtime ? item.activityEndtime : '')

                    item.couponTime = (item.couponBegintime ? item.couponBegintime + ' - ' : '') + (item.couponEndtime ? item.couponEndtime : '')
                })
                this.loadData = data.records
                console.log(this.loadData)
                this.$emit("trigger")
                this.loading = false
            }).catch(() => {
                this.loading = false
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        
        details(item) {
            this.$router.push({path:'/marketing/activeList/couponDetails',query:{couponId:item.couponId}})
        },
        peidan(row) {
            this.$confirm('是否提前上线优惠券名称"' + row.couponName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(()=> {
                return changeActStatus({ id: row.id, changeActStatus: '00' })
            }).then(() => {
                this.$emit("trigger",1) //1重新加载其他tab请求
                this.$message.success('上线成功')
            })
        },
        addMendian() {
            this.$router.push({ path: "/marketing/activeList/addCoupon" })
        },
        // 删除
        del(row){
            var that = this
            this.$confirm('是否删除"' + row.couponName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                that.getList()
                that.$message.success('删除成功')
            })
        },
        edit(item){
            this.$router.push({path:'/marketing/activeList/addCoupon',query:{couponId:item.couponId}})
        }
    }

} 