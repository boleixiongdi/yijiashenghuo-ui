import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import openShop from "./openShop/openShop.vue";
import stopShop from "./stopShop/stopShop.vue";
import end from "./end/end.vue";
import close from "./close/close.vue";
import comTitle from "@/views/com/com_title.vue";

import { mapGetters } from 'vuex'
const tabName = [
    "未开始",
    "活动中",
    "活动结束",
    "提前下线",
    "查看审核"
]
export default {
    mixins: [search],
    components: {
        allShop,
        openShop,
        stopShop,
        end,
        close,
        comTitle,
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "未开始",
                openShop: "活动中",
                stopShop: "活动结束",
                end: "提前下线",
                close: "查看审核"
            },
            dialogVisible: false,
            loadingSure: false,

            ruleForm: {

            },
            mendian: [],  //选择的门店
            mendianList: [],   //门店list
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            console.log(this.searchForm)
            switch (this.keyName) {
                case '1':
                    this.$refs.allShop.getList(this.searchForm, 1) //未开始列表
                    break;
                case '2':
                    this.$refs.openShop.getList(this.searchForm, 1) //活动中列表
                    break;
                case '3':
                    this.$refs.stopShop.getList(this.searchForm, 1) //活动结束列表
                case '4':
                    this.$refs.end.getList(this.searchForm, 1) //提前下线列表
                case '5':
                    this.$refs.close.getList(this.searchForm, 1) //查看审核列表
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allShop.getList(this.searchForm, 1)       //未开始列表
            this.$refs.openShop.getList(this.searchForm, 1)  //活动中列表
            this.$refs.stopShop.getList(this.searchForm, 1) //活动结束列表
            this.$refs.end.getList(this.searchForm, 1) //提前下线列表
            this.$refs.close.getList(this.searchForm, 1) //查看审核列表
        },
        trigger(st) {
            if (st) {
                this.getList()
            } else {
                this.tab.allShop = `${tabName[0]}(${this.$refs.allShop.page.total})`
                this.tab.openShop = `${tabName[1]}(${this.$refs.openShop.page.total})`
                this.tab.stopShop = `${tabName[2]}(${this.$refs.stopShop.page.total})`
                this.tab.end = `${tabName[3]}(${this.$refs.end.page.total})`
                this.tab.close = `${tabName[4]}(${this.$refs.close.page.total})`
            }
            this.$forceUpdate()
        },
    }
}