import { tableOption } from './buyingIfor.config.js'

export default {
    name: "buyingIfor",
    props: {
        loadData: {
            type: [Object, Array],
            default: () => { },
        },
        number: {
            type: Number,
            default: 0
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
        }
    },
    methods: {

    }

} 