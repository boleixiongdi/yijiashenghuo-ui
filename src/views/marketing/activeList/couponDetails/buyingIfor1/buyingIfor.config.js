export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu:false,
    column: [
        {
            label: '商品ID',
            prop: 'goodsId',
        },
        {
            label: '所属店铺',
            prop: 'shopName',
        },
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '价格(元)',
            prop: 'sellingPrice',
        },
        {
            label: '库存',
            prop: 'stockNum',
        },
    ]
}

