export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu:false,
    column: [
        {
            label: '门店ID',
            prop: 'shopId',
        },
        {
            label: '门店名称',
            prop: 'shopName',
        },
        {
            label: '所属业务',
            prop: 'businessType1',
        },
    ]
}

