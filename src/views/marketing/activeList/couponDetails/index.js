import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import buyingIfor1 from "./buyingIfor1/buyingIfor.vue";
import { couponInfoDetail } from '@/api/marketing/activeList.js'
export default {
    components: {
        comTitle,
        buyingIfor,
        buyingIfor1
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {

            },
            rules: {

            },
            shopPurchaseShopInfoDto: {},
            xsleList: [  //业态
                // {
                //     lebel: "全部业务",
                //     value: '01'
                // },
                {
                    label: "商超",
                    value: "02"
                },
                {
                    label: "咖吧",
                    value: "03"
                },
                {
                    label: "洗衣",
                    value: "04"
                },
                {
                    label: "家政",
                    value: "05"
                }
            ],
        }
    },
    filters: {
        saleScope(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '全渠道'
                    break;
                case '01':
                    obj = '线下'
                    break;
                case '02':
                    obj = '线上'
                    break;
            }
            return obj
        },
        activityStatus(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '正常'
                    break;
                case '01':
                    obj = '活动结束下线'
                    break;
                case '02':
                    obj = '提前下线'
                    break;
                case '03':
                    obj = '提前上线'
                    break;
            }
            return obj
        }

    },
    mounted() {
        if (this.$route.query.couponId) {
            this.getList(this.$route.query.couponId)
        }
    },
    methods: {
        getList(couponId) {
            couponInfoDetail({ couponId: couponId }).then(val => {

                let data = val.data.data
                this.ruleForm = data
                this.ruleForm.businessType3 = []
                this.ruleForm.businessType2 = this.ruleForm.businessType.split(',')
                this.ruleForm.businessType2.forEach(i => {
                    this.xsleList.forEach(m => {
                        if (i == m.value) {
                            this.ruleForm.businessType3.push(m.label)

                        }
                    })
                })
                this.ruleForm.businessType1 = this.ruleForm.businessType3.join(',')

                this.ruleForm.couponShopList.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                })

            })




        },
        save() {   //保存
            console.log(this.ruleForm)
            if (this.ruleForm.sptm == 0) {
                this.rules.sptm1[0].required = false; //商品条码
            } else {
                this.rules.sptm1[0].required = true;
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleCropperSuccess(data) {
            //图片裁切完成保存
            fileBase64(data).then(result => {
                this.imageUrl = result.result;
                this.fileList.push({
                    uid: this.number++,
                    name: "image.png",
                    status: "done",
                    url: this.imageUrl
                });
            });
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 