import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getSearchList() {
            if (this.keyName == '1') {
                this.$refs.allShop.getList(this.searchForm, 1)         //全部
            } else if (this.keyName == '2') {
                this.$refs.openShop.getList(this.searchForm, 1)     //营业中
            } else if (this.keyName == '3') {
                this.$refs.stopShop.getList(this.searchForm, 1)        //下线中
            } else if (this.keyName == '4') {
                this.$refs.end.getList(this.searchForm, 1)        //下线中
            }
            else if (this.keyName == '5') {
                this.$refs.close.getList(this.searchForm, 1)        //下线中
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },

    }

} 