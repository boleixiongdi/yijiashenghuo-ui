export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    column: [
        {
            label: '商品ID',
            prop: 'goodsId',
        },
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '所属门店',
            prop: 'shopName',
        },
        {
            label: '价格',
            prop: 'sellingPrice',
        },
        {
            label: '库存',
            prop: 'stockNum',
        },
        // {
        //     label: '满金额',
        //     prop: 'costPrice',
        // },
        // {
        //     label: '减免',
        //     prop: 'promotePrice',
        // },
    ]
}

