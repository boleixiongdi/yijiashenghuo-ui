import { tableOption } from './buyingIfor.config.js'
import { toDecimal2 } from '@/util/util'
import choiceGoods from './../choiceGoods.vue'
import { persongrade, addcouponinfo, couponInfoDetail,goodstockfetchList } from '@/api/marketing/activeList.js'
export default {
    name: "buyingIfor",
    components: {
        choiceGoods
    },

    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            total: { number: 0, price: 0 },
            dataList: [],
            stockUnitList:[]
        }
    },
    filters: {
        businessType(val) {
            let obj = ''
            switch (val) {
                case '02':
                    obj = '商超'
                    break;
                case '03':
                    obj = '洗衣'
                    break;
                case '04':
                    obj = '家政'
                    break;
                case '05':
                    obj = '咖吧'
                    break;
            }
            return obj
        }

    },
    mounted() {
        if (this.$route.query.couponId) {
            this.huiXianData(this.$route.query.couponId)
        }
        
    },
    methods: {
        huiXianData(couponId) {
            couponInfoDetail({ couponId: couponId }).then(val => {
                console.log(val)
                if (val.data.data.couponGoodsList) {
                    this.dataList = val.data.data.couponGoodsList
                    this.onConversionNumChange()
                }
                
            })
        },
        getList(list){
            goodstockfetchList().then(val => {  //库存单位
                let data = val.data.data
                data.records.forEach(item=>{
                    list.forEach(items=>{
                        if (item.stockUnitId == items.stockUnit) {
                            items.stockUnitName = item.stockUnitName
                        }
                    })
                })
                this.dataList = list
                
            })
            this.dataList = list
            console.log(this.dataList)
            
        },
        del(index) {
            let obj = [...this.dataList]
            obj.splice(index, 1)
            this.dataList = obj
            this.onConversionNumChange();
        },
        onConversionNumChange: function (e) {
            console.log("e", e);
            this.dataList.forEach(good => {
                good.conversionNum = parseInt(good.conversionNum);
                //计算转换数量
                good.goodsNum = good.conversionNum * good.convertUnitNum;
                //计算小计
                good.goodSubtotal = good.goodsNum * good.costPrice;
                console.log('good', good)
            })
            this.dataList = this.dataList
            this.$forceUpdate()
            this.cacaulateSum();
        },
        //计算商品数量 和总计金额
        cacaulateSum() {
            let sumAmt = 0
            this.dataList.forEach((ele) => {
                sumAmt += parseFloat(ele.goodSubtotal);
            })
            console.log(sumAmt)
            this.total.price = sumAmt
            console.log('cacaulateSum this.total', this.total)
        },
    }

} 