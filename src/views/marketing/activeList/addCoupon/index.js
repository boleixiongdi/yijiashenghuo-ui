import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import buyingIfor1 from "./buyingIfor1/buyingIfor.vue";

import choiceShop from "./choiceShop.vue";
import choiceGoods from "./choiceGoods.vue";

import { persongrade, addcouponinfo, couponInfoDetail, putObj } from '@/api/marketing/activeList.js'


export default {
    components: {
        comTitle,
        buyingIfor,
        buyingIfor1,
        choiceGoods,
        choiceShop,
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                receiveLimit: '',
                receiveNum: '',
                discount: '',
                // businessType: [],
                businessType: '02',
                types: '00'
            },
            rules: {
                couponName: [
                    { required: true, message: '请输入优惠券名称', trigger: 'blur' },
                ],
                couponNum: [
                    { required: true, message: '请输入发行总量', trigger: 'blur' },
                ],
                activityDec: [
                    { required: true, message: '请填写活动描述', trigger: 'blur' },
                ],
                activityBegintime: [
                    { required: true, message: '请选择领取时间', trigger: 'change' },
                ],
                couponBegintime: [
                    { required: true, message: '请选择用券有效期', trigger: 'change' },
                ],
                businessType: [
                    { required: true, message: '请选择所属业务', trigger: 'change' },
                ],
            },
            shopList: [], //选择的所有门店

            yongQuanTime: [],  //用券
            lingquTime: [],  //领取时间
            hyList: [],
            recipients: '00',//领取人限制
            unlimited: '00',//不限次数
            yhContent: '00',//优惠内容
            receiveLimit: '00',//优惠条件
        }
    },
    mounted() {
        this.getList()
        if (this.$route.query.couponId) {
            this.huiXianData(this.$route.query.couponId)
        }
    },
    methods: {
        getList() {
            persongrade().then(val => {
                console.log(val)
                let data = val.data.data
                this.hyList = data.records.map(item => {
                    return {
                        label: item.gradeName,
                        value: item.gradeId
                    }
                })
            })
        },
        huiXianData(couponId) {
            couponInfoDetail({ couponId: couponId }).then(val => {
                console.log(val)
                this.ruleForm = val.data.data
                // if (this.ruleForm.businessType) {
                //     if ((this.ruleForm.businessType.indexOf('02') > -1) && (this.ruleForm.businessType.indexOf('03') > -1) && (this.ruleForm.businessType.indexOf('04') > -1) && (this.ruleForm.businessType.indexOf('05') > -1)) {
                //         this.ruleForm.businessType = ['01']
                //     } else {
                //         this.ruleForm.businessType = this.ruleForm.businessType.split(',')
                //     }
                //     this.ruleForm.businessType = this.ruleForm.businessType.split(',')
                // } else {
                //     this.ruleForm.businessType = []
                // }

                this.lingquTime = [this.ruleForm.activityBegintime, this.ruleForm.activityEndtime]

                if (this.ruleForm.types == '00') {
                    this.yongQuanTime = [this.ruleForm.couponBegintime, this.ruleForm.couponEndtime]
                } else {
                    this.yongQuanTime = []
                    this.ruleForm.couponBegintime = ''
                    this.ruleForm.couponEndtime = ''
                }

                if (this.ruleForm.types == '02') {
                    this.ruleForm.validdays1 = this.ruleForm.validdays
                    this.ruleForm.validdays = ''
                }

            })
        },
        submitSave() {   //提交并保存
            console.log(this.ruleForm)
            let shop = [], goods = [], form = { ...this.ruleForm }
            this.$refs['buyingIfor'].dataList.forEach(item => {
                shop.push(item.shopId)
            })
            // this.$refs['buyingIfor1'].dataList.forEach(item => {
            //     goods.push(item.goodsId)
            // })

            // form.businessType = form.businessType.join(',')
            Object.assign(form, { //goodsId: goods.join(','),
                shopId: shop.join(',')
            })
            console.log(form)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.$refs['buyingIfor'].dataList.length > 0) {
                        // if (this.$refs['buyingIfor1'].dataList.length > 0) {
                        //     addcouponinfo(form).then(() => {
                        //         this.quxiao('ruleForm')
                        //     })
                        // } else {
                        //     this.$message.warning('请选择商品')
                        // }
                        addcouponinfo(form).then(() => {
                            this.quxiao('ruleForm')
                        })
                    } else {
                        this.$message.warning('请选择门店')
                    }
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        upData() {
            console.log(this.ruleForm)
            let shop = [], goods = [], form = { ...this.ruleForm }
            this.$refs['buyingIfor'].dataList.forEach(item => {
                shop.push(item.shopId)
            })
            // this.$refs['buyingIfor1'].dataList.forEach(item => {
            //     goods.push(item.goodsId)
            // })
            // form.businessType = form.businessType.join(',')

            if (form.types == '02') {
                form.validdays = form.validdays1
            }
            // console.log(Object.assign(form, { goodsId: goods.join(','), shopId: shop.join(',') }))
            Object.assign(form, { //goodsId: goods.join(','),
                shopId: shop.join(',')
            })
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.$refs['buyingIfor'].dataList.length > 0) {
                        // if (this.$refs['buyingIfor1'].dataList.length > 0) {
                        //     putObj(form).then(() => {
                        //         this.quxiao('ruleForm')
                        //     })
                        // } else {
                        //     this.$message.warning('请选择商品')
                        // }
                        putObj(form).then(() => {
                            this.quxiao('ruleForm')
                        })
                    } else {
                        this.$message.warning('请选择门店')
                    }
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {
                receiveLimit: '',
                receiveNum: '',
                discount: '',
                businessType: [],
                types: '00'
            }
        },
        // addStore() {
        //     if (this.ruleForm.businessType.length) {
        //         this.$refs.choiceShop.dialogVisibleTile = '选择门店'
        //         this.$refs.choiceShop.dialogVisible = true
        //         this.$refs.choiceShop.getPlatformShopInfo(this.ruleForm.businessType)
        //     } else {
        //         this.$message.warning('请选择所属业务')
        //     }
        // },
        addStore() {
            console.log(this.ruleForm.businessType)
            if (this.ruleForm.businessType) {
                this.$refs.choiceShop.dialogVisibleTile = '选择门店'
                this.$refs.choiceShop.dialogVisible = true
                this.$refs.choiceShop.getPlatformShopInfo(this.ruleForm.businessType)
            } else {
                this.$message.warning('请选择所属业务')
            }
        },
        trigger(data) {  //选择门店后
            let list = []
            data.chioseShops.forEach(i => {
                list.push(i)
            });
            this.$refs.buyingIfor.getList(list)
        },
        addGoods() {
            if (this.$refs.buyingIfor.dataList.length) {
                this.$refs.choiceGoods.dialogVisibleTile = '选择商品'
                this.$refs.choiceGoods.dialogVisible = true
                let obj = []
                this.$refs.buyingIfor.dataList.forEach(item => { //取出门店list
                    obj.push(item.shopId)
                })
                this.$refs.choiceGoods.getList(obj.join(','))
            } else {
                this.$message.warning('请至少选择一个门店')
            }
        },
        trigger1(data) {  //选择商品后
            let list = []
            data.chioseGoods.forEach(i => {
                list.push(i)
            });
            this.$refs.buyingIfor1.getList(list)
        },

        yongQuanTimeChange(e) {
            if (e) {
                this.ruleForm.couponBegintime = this.formDate(e[0])
                this.ruleForm.couponEndtime = this.formDate(e[1])
            }
            this.$forceUpdate()
        },
        lingquTimeChange(e) {
            if (e) {
                this.ruleForm.activityBegintime = this.formDate(e[0])
                this.ruleForm.activityEndtime = this.formDate(e[1])
            }
            this.$forceUpdate()
        },
        receiveNumChange(e) { //每人限领次数
            if (e == '00') {
                this.ruleForm.receiveNum = ''
            }
        },
        recipientsChange(e) { //领取人限制
            if (e == '00') {
                this.ruleForm.receiveLimit = ''
            }
        },
        receiveNumChange(e) { //优惠条件
            if (e == '01') {
                this.ruleForm.fullAmount = ''
            }
        },
        yhContentChange(e) { //优惠内容
            if (e == '00') {
                this.ruleForm.discount = ''

            } else {
                this.ruleForm.discountAmount = ''

            }
        },
        typesChange(e) {
            console.log(e)
            if (e == '02') {
                this.ruleForm.validdays = ''
            } else {
                this.ruleForm.validdays1 = ''
            }
            if (e == '01' || e == '02') {
                this.yongQuanTime = undefined
                this.ruleForm.couponBegintime = ''
                this.ruleForm.couponEndtime = ''
            }
            this.$forceUpdate()
        },
        businessTypeChange(e) {
            if (e.some((item, index) => { return item == '01' })) {
                this.ruleForm.businessType = ['01']
            }
            console.log(this.ruleForm.businessType)
        },


        delGoods(data) {  //根据门店移除对应门店的商品
            let list = []
            this.$refs.buyingIfor1.dataList.forEach((item) => {
                if (item.shopId != data.shopId) {
                    list.push(item)
                }
            })
            this.$refs.buyingIfor1.dataList = list
        }
    },

} 