import { tableOption } from './buyingIfor.config.js'
import { toDecimal2 } from '@/util/util'
import choiceGoods from './../choiceGoods.vue'
import { persongrade, addcouponinfo, couponInfoDetail } from '@/api/marketing/activeList.js'
export default {
    name: "buyingIfor",
    components: {
        choiceGoods
    },
    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            dataList: [],
            xsleList: [

                {
                    label: '商超',
                    value: '02'
                }, {
                    label: '洗衣',
                    value: '03'
                }, {
                    label: '家政',
                    value: '04'
                }, {
                    label: '咖吧',
                    value: '05'
                }
            ]
        }
    },
    filters: {
        businessType(val) {
            let obj = ''
            switch (val) {
                case '02':
                    obj = '商超'
                    break;
                case '03':
                    obj = '洗衣'
                    break;
                case '04':
                    obj = '家政'
                    break;
                case '05':
                    obj = '咖吧'
                    break;
            }
            return obj
        }

    },
    mounted() {
        if (this.$route.query.couponId) {
            this.huiXianData(this.$route.query.couponId)
        }
    },
    methods: {
        huiXianData(couponId) {
            couponInfoDetail({ couponId: couponId }).then(val => {
                console.log(val)
                if (val.data.data.couponShopList) {
                    this.dataList = val.data.data.couponShopList
                    this.dataList.forEach(item => {
                        item.businessType3 = []
                        item.businessType2 = item.businessType.split(',')
                        item.businessType2.forEach(i => {
                            this.xsleList.forEach(m => {
                                if (i == m.value) {
                                    item.businessType3.push(m.label)

                                }
                            })
                        })
                        item.businessType1 = item.businessType3.join(',')
                    })

                }

            })
        },
        getList(list) {
            list.forEach(item => {
                item.businessType3 = []
                item.businessType2 = item.businessType.split(',')
                item.businessType2.forEach(i => {
                    this.xsleList.forEach(m => {
                        if (i == m.value) {
                            item.businessType3.push(m.label)

                        }
                    })
                })
                item.businessType1 = item.businessType3.join(',')
            })
            this.dataList = list
        },
        del(index) {
            let obj = [...this.dataList]
            this.$emit('delGoods', this.dataList[index])
            obj.splice(index, 1)
            this.dataList = obj
        }
    }

} 