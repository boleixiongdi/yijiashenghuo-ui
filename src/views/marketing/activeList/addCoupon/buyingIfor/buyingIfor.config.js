export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    column: [
        {
            label: '门店ID',
            prop: 'shopId',
        },
        {
            label: '门店名称',
            prop: 'shopName',
        },
        {
            label: '所属业务',
            prop: 'businessType1',
            // dicData: [
            //     {
            //         label: "全部业务",
            //         value: "01"
            //     },
            //     {
            //         label: "商超",
            //         value: "02"
            //     },
            //     {
            //         label: "咖吧",
            //         value: "03"
            //     },
            //     {
            //         label: "洗衣",
            //         value: "04"
            //     },
            //     {
            //         label: "家政",
            //         value: "05"
            //     }
            // ]
        },
    ]
}

