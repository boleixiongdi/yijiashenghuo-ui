export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '券活动ID',
            prop: 'activityId',
            width:150,
            overHidden: true,

        },
        {
            label: '优惠券',
            prop: 'couponName',
            width:150,
            overHidden: true,
        },
        {
            label: '活动时间',
            prop: 'activityTime',
            width:200,
            overHidden: true,
        },
        {
            label: '所属业务',
            prop: 'businessType1',
            width:150,
            overHidden: true,

        },
        {
            label: "券内容",
            prop: "couponContent",
            slot: true,
            width:150,
            overHidden: true,
        },
        {
            label: '发放总数量(个)',
            prop: 'couponNum',
            width:150,
            overHidden: true,

        },
        //  {
        //     label: '券总金额(元)',
        //     prop: 'operatorId',

        // }, {
        //     label: '实付总金额(元)',
        //     prop: 'createTime'

        // },
        {
            label: '使用状态',
            prop: 'couponStatus',
            dicData: [
                {
                    label: "正常",
                    value: "00"
                },
                {
                    label: "已使用",
                    value: "01"
                },
                {
                    label: "已过期",
                    value: "02"
                }
            ]
        },
    ]
}

