import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { couponrecpage, getCountInfo } from '@/api/marketing/couponCollectionList.js'

export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            searchForm: {},
            xsleList: [
                // {
                //     label: "全部业务",
                //     value: "01",
                // },
                {
                    label: "商超",
                    value: "02",
                },
                {
                    label: "洗衣",
                    value: "03",
                },
                {
                    label: "家政",
                    value: "04",
                },
                {
                    label: "咖吧",
                    value: "05",
                },
            ],
            ruleForm:{}
        }
    },
    mounted() {
        getCountInfo().then(val => {
            console.log(val)
            let data = val.data.data
            this.ruleForm = data
        })
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            this.loading = true
            couponrecpage(Object.assign({}, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                    item.activityTime = item.activityBegintime + ' - ' + item.activityEndtime
                    item.couponEndtime = item.couponBegintime + ' - ' + item.couponEndtime
                })
                console.log(this.loadData)
                this.loading = false
            }).catch(() => {
                this.loading = false
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        // 删除
        del() {

        },
        details(item) {
            this.$router.push({ path: '/marketing/couponCollectionList/securitiesDetails', query: { couponId: item.couponId } })
        },
    }

} 