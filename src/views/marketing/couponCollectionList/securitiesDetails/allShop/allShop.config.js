export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    menu:false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '优惠券ID',
            prop: 'couponId',

        },
        {
            label: '优惠券描述',
            prop: 'couponDec'
        },
        {
            label: '领取时间',
            prop: 'receiveTime',

        },
        {
            label: '领取人',
            prop: 'couponRecId'

        },
        //  {
           
        //     label: '领取数量',
        //     prop: 'activityDec'

        // },
         {
            label: '券金额(元)',
            prop: 'couponAmd',

        }, {
            label: '关联订单号',
            prop: 'subOrderNo'

        },
        {
            label: '实付金额(元)',
            prop: 'actAmt'

        },
        {
            label: '使用状态',
            prop: 'couponStatus',
            dicData: [
                {
                    label: "正常",
                    value: "00"
                },
                {
                    label: "已使用",
                    value: "01"
                },
                {
                    label: "已过期",
                    value: "02"
                }
            ]
        },
    ]
}

