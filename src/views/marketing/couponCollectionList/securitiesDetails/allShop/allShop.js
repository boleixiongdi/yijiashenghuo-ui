import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { getCountInfo, couponrecdetail } from '@/api/marketing/couponCollectionList.js'

export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            searchForm: {},
            ruleForm: {}
        }
    },
    $route() {
        this.getList()
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            getCountInfo({ couponId: this.$route.query.couponId }).then(val => {
                console.log(val)
                let data = val.data.data
                this.ruleForm = data
            })
            
            couponrecdetail(Object.assign({ couponId: this.$route.query.couponId }, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.loadData = data.records
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                

            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        // 删除
        del() {

        },
        details(item) {

        },
    }

} 