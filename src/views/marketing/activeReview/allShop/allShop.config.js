export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '活动ID',
            prop: 'activityId',
            width:150,
            overHidden: true,

        },
        {
            label: '活动名称',
            prop: 'activityName',
            width:150,
            overHidden: true,
        },
        {
            label: '活动时间',
            prop: 'activityTime',
            width:200,
            overHidden: true,
        },
        {
            label: '所属业务',
            prop: 'businessType1',
            width:150,
            overHidden: true,

        }, {
            label: '优惠内容',
            prop: 'activityDec',
            width:150,
            overHidden: true,

        }, {
            label: '创建人',
            prop: 'operatorId',
            overHidden: true,

        }, {
            label: '创建时间',
            prop: 'createTime',
            overHidden: true,
        },
        {
            label: '审核状态',
            prop: 'auditStatus',
            dicData: [
                {
                    label: "待审核",
                    value: "00"
                },
                {
                    label: "审核通过",
                    value: "01"
                },
                {
                    label: "驳回",
                    value: "02"
                }
            ]
        },
    ]
}

