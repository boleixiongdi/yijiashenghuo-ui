import { tableOption } from './openShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { couponinfopage, doAudit } from '@/api/marketing/activeReview.js'

export default {
    name: "openShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            searchForm: {},
            ruleForm: {},
            dialogVisible: false,
            rules: {
                rebutReason: [
                    { required: true, message: '请填写驳回原因', trigger: 'blur' }
                ]
            },
            loadingSure: false,

            xsleList: [  //业态
                // {
                //     label: "全部业务",
                //     value: "01"
                // },
                {
                    label: "商超",
                    value: "02"
                },
                {
                    label: "咖吧",
                    value: "03"
                },
                {
                    label: "洗衣",
                    value: "04"
                },
                {
                    label: "家政",
                    value: "05"
                }
            ],
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            this.loading = true
            couponinfopage(Object.assign({auditStatus:'00'}, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')

                    item.activityTime = (item.activityBegintime ? item.activityBegintime + ' - ' : '') + (item.activityEndtime ? item.activityEndtime : '')

                    item.couponTime = (item.couponBegintime ? item.couponBegintime + ' - ' : '') + (item.couponEndtime ? item.couponEndtime : '')
                })
                this.$emit("trigger")
                this.loading = false
            }).catch(() => {
                this.loading = false
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        // 删除
        del() {
            console.log(123321)
        },
        details(item) {
            this.$router.push({ path: '/marketing/activeList/couponDetails', query: { couponId: item.couponId } })
        },
        peidan(row) {
            this.ruleForm.id = row.id
            this.dialogVisible = true
        },
        sure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    doAudit(this.ruleForm).then(() => {
                        this.loadingSure = false
                        this.$emit("trigger",1) //1重新加载其他tab请求
                        this.close()
                    }).catch(() => {
                        this.loadingSure = false
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        close() {
            this.dialogVisible = false
            this.ruleForm = {}
        },
    }

} 