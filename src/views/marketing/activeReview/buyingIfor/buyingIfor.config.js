export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu:false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '采购数量',
            prop: 'goodsNum',

        },
        {
            label: '采购箱',
            prop: 'conversionNum',
        },

        {
            label: '采购价/元/件',
            prop: 'costPrice',


        }, {
            label: '小计/元',
            prop: 'goodSubtotal',
        },
    ]
}

