import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import addList from "./addList.vue";
import { fetchList, addObj, editObj, statusObj } from '@/api/shop/channel/list.js'
export default {
    name: "list_table",
    mixins: [search],
    components: {
        addList
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [{
                zt: 0
            }, {
                zt: 1
            }],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增",
                    isShow: true
                }
            ],
            searchForm:{}
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        switchs(row) {         // 停用
            if (row.status == '00') {
                this.$confirm(
                    "是否停用，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        statusObj(Object.assign({ status: "01" }, { merchantId: row.merchantId })).then((val) => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "停用成功!"
                            });

                        })

                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消停用"
                        });
                    });
            } else {
                statusObj(Object.assign({ status: "00" }, { merchantId: row.merchantId })).then((val) => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "启用成功!"
                    })

                })
                    ;
            }

        },
        edit(item) {  //编辑
            this.dialogVisible = true
            this.dialogTitle = 0
            this.$nextTick(() => {
                this.$refs.addList.ruleForm = item
                if (this.$refs.addList.ruleForm.businessType) {
                    this.$refs.addList.ruleForm.businessType = this.$refs.addList.ruleForm.businessType.split(',')
                }

            })

        },
        supplierList(id) {   //供应商列表
            this.$router.push({
                path: '/supplier/goods/index',
                query: { id: id }
            })
        },
        addSupplier() {  //新增
            this.dialogVisible = true
            this.dialogTitle = 1
            
            this.$nextTick(() => {
                this.$refs.addList.ruleForm = {
                    // businessType: [], //业态
                    channelType: '01',
                    status: '00'
                }

            })
        },
        handlSure() {  //确定
            // if (this.$refs.addList.ruleForm.businessType) {
            //     this.$refs.addList.ruleForm.businessType = this.$refs.addList.ruleForm.businessType.join(',')
            // }
            this.$refs.addList.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    console.log(this.$refs.addList.ruleForm)
                    if (this.$refs.addList.ruleForm.id) {
                        editObj(this.$refs.addList.ruleForm).then(() => {
                            this.$message.success('编辑成功')
                            this.getList(this.page)
                            this.dialogVisible = false
                        })
                    } else {
                        addObj(this.$refs.addList.ruleForm).then(() => {
                            this.$message.success('添加成功')
                            this.getList(this.page)
                            this.dialogVisible = false
                        })
                    }

                } else {
                    // this.$message.error('失败')
                    return false;
                }
            });
        },
        handlClose() {
            this.dialogVisible = false
        }
    }

} 