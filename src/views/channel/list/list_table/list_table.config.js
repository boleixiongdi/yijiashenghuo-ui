const DIC = {
    channelType: [{
        label: '平台',
        value: '00'
    }, {
        label: '代理商',
        value: '02'
    }, {
        label: '分公司',
        value: '01'
    }],
    
    status: [{
        label: '启用',
        value: '00'
    }, {
        label: '停用',
        value: '01'
    }, ],

}

export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '渠道类型',
            prop: 'channelType',
            dicData: DIC.channelType,

        },
        {
            label: '公司编号',
            prop: 'merchantId',
            width:180,
            overHidden: true,
        },
        {
            label: '公司名称',
            prop: 'companyName',
            width:150,
            overHidden: true,

        },
        {
            label: '公司地址',
            prop: 'companyAdress',
            width:150,
            overHidden: true,
        },
        {
            label: '联系人',
            prop: 'contacts',
            width:150,
            overHidden: true,

        }, 
        {
            label: '联系电话',
            prop: 'contactsNum',
            width:150,
            overHidden: true,
        }, 
        {
            label: '分润规则',
            prop: 'profitRuleName',
            width:150,
            overHidden: true,
        }, 
        {
            label: '状态',
            prop: 'status',
            dicData: DIC.status,

        },
    ]
}

