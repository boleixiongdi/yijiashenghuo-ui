import { integralrulefetchList, addObj, updateById, delObj } from '@/api/shop/member/pointsRule.js'
export default {
    data() {
        const DIC = {
            isenabled: [{
                label: '未启用',
                value: '00'
            }, {
                label: '已启用',
                value: '01'
            }],
            integralChannelType: [
                {
                    label: '线上',
                    value: '00'
                }, {
                    label: '线下',
                    value: '01'
                }
            ],
            integralRuleType: [
                {
                    label: '表示消费积分',
                    value: '01'
                },
            ]
        }
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width:80
                    },
                    {
                        label: "规则名称",
                        prop: "integralRuleName",
                        overHidden: true,
                    },
                    {
                        label: "积分类型",
                        prop: "integralRuleType",
                        dicData: DIC.integralRuleType,
                        width:150,
                    },
                    {
                        label: "积分比例",
                        prop: "integralRuleProportion"
                    },
                    {
                        label: "渠道类型",
                        prop: "integralChannelType",
                        dicData: DIC.integralChannelType,
                    },
                    {
                        label: "规则有效期",
                        prop: "time",
                        overHidden: true,
                        width:260,
                        slot:true
                    },
                    {
                        label: "积分规则说明",
                        prop: "integralRuleExplain",
                        overHidden: true,
                        width:180,
                    },
                    {
                        label: "创建人",
                        prop: "operatorId"
                    },
                    {
                        label: "创建时间",
                        prop: "createTime",
                        overHidden: true,
                    },
                    {
                        label: "状态",
                        prop: "isenabled",
                        dicData: DIC.isenabled,
                    }
                ]
            },
            rules: {
                integralRuleName: [
                    { required: true, message: '请输入规则名称', trigger: 'blur' },
                ],
                integralRuleType: [
                    { required: true, message: '请选择规则类型', trigger: 'change' },
                ],
                integralRuleExplain: [
                    { required: true, message: '请输入积分规则说明', trigger: 'blur' },
                ],
                gzyxqsj: [
                    { required: true, message: '请选择起止时间', trigger: 'change' },
                ],
                isenabled: [
                    { required: true, message: '请选择状态', trigger: 'change' },
                ],
                integralChannelType: [
                    { required: true, message: '请选择渠道类型', trigger: 'change' },
                ]
            },
            ruleForm: {
                isenabled: "01",
                // integralChannelType:[]
            },
            jflxList: [  //积分类型
                {
                    label: "消费积分",
                    value: '01'
                }
            ],
            qdlxList: [  //渠道类型
                {
                    label: "全部渠道",
                    value: '00'
                },
                {
                    label: "线上",
                    value: '01'
                },
                {
                    label: "线下",
                    value: '02'
                },
            ],
            gzyxqList: [  //规则有效期
                {
                    label: "永久",
                    value: 0
                },
                {
                    label: "自定义时间",
                    value: 1
                }
            ],
            gzyxqShow: false
        };
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
           
            integralrulefetchList(Object.assign({}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    if (item.beginTime || item.endTime) {
                        item.gzyxqsj = [item.beginTime, item.endTime]
                    }
                })
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel() {
            this.dialogVisible = true
            this.dialogTitle = 1
            this.ruleForm = { isenabled: "01",  integralChannelType: '00' ,gzyxq: 0}
            if (this.ruleForm.beginTime || this.ruleForm.endTime) {  //规则有效期
                this.ruleForm.gzyxq = 1
                this.gzyxqShow = true
            } else {
                this.ruleForm.gzyxq = 0
                this.gzyxqShow = false
            }
            console.log( this.ruleForm)
        },
        edit(item) {
            this.dialogVisible = true
            this.dialogTitle = 0

            this.ruleForm = item
            if (this.ruleForm.beginTime || this.ruleForm.endTime) {  //规则有效期
                this.ruleForm.gzyxq = 1
                this.gzyxqShow = true
            } else {
                this.ruleForm.gzyxq = 0
                this.gzyxqShow = false
            }

            // if (item.integralChannelType.indexOf(',') > -1) {  //销售渠道
            //     this.ruleForm.integralChannelType = item.integralChannelType.split(',')
            // }else{
            //     this.ruleForm.integralChannelType = [item.integralChannelType]
            // }
            console.log(this.ruleForm)
        },
        switchs(row) {
            if (row.isenabled == '01') {
                this.$confirm(
                    "会员等级禁用，属于此等级的会员不再享受等级权益，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        updateById(Object.assign(row, { isenabled: '00' })).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "会员等级禁用成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                updateById(Object.assign(row, { isenabled: '01' })).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "会员等级启用成功!"
                    });
                })
            }
        },
        handlSure() {
            
            console.log(this.ruleForm)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    addObj(this.ruleForm).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '创建成功',
                            type: 'success',
                            duration: 2000
                        })
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    return false;
                }
            })
        },
        handlUpdate() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    updateById(this.ruleForm).then(val => {
                        this.getList()
                        this.handlClose()

                        this.$message({
                            title: '成功',
                            message: '修改成功',
                            type: 'success',
                            duration: 2000
                        })
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
        },
        del(row) {
            var _this = this
            this.$confirm('是否确认删除权益名称为"' + row.integralRuleName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('删除成功')
            }).catch(function () {
            })
        },
        gzyxqChange(e) {  //规则有效期
            if (e == 0) {
                this.gzyxqShow = false
                this.ruleForm.beginTime = ''
                this.ruleForm.endTime = ''
            } else {
                this.gzyxqShow = true
            }
        },
        gzyxqsjChange(e) {
            if (e) {
                this.ruleForm.beginTime = this.formDate(e[0])
                this.ruleForm.endTime = this.formDate(e[1])
            } else {
                delete this.ruleForm.gzyxqsj
            }
        }

    }
};

