export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            minWidth: 80
        },
        {
            label: '会员ID',
            prop: 'personId',
            minWidth: 120,
            overHidden: true,
        },
        {
            label: '手机号码',
            prop: 'phoneNum',
            minWidth: 120,
            overHidden: true,

        },
        {
            label: '姓名',
            prop: 'personName',
            minWidth: 120,
            overHidden: true,
        },
        {
            label: '会员等级',
            prop: 'gradeName',
            minWidth: 120,
            slot: true,
            overHidden: true,

        }, {
            label: '积分（个）',
            prop: 'accountIntegral',
            minWidth: 120,
            overHidden: true,
        }, {
            label: '储值余额（元）',
            prop: 'accountBalance',
            minWidth: 120,
            overHidden: true,
        }, {
            label: '消费次数',
            prop: 'orderNum',
            minWidth: 120,
            overHidden: true,
        }, {
            label: '累计消费金额（元）',
            prop: 'orderAmt',
            minWidth: 150,
            overHidden: true,
        }, {
            label: '注册渠道',
            prop: 'registerChannel',
            minWidth: 120,
            overHidden: true,
            dicData: [
                {
                    label: "线下",
                    value: "00"
                },
                {
                    label: "线上",
                    value: "01"
                }
            ]

        }, {
            label: '注册门店',
            prop: 'shopName',
            minWidth: 120,
            overHidden: true,
        }, {
            label: '注册时间',
            prop: 'registerTime',
            minWidth: 120,
            overHidden: true,
        }, {
            label: '最近登录时间',
            prop: 'lastLoginTime',
            minWidth: 120,
            overHidden: true,
        },
    ]
}

