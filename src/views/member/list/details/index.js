import baseInfor from './baseInfor.vue'
import pointsDetails from './pointsDetails.vue'
import storedDetails from './storedDetails.vue'
import exchangeOrder from './exchangeOrder.vue'
import coupon from './coupon.vue'
export default {
    components: {
        baseInfor,
        pointsDetails,
        storedDetails,
        exchangeOrder,
        coupon
    },
    data() {
        return {
            keyName: "1",
            tab: {
                baseInfor: "基本信息",
                pointsDetails: "积分明细",
                storedDetails: "储值明细",
                exchangeOrder: "兑换订单",
                coupon: "优惠券",
            },
        }
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.baseInfor.getList() //基本信息
                    break;
                case '2':
                    this.$refs.pointsDetails.getList() //积分明细
                    break;
                case '3':
                    this.$refs.storedDetails.getList() //储值明细
                    break;
                case '4':
                    this.$refs.exchangeOrder.getList() //兑换订单
                    break;
                case '5':
                    this.$refs.coupon.getList() //优惠券
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.baseInfor.getList()       //基本信息
            this.$refs.pointsDetails.getList()  //积分明细
            this.$refs.storedDetails.getList() //储值明细
            this.$refs.exchangeOrder.getList() //兑换订单
            this.$refs.coupon.getList() //优惠券
        },
    },

} 