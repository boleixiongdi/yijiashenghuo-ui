import formNode from '@/views/com/formNode.vue'
import { equityrulefetchList, addObj, updateById, delObj } from '@/api/shop/member/rights.js'
import { integralrulefetchList } from '@/api/shop/member/pointsRule.js'
import { couponinfopage } from '@/api/marketing/activeList.js'

export default {
    components: {
        formNode,
    },
    data() {
        const DIC = {
            isenabled: [{
                label: '未启用',
                value: '00'
            }, {
                label: '已启用',
                value: '01'
            }],
            equityRuleType: [
                {
                    label: '积分权益',
                    value: '00'
                }, {
                    label: '储值权益',
                    value: '01'
                }, {
                    label: '会员等级权益',
                    value: '02'
                }
            ]
        }
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width:80
                    },
                    {
                        label: "权益名称",
                        prop: "equityRuleName"
                    },
                    {
                        label: "权益类型",
                        prop: "equityRuleType",
                        dicData: DIC.equityRuleType,

                    },
                    {
                        label: "权益说明",
                        prop: "equityRuleExplain"
                    },
                    {
                        label: "创建人",
                        prop: "operatorId"
                    },
                    {
                        label: "创建时间",
                        prop: "createTime"
                    },
                    {
                        label: "状态",
                        prop: "isenabled",
                        dicData: DIC.isenabled,
                    }
                ]
            },
            // formNodeList: [
            //     {
            //         type: 'input',
            //         label: "权益名称",
            //         prop: "equityRuleName",
            //         placeholder: "请输入权益名称，不超过30个字",
            //         change: (e) => {
            //             console.log(e)
            //         },
            //         blur: (e, val) => {
            //             console.log(val)
            //         },
            //     },
            //     {
            //         type: 'select',
            //         label: "权益类型",
            //         prop: "equityRuleType",
            //         style: {
            //             width: '100%'
            //         },
            //         option: [
            //             {
            //                 label: "积分权益",
            //                 value: '00'
            //             },
            //             {
            //                 label: "储值权益",
            //                 value: '01'
            //             },
            //             {
            //                 label: "等级权益",
            //                 value: '02'
            //             }
            //         ],
            //         change: (e) => {
            //             console.log(e)
            //         }
            //     },
            //     {
            //         type: 'input',
            //         label: "权益说明",
            //         prop: "equityRuleExplain",
            //         inputType: "textarea",
            //         autosize: false,
            //         placeholder: "请输入权益说明，不超过300个字",
            //         change: (e) => {
            //             console.log(e)
            //         },
            //         blur: (e, val) => {
            //             console.log(val)
            //         },
            //     },
            //     {
            //         type: 'radio',
            //         label: "状态",
            //         prop: "isenabled",
            //         radioValue: [
            //             {
            //                 label: "启用",
            //                 value: '01'
            //             },
            //             {
            //                 label: "停用",
            //                 value: '00'
            //             }
            //         ],
            //         change: (e, val) => {
            //             console.log(val)
            //         }
            //     },
            // ],
            rules: {
                equityRuleName: [
                    { required: true, message: '请输入权益名称', trigger: 'blur' },
                ],
                equityRuleType: [
                    { required: true, message: '请选择权益类型', trigger: 'change' },
                ],
                equityRuleExplain: [
                    { required: true, message: '请输入权益说明', trigger: 'blur' },
                ],
                isenabled: [
                    { required: true, message: '请选择状态', trigger: 'change' },
                ],
                integralRuleId: [
                    { required: true, message: '请选择积分规则', trigger: 'change' },
                ],
                gradeId: [
                    { required: true, message: '请选择会员等级', trigger: 'change' },
                ],
                equityRuleContent: [
                    { required: true, message: '请选择权益内容', trigger: 'change' },
                ],
                giveIntegral: [
                    { required: true, message: '请输入赠送积分数量', trigger: 'blur' },
                ],
                couponId: [
                    { required: true, message: '请选择优惠券', trigger: 'change' },
                ]
            },
            ruleForm: {

            },
            equityRuleTypeList: [
                {
                    label: "积分权益",
                    value: '00'
                },
                {
                    label: "储值权益",
                    value: '01'
                },
                {
                    label: "等级权益",
                    value: '02'
                }
            ],
            isenabledList: [
                {
                    label: "启用",
                    value: '01'
                },
                {
                    label: "停用",
                    value: '00'
                }
            ],
            integralRuleTypeList: [], //积分规则

            hydjList: [ //会员等级
                {
                    value: 'LV1',
                    label: 'LV1'
                },
                {
                    value: 'LV2',
                    label: 'LV2'
                },
                {
                    value: 'LV3',
                    label: 'LV3'
                },
                {
                    value: 'LV4',
                    label: 'LV4'
                },
                {
                    value: 'LV5',
                    label: 'LV5'
                },
                {
                    value: 'LV6',
                    label: 'LV6'
                },
                {
                    value: 'LV7',
                    label: 'LV7'
                },
                {
                    value: 'LV8',
                    label: 'LV8'
                },
                {
                    label: 'LV9',
                    value: 'LV9'
                },
                {
                    label: 'LV10',
                    value: 'LV10'
                }
            ],
            youHuiList: []
        };
    },
    mounted() {
        this.getList()
        integralrulefetchList().then(val => { //积分规则
            let data = val.data.data.records
            this.integralRuleTypeList = data.map(item => {
                return {
                    label: item.integralRuleName,
                    value: item.integralRuleId
                }
            })
        })
        couponinfopage().then(val => {  //优惠券
            let data = val.data.data.records
            this.youHuiList = data.map(item => {
                return {
                    label: item.couponName,
                    value: item.couponId
                }
            })
        })
    },
    methods: {
        getList() {
            equityrulefetchList(Object.assign(this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel() {
            this.dialogVisible = true
            this.dialogTitle = 1
            this.ruleForm = { isenabled: "01", equityRuleContent: [] }
        },
        edit(item) {
            this.dialogVisible = true
            this.dialogTitle = 0
            let obj = { ...item }
            console.log(obj)
            // return
            if (obj.equityRuleType == '02') {
                if (obj.equityRuleContent) {
                    if (obj.equityRuleContent.indexOf(',') > -1) {
                        obj.equityRuleContent = obj.equityRuleContent.split(',')
                    } else {
                        if (obj.equityRuleContent) {
                            obj.equityRuleContent = [obj.equityRuleContent]
                        } else {
                            obj.equityRuleContent = []
                        }
                    }
                } else {
                    obj.equityRuleContent = []
                }

            }
            this.ruleForm = obj
            console.log(this.ruleForm)
        },
        switchs(row) {
            if (row.isenabled == '01') {
                this.$confirm(
                    "会员等级禁用，属于此等级的会员不再享受权益，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        updateById(Object.assign(row, { isenabled: '00' })).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "会员等级禁用成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消会员等级禁用"
                        });
                    });
            } else {
                updateById(Object.assign(row, { isenabled: '01' })).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "会员等级启用成功!"
                    });
                })
            }
        },
        handlSure() {
            let obj = { ...this.ruleForm }
            if (this.ruleForm.equityRuleType == '02') {
                obj.equityRuleContent = obj.equityRuleContent.join(',')
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    addObj(obj).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '创建成功',
                            type: 'success',
                            duration: 2000
                        })
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            })
        },
        handlUpdate() {
            let obj = { ...this.ruleForm }
            if (this.ruleForm.equityRuleType == '02') {
                obj.equityRuleContent = obj.equityRuleContent.join(',')
            }

            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    updateById(obj).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '修改成功',
                            type: 'success',
                            duration: 2000
                        })
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
        },
        del(row) {
            var _this = this
            this.$confirm('是否确认删除权益名称为"' + row.equityRuleName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('删除成功')
            }).catch(function () {
            })
        },
        equityRuleContentChange(e) {
            e.forEach(item => {
                if (item == '00') {
                    this.ruleForm.couponId = undefined
                } else {
                    this.ruleForm.giveIntegral = ''
                }
            });
            //  if (item == '00') {
            //         this.ruleForm.couponId = undefined
            //     } else {
            //         this.ruleForm.giveIntegral = ''
            //     }
        },
        equityRuleTypeChange(e) {
            let form = { ...this.ruleForm }
            if (e == '00') {
                form = {}
                form.equityRuleName = this.ruleForm.equityRuleName ? this.ruleForm.equityRuleName : ''
                form.equityRuleType = this.ruleForm.equityRuleType
                form.isenabled = this.ruleForm.isenabled
                form.equityRuleExplain = this.ruleForm.equityRuleExplain ? this.ruleForm.equityRuleExplain : ''
            } else if (e == '01') {
                form = {}
                form.equityRuleName = this.ruleForm.equityRuleName ? this.ruleForm.equityRuleName : ''
                form.equityRuleType = this.ruleForm.equityRuleType
                form.isenabled = this.ruleForm.isenabled
                form.equityRuleExplain = this.ruleForm.equityRuleExplain ? this.ruleForm.equityRuleExplain : ''
            } else if (e == '02') {
                form = {}
                form.equityRuleName = this.ruleForm.equityRuleName ? this.ruleForm.equityRuleName : ''
                form.equityRuleType = this.ruleForm.equityRuleType
                form.isenabled = this.ruleForm.isenabled
                form.equityRuleExplain = this.ruleForm.equityRuleExplain ? this.ruleForm.equityRuleExplain : ''
                form.equityRuleContent = []
                form.couponId = undefined
            }
            this.ruleForm = form
            console.log(this.ruleForm)
        }
    }
};

