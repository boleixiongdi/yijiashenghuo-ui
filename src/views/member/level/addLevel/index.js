import {
    persongradefetchList,
    getEquityRuleList,
    addObj,
    putObj,
    delObj,
    getObj
} from "@/api/shop/member/level.js";

import { equityrulefetchList, } from "@/api/shop/member/rights.js";
export default {
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                status: '01',
                integralRightsIds: [],
                storedRightsIds: [],
                gradeRightsIds: [],
            },
            rules: {
                gradeName: [{ required: true, message: '请输入等级名称', trigger: 'blur' }],
                gradeId: [{ required: true, message: '请选择会员等级', trigger: 'change' }],
                validityday: [{ required: true, message: '请选择会员有效期', trigger: 'change' }],

                integralRightsIds: [{ required: true, message: '请选择积分权益', trigger: 'change' }],
                storedRightsIds: [{ required: true, message: '请选择储值权益', trigger: 'change' }],
                gradeRightsIds: [{ required: true, message: '请选择会员等级权益', trigger: 'change' }],

                demotiondayAfter: [{ required: true, message: '请选择降级规则', trigger: 'change' }],
                demotiondayNear: [{ required: true, message: '请选择降级规则', trigger: 'change' }],
            },
            hydjList: [ //会员等级
                {
                    value: '1',
                    label: 'LV1'
                },
                {
                    value: '2',
                    label: 'LV2'
                },
                {
                    value: '3',
                    label: 'LV3'
                },
                {
                    value: '4',
                    label: 'LV4'
                },
                {
                    value: '5',
                    label: 'LV5'
                },
                {
                    value: '6',
                    label: 'LV6'
                },
                {
                    value: '7',
                    label: 'LV7'
                },
                {
                    value: '8',
                    label: 'LV8'
                },
                {
                    value: '9',
                    label: 'LV9'
                },
                {
                    value: '10',
                    label: 'LV10'
                },
            ],
            quanyiList: [  //权益
                {
                    label: "积分权益",
                    value: "00",
                    data: [

                    ],
                },
                {
                    label: "储值权益",
                    value: "01",
                    data: [

                    ],
                },
                {
                    label: "会员等级权益",
                    value: "03",
                    data: [

                    ]
                }
            ],
            jiangjiList: [
                {
                    label: 1,
                    value: 1
                },
                {
                    label: 2,
                    value: 2
                },
                {
                    label: 3,
                    value: 3
                },
                {
                    label: 4,
                    value: 4
                },
                {
                    label: 5,
                    value: 5
                },
                {
                    label: 6,
                    value: 6
                },
                {
                    label: 7,
                    value: 7
                },
                {
                    label: 8,
                    value: 8
                },
                {
                    label: 9,
                    value: 9
                },
                {
                    label: 10,
                    value: 10
                },
                {
                    label: 11,
                    value: 11
                },
                {
                    label: 12,
                    value: 12
                }
            ],
            quanYi: [],

            storeintegralRightsIdList: [],
            storestoredRightsIdList: [],
            storegradeRightsIdList: [],

            integralRightsIdList: [], //积分权益
            storedRightsIdList: [], //储值权益
            gradeRightsIdList: [], //会员等级权益



        }
    },
    watch: {
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            getEquityRuleList({ equityRuleType: '00' }).then(val => { //积分权益
                // console.log('权益', val)
                let data = val.data.data
                this.storeintegralRightsIdList = data
                this.integralRightsIdList = data.map(item => {
                    return {
                        label: item.equityRuleName,
                        value: item.equityRuleId
                    }
                })
            })
            getEquityRuleList({ equityRuleType: '01' }).then(val => {//储值权益
                // console.log('权益', val)
                let data = val.data.data
                this.storestoredRightsIdList = data
                this.storedRightsIdList = data.map(item => {
                    return {
                        label: item.equityRuleName,
                        value: item.equityRuleId
                    }
                })
            })
            getEquityRuleList({ equityRuleType: '02' }).then(val => {//会员等级权益
                // console.log('权益', val)
                let data = val.data.data
                this.storegradeRightsIdList = data
                this.gradeRightsIdList = data.map(item => {
                    return {
                        label: item.equityRuleName,
                        value: item.equityRuleId
                    }
                })
            })
            if (this.$route.query.id) {
                getObj(this.$route.query.id).then(val => {
                    let data = val.data.data
                    console.log('id', data)
                    this.ruleForm = data
                    this.ruleForm.integralRightsIds = this.ruleForm.integralRightsIdList.map(item => {
                        return item.equityRuleId
                    })
                    this.ruleForm.storedRightsIds = this.ruleForm.storedRightsIdList.map(item => {
                        return item.equityRuleId
                    })
                    this.ruleForm.gradeRightsIds = this.ruleForm.gradeRightsIdList.map(item => {
                        return item.equityRuleId
                    })

                    console.log('id', this.ruleForm)
                })
            }
        },
        sure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.ruleForm.integralRightsId = []
                    this.ruleForm.storedRightsId = []
                    this.ruleForm.gradeRightsId = []

                    this.ruleForm.integralRightsIds.forEach(item => {
                        this.storeintegralRightsIdList.forEach(item1 => {
                            if (item == item1.equityRuleId) {
                                this.ruleForm.integralRightsId.push(item1)
                            }
                        })

                    });

                    this.ruleForm.storedRightsIds.forEach(item => {
                        this.storestoredRightsIdList.forEach(item1 => {
                            if (item == item1.equityRuleId) {
                                this.ruleForm.storedRightsId.push(item1)
                            }
                        })

                    });
                    this.ruleForm.gradeRightsIds.forEach(item => {
                        this.storegradeRightsIdList.forEach(item1 => {
                            if (item == item1.equityRuleId) {
                                this.ruleForm.gradeRightsId.push(item1)
                            }
                        })

                    });
                    console.log(this.ruleForm)
                    if (this.ruleForm.id) {
                        putObj(this.ruleForm).then(() => {
                            this.close()
                            this.ruleForm = {
                                status: '01',
                                integralRightsIds: [],
                                storedRightsIds: [],
                                gradeRightsIds: []
                            }
                        })
                    } else {
                        addObj(this.ruleForm).then(() => {
                            this.close()
                            this.ruleForm = {
                                status: '01',
                                integralRightsIds: [],
                                storedRightsIds: [],
                                gradeRightsIds: []
                            }
                        })
                    }
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        close() {
            this.$router.go(-1)
        },
    },

} 