import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { deliveryinfofetchList, addObj, delObj, putObj } from '@/api/shop/distributionSheet.js'

export default {
    name: "list_table",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增",
                    isShow: true
                }
            ],
            searchForm: {},
            yetaiList: [
                //商品业态
                {
                    label: "商超",
                    value: "02",
                },
                {
                    label: "洗衣",
                    value: "03",
                },
                {
                    label: "家政",
                    value: "04",
                },
                {
                    label: "咖吧",
                    value: "05",
                },
            ],
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            deliveryinfofetchList(Object.assign({}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    current: data.current,
                    total: data.total,
                    size: data.size,
                }
                this.loadData = data.records
                console.log(this.loadData)
                this.loadData.forEach(item => {
                    item.disabled = false
                })
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.add()
            }
        },
        add() {
            this.loadData.unshift({ disabled: true })
        },
        edit(item) {  //编辑
            const newData = [...this.loadData];
            this.$nextTick(() => {
                newData.forEach(i => {
                    if (i.id == item.id) {
                        i.disabled = true
                    } else {
                        i.disabled = false
                    }
                    i.disabledEdit = true  //修改不允许编辑
                });
            })
            this.loadData = newData
            console.log(this.loadData)
        },
        save(item) {
            console.log(item)
            item.disabled = false
            if (item.businessType && item.deliveryFee) {
                if (item.id) {
                    putObj(item).then(val => {
                        const newData = [...this.loadData];
                        this.$nextTick(() => {
                            newData.forEach(i => {
                                if (i.id == item.id) {
                                    i.disabled = false
                                }
                            });
                        })
                        this.loadData = newData
                        this.$message.success('修改成功');
                    })
                } else {
                    addObj(item).then(val => {
                        const newData = [...this.loadData];
                        this.$nextTick(() => {
                            newData.forEach(i => {
                                if (i.id == item.id) {
                                    i.disabled = false
                                }
                            });
                        })
                        this.loadData = newData
                        this.$message.success('保存成功');
                    })
                }
            } else {
                this.$message.warning('请填写所属业态或配送费');
            }


        },
        del(item) {
            if (item.id) {
                this.$confirm('确定删除业态名称为' + this.businessTypeName(item.businessType) + '的数据, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    delObj(item.id).then(() => {
                        this.$message({
                            type: 'success',
                            message: '删除成功!'
                        });
                        this.getList()
                    })

                }).catch(() => {

                });
            } else {
                console.log(item)
                this.loadData.splice(item.$index, 1)
            }

        },
        businessTypeName(name) {
            if (name == '02') {
                return '商超'
            } else if (name == '03') {
                return '洗衣'
            } else if (name == '04') {
                return '家政'
            } else if (name == '05') {
                return '咖吧'
            }
        }
    }

} 