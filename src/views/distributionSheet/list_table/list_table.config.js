export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '所属业态',
            prop: 'businessType',
            slot: true
        },
        {
            label: '配送费（元）',
            prop: 'deliveryFee',

            slot: true
        },
    ]
}

