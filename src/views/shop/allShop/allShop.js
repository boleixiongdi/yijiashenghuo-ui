import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { fetchList, putObj, changeOnlineStatus } from '@/api/shop/shop/list.js'
export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {

        return {
            tableOption: tableOption,
            loading: false,
            loadData: [/* {
                id: 1,
                fs: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1592308474873&di=2992cc1a08e1bc47bb605018ba6211da&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg'
            } */],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({}, searchForm ? searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                console.log(888, data)
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
                console.log(this.loadData)
                this.loadData.forEach(item => {
                    if (item.businessType) {
                        item.businessType3 = []
                        item.businessType2 = item.businessType.split(',')
                        item.businessType2.forEach(i => {
                            this.$store.getters.businessTypeList.forEach(m => {
                                if (i == m.value) {
                                    item.businessType3.push(m.label)

                                }
                            })
                        })
                        item.businessType1 = item.businessType3.join(',')
                    }
                })
            })
            /* setTimeout(() => {
                this.page.total = 200
               
            }, 500) */
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        // 删除
        delete() {
            console.log(123321)
        },
        edit(item) {
            let obj = {}
            Object.assign(obj, item, { isEdit: false })
            this.$router.push({ path: '/shop/addShop', query: obj })
        },
        view(item) {
            let obj = {}
            Object.assign(obj, item, { isEdit: true })
            this.$router.push({ path: '/shop/addShop', query: obj })
        },
        upDown(row) {
            if (row.status == '01') {
                this.$confirm(
                    "确定下线该门店，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        changeOnlineStatus(Object.assign({ status: '02' }, { id: row.id })).then(val => {
                            // this.getList()
                            this.$emit("trigger", 1) //1重新加载其他tab请求
                            this.$message({
                                type: "success",
                                message: "下线成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                changeOnlineStatus(Object.assign({ status: '01' }, { id: row.id })).then(val => {
                    this.$emit("trigger", 1) //1重新加载其他tab请求
                    this.$message({
                        type: "success",
                        message: "上线成功!"
                    });
                })
            }
        },
        shopGoodsList(item) {
            let obj = {
                shopId: item.shopId
            }
            this.$router.push({ path: '/shop/shopGoodsList', query: obj })
        }
    }

} 