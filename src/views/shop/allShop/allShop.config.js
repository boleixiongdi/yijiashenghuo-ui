const DIC = {
    status: [{
        label: '营业中',
        value: '01'
    }, {
        label: '停业',
        value: '02'
    }],
    businessType: [
       
     {
        label: '商超',
        value: '02'
    }, {
        label: '洗衣',
        value: '03'
    }, {
        label: '家政',
        value: '04'
    }, {
        label: '咖吧',
        value: '05'
    }],

}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 400,
    
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '门店编号',
            prop: 'shopId',
            overHidden:true,
        },
        {
            label: '门店名称',
            prop: 'shopName',
            overHidden:true,

        },
        // {
        //     label: '供应商名称',
        //     prop: 'merchantId',
        //     overHidden:true,
        // },
        {
            label: '所在城市',
            prop: 'cityCode',
            overHidden:true,
        }, {
            label: '所属业务',
            prop: 'businessType1',
            overHidden:true,
        }, {
            label: '门店状态',
            prop: 'status',
            dicData: DIC.status,

        }, {
            label: '员工数量',
            prop: 'workerNum',

        }, {
            label: '店主',
            prop: 'shopDirector',

        },
    ]
}

