import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import openShop from "./openShop/openShop.vue";
import stopShop from "./stopShop/stopShop.vue";
import { mapGetters } from 'vuex'
const tabName = [
    "全部",
    "营业中",
    "下线中",
]
export default {
    mixins: [search],
    components: {
        allShop,
        openShop,
        stopShop
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "全部",
                openShop: "营业中",
                stopShop: "下线中",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allShop.getList(this.searchForm, 1) //全部列表
                    break;
                case '2':
                    this.$refs.openShop.getList(this.searchForm, 1) //营业中列表
                    break;
                case '3':
                    this.$refs.stopShop.getList(this.searchForm, 1) //下线中列表
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allShop.getList()       //全部列表
            this.$refs.openShop.getList()  //营业中列表
            this.$refs.stopShop.getList() //下线中列表
        },
        trigger(st) {
            if (st) {
                this.getList()
            } else {
                this.tab.allShop = `${tabName[0]}(${this.$refs.allShop.page.total})`
                this.tab.openShop = `${tabName[1]}(${this.$refs.openShop.page.total})`
                this.tab.stopShop = `${tabName[2]}(${this.$refs.stopShop.page.total})`
            }
            this.$forceUpdate()
        },
        addMendian() {
            if (this.keyName == "1") {
                this.$refs.allShop.getList()       //全部列表
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //营业中列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //下线中列表
            }
            this.$router.push({
                path: "/shop/addShop"
            })
        },
        drShangpin() {
            if (this.keyName == "1") {
                this.$refs.allShop.getList()       //全部列表
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //营业中列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //下线中列表
            }
        }
    }
}