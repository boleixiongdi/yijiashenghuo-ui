export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            overHidden:true,
        },
        {
            label: '业态',
            prop: 'businessType1',
        },
        {
            label: '商品条码',
            prop: 'barCode',
            overHidden:true,
        }, {
            label: '商品图片',
            prop: 'goodsPic',
            slot: true,
        }, {
            label: '商品类目',
            prop: 'categoryName',

        }, {
            label: '进价（元）',
            prop: 'costPrice',

        },
        {
            label: '售价（元）',
            prop: 'sellingPrice',

        },
        {
            label: '库存单位',
            prop: 'stockUnit',

        },
    ]
}