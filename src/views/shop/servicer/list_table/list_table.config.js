const DIC = {
    status: [{
        label: '停止服务',
        value: '00'
    }, {
        label: '提供服务',
        value: '01'
    },],
    sex: [
        {
            label: '男',
            value: '00'
        }, {
            label: '女',
            value: '01'
        },
    ],
    role:[
        {
            label: '普通店员',
            value: '00'
        }, {
            label: '店长',
            value: '01'
        },{
            label: '服务人员',
            value: '02'
        },
    ]

}

export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: '服务人员姓名',
            prop: 'personName',
            width:150,
            overHidden: true,
        },
        {
            label: '性别',
            prop: 'sex',
            dicData: DIC.sex,
        },{
            label: '角色',
            prop: 'isShopowner',
            dicData: DIC.role,
        },
        {
            label: '联系电话',
            prop: 'phoneNum',
            width:150,
            overHidden: true,
        },
        {
            label: '涉及业态',
            prop: 'industryId1',
            width:150,
            overHidden: true,
        },
        {
            label: '绑定门店',
            prop: 'shopName',
            width:150,
            overHidden: true,
        },
        {
            label: '佣金规则',
            prop: 'profitRule',
            width:150,
            overHidden: true,
            slot:true
        },
        {
            label: '状态',
            prop: 'state',
            dicData: DIC.status,
            width:150,
            overHidden: true,
        },
    ]
}

