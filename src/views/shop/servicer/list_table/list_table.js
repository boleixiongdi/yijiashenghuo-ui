import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import addList from "./addList.vue";
import { shoppersonfetchList, addObj, editObj, statusObj } from '@/api/shop/servicer/list.js'
export default {
    name: "list_table",
    mixins: [search],
    components: {
        addList
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增",
                    isShow: true
                }
            ],
            xsleList: [
                // {
                //     label: '全部业务',
                //     value: '01'
                // },
                {
                    label: '商超',
                    value: '02'
                }, {
                    label: '洗衣',
                    value: '03'
                }, {
                    label: '家政',
                    value: '04'
                }, {
                    label: '咖吧',
                    value: '05'
                }
            ],
            searchForm: {}

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            shoppersonfetchList(Object.assign({}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {  //处理业态
                    if (item.industryId) {
                        item.industryId3 = []
                        item.industryId2 = item.industryId.split(',')
                        item.industryId2.forEach(i => {
                            this.xsleList.forEach(m => {
                                if (i == m.value) {
                                    item.industryId3.push(m.label)

                                }
                            })
                        })
                        item.industryId1 = item.industryId3.join(',')
                    }

                })
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        switchs(row) {         // 停用
            if (row.state == '01') {
                this.$confirm(
                    "服务人员停用，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        statusObj(Object.assign({ state: "00" }, { personId: row.personId })).then((val) => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "服务人员停用成功!"
                            });

                        })

                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消服务人员停用"
                        });
                    });
            } else {
                statusObj(Object.assign({ state: "01" }, { personId: row.personId })).then((val) => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "服务人员启用成功!"
                    })

                })
                    ;
            }

        },
        edit(item) {  //编辑
            this.dialogVisible = true
            this.dialogTitle = 0
            this.$nextTick(() => {
                let obj = { ...item }
                this.$refs.addList.ruleForm = obj
                if (this.$refs.addList.ruleForm.industryId) {
                    if (this.$refs.addList.ruleForm.industryId.indexOf(',') > -1) {
                        this.$refs.addList.ruleForm.industryId = this.$refs.addList.ruleForm.industryId.split(',')
                    } else {
                        let list = []
                        list.push(this.$refs.addList.ruleForm.industryId)
                        this.$refs.addList.ruleForm.industryId = list
                    }
                } else {
                    this.$refs.addList.ruleForm.industryId = []
                }

                if (this.$refs.addList.ruleForm.householdType) {
                    if (this.$refs.addList.ruleForm.householdType.indexOf(',') > -1) {
                        this.$refs.addList.ruleForm.householdType1 = this.$refs.addList.ruleForm.householdType.split(',')
                    } else {
                        let list = []
                        list.push(this.$refs.addList.ruleForm.householdType)
                        this.$refs.addList.ruleForm.householdType1 = list
                    }
                }
                this.$refs.addList.industryIdChange(this.$refs.addList.ruleForm.industryId)
                // this.$refs.addList.getFenrun()
            })

        },
        supplierList(id) {   //供应商列表
            this.$router.push({
                path: '/supplier/goods/index',
                query: { id: id }
            })
        },
        addSupplier() {  //新增
            this.dialogVisible = true
            this.dialogTitle = 1
            this.$nextTick(() => {
                this.$refs.addList.ruleForm = {
                    sex: '00',
                    industryId: [], //业态
                    state: '01',
                    isShopowner: '00',
                    idCardFront: "",
                    idCardBck: "",
                }
            })
        },
        handlSure() {  //确定
            let obj = { ...this.$refs.addList.ruleForm }
            if (obj.industryId) {
                if (!(obj.industryId.some(item => { return item == '04' }))) {
                    obj.householdType = ''
                }

                obj.industryId = obj.industryId.join(',')
            }

            obj.householdType = obj.householdType1.join(',')  //家政多选

            console.log(obj)
            this.$refs.addList.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (obj.isShopowner == '01') {
                        if (obj.shopownerId) {
                            if (obj.shopownerId == obj.personId) {
                                if (obj.id) {
                                    editObj(obj).then(() => {
                                        this.$message.success('编辑成功')
                                        this.getList(this.page)
                                        this.dialogVisible = false
                                    })
                                } else {
                                    addObj(obj).then(() => {
                                        this.$message.success('添加成功')
                                        this.getList(this.page)
                                        this.dialogVisible = false
                                    })
                                }
                                console.log('自己就是店长')
                                return
                            }
                            this.$confirm('此操作将替换原来的店长, 是否继续?', '提示', {
                                confirmButtonText: '确定',
                                cancelButtonText: '取消',
                                type: 'warning'
                            }).then(() => {
                                console.log('将替换原来的店长')
                                if (obj.id) {
                                    editObj(obj).then(() => {
                                        this.$message.success('编辑成功')
                                        this.getList(this.page)
                                        this.dialogVisible = false
                                    })
                                } else {
                                    addObj(obj).then(() => {
                                        this.$message.success('添加成功')
                                        this.getList(this.page)
                                        this.dialogVisible = false
                                    })
                                }
                            }).catch(() => {
                                return false
                            });
                        } else {
                            if (obj.id) {
                                editObj(obj).then(() => {
                                    this.$message.success('编辑成功')
                                    this.getList(this.page)
                                    this.dialogVisible = false
                                })
                            } else {
                                addObj(obj).then(() => {
                                    this.$message.success('添加成功')
                                    this.getList(this.page)
                                    this.dialogVisible = false
                                })
                            }
                            console.log('门店还没店长')
                        }

                    } else {
                        if (obj.id) {
                            editObj(obj).then(() => {
                                this.$message.success('编辑成功')
                                this.getList(this.page)
                                this.dialogVisible = false
                            })
                        } else {
                            addObj(obj).then(() => {
                                this.$message.success('添加成功')
                                this.getList(this.page)
                                this.dialogVisible = false
                            })
                        }
                        console.log('我是普通店员')
                    }
                } else {
                    return false;
                }
            });
        },
        handlClose() {
            this.dialogVisible = false
        }
    }

} 