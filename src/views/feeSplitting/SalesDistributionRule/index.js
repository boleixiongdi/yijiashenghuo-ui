import search from "./search/search.js";
import SalesDistributionRule from "./SalesDistributionRule_list/SalesDistributionRule.vue";

export default {
    mixins: [search],
    components: {
        SalesDistributionRule,
    },
    data() {
        return {
            
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.SalesDistributionRule.getList()      
        }
    }
}