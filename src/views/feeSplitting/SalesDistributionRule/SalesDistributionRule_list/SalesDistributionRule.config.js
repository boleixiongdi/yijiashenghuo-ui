const DIC = {
    statuslistt: [
        {
            label: "分公司",
            value: "01"
        },
        {
            label: "代理商",
            value: "02"
        }
    ], //渠道类型
    xsleList: [  //业态
        {
            label: "商超",
            value: "02"
        },
        {
            label: "洗衣",
            value: "03"
        },
        {
            label: "家政",
            value: "04"
        },
        {
            label: "咖吧",
            value: "05"
        }
    ],
    ztaiList: [ //状态
        {
            label: "停用",
            value: "00"
        },
        {
            label: "启用",
            value: "01"
        },
    ],
}
export const tableOption = {
    selection: false,
    reserveSelection: false,
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '分润规则名称',
            prop: 'profitRuleName',

        },
        {
            label: '适用渠道类型',
            prop: 'channelType',
            dicData: DIC.statuslistt,

        },
        {
            label: '涉及业态',
            prop: 'businessType1',

        },
        {
            label: '状态',
            prop: 'status',
            dicData: DIC.ztaiList,
        },
    ]
}

