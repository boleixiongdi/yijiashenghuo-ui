import { tableOption } from './ServiceCommissionsRule.config.js'
import { fetchList, addObj, putObj, getObj, editObj } from '@/api/feeSplitting/ServiceCommissionsRule.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
export default {
    name: "ServiceCommissionsRule",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增",
                    isShow: true,
                    type: "primary",
                    icon: "el-icon-plus"
                },
            ],
            title: 1,
            dialogFormVisible: false,
            loading: false,
            ruleForm: {
                businessType: [],
                ruleTypeList: []
            },
            rules: {
                profitRuleName: [
                    { required: true, message: '请输入服务佣金名称', trigger: 'blur' },
                ],
                businessType: [
                    { type: 'array', required: true, message: '请至少选择一个涉及业态', trigger: 'change' }
                ],
                status: [
                    { required: true, message: '请选择状态', trigger: 'change' }
                ],
            },
            xsleList: [{
                label: "商超",
                value: "02",
                checked: false
            },
            {
                label: "洗衣",
                value: "03",
                checked: false
            },
            {
                label: "家政",
                value: "04",
                checked: false
            },
            {
                label: "咖吧",
                value: "05",
                checked: false
            }], //业态
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({ profitRuleType: '00' }, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.xsleList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                })

            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addGoods()
            }
        },
        //00表示停用 01 表示启用
        upDown(row) {
            if (row.status == '01') {
                this.$confirm(
                    "销售分润规则停用，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        putObj(Object.assign({}, { status: '00', id: row.id, profitRuleType: '00' })).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "停用成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                putObj(Object.assign({}, { status: '01', id: row.id, profitRuleType: '00' })).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "启用成功!"
                    });
                })
            }
        },
        // 新增
        addGoods() {
            this.title = 1
            this.dialogFormVisible = true
            this.ruleForm = {
                businessType: [],
                status: '01',
                ruleTypeList1: [{
                    label: '商超',
                    businessTypeDisabled: true,
                    businessType: '02',
                    meteringUnit: '00'
                }, {
                    label: '洗衣',
                    businessTypeDisabled: true,
                    businessType: '03',
                    meteringUnit: '00'
                }, {
                    label: '家政',
                    businessTypeDisabled: true,
                    businessType: '04',
                    meteringUnit: '00'
                }, {
                    label: '咖吧',
                    businessTypeDisabled: true,
                    businessType: '05',
                    meteringUnit: '00'
                }]
            }

            this.xsleList = [  //业态
                {
                    label: "商超",
                    value: "02",
                    checked: false
                },
                {
                    label: "洗衣",
                    value: "03",
                    checked: false
                },
                {
                    label: "家政",
                    value: "04",
                    checked: false
                },
                {
                    label: "咖吧",
                    value: "05",
                    checked: false
                }
            ]
        },
        edit(row) {
            this.title = 0
            this.dialogFormVisible = true
            this.ruleForm = {
                businessType: [],
                ruleTypeList1: [{
                    label: '商超',
                    businessTypeDisabled: true,
                    businessType: '02',
                    meteringUnit: '00'
                }, {
                    label: '洗衣',
                    businessTypeDisabled: true,
                    businessType: '03',
                    meteringUnit: '00'
                }, {
                    label: '家政',
                    businessTypeDisabled: true,
                    businessType: '04',
                    meteringUnit: '00'
                }, {
                    label: '咖吧',
                    businessTypeDisabled: true,
                    businessType: '05',
                    meteringUnit: '00'
                }]
            }
            getObj(row.id).then(val => {
                console.log(val)
                let data = val.data.data
                this.ruleForm = data
                this.ruleForm.businessType = this.ruleForm.businessType.split(',')
                // this.ruleForm.ruleTypeList.forEach(val => {
                //     this.xsleList.forEach(items => {
                //         if (val.businessType == items.value) {
                //             val.label = items.label
                //         }
                //     })
                //     if (val.meteringUnit == '01') {
                //         val.commissionNum = ''
                //         val.disabled = true
                //     } else {
                //         val.benefitProportion = ''
                //         val.disabled = false
                //     }
                // })
                this.ruleForm.ruleTypeList1 = [{
                    label: '商超',
                    businessTypeDisabled: true,
                    businessType: '02',
                    meteringUnit: '00'
                }, {
                    label: '洗衣',
                    businessTypeDisabled: true,
                    businessType: '03',
                    meteringUnit: '00'
                }, {
                    label: '家政',
                    businessTypeDisabled: true,
                    businessType: '04',
                    meteringUnit: '00'
                }, {
                    label: '咖吧',
                    businessTypeDisabled: true,
                    businessType: '05',
                    meteringUnit: '00'
                }]
                this.ruleForm.ruleTypeList.forEach((item) => {
                    if (item.benefitProportion || item.commissionNum) {
                        if (item.meteringUnit == '01') {
                            item.commissionNum = undefined
                            item.disabled = true
                        } else {
                            item.benefitProportion = undefined
                            item.disabled = false
                        }
                        this.ruleForm.ruleTypeList1.forEach((items, index) => {
                            if (item.businessType == items.businessType) {
                                item.businessTypeDisabled = false
                                Object.assign(items, item)  //替换数据
                                this.xsleList[index].checked = true  //业态选中
                            }
                        })
                    }
                })

                console.log(this.ruleForm.ruleTypeList1)
            })
        },
        handleSave() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loading = true
                    this.ruleForm.ruleTypeList = []
                    this.ruleForm.ruleTypeList1.forEach(item => {
                        if (item.benefitProportion || item.commissionNum) {
                            this.ruleForm.ruleTypeList.push(item)
                        }
                    })
                    addObj(Object.assign(this.ruleForm, { profitRuleType: '00' })).then(data => {
                        this.$message.success('添加成功')
                        this.loading = false
                        this.dialogFormVisible = false
                        this.getList(this.page)
                    }).catch(() => {
                        this.loading = false
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },

        handleUpdate() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loading = true
                    this.ruleForm.ruleTypeList = []
                    this.ruleForm.ruleTypeList1.forEach(item => {
                        if (item.benefitProportion || item.commissionNum) {
                            this.ruleForm.ruleTypeList.push(item)
                        }
                    })
                    editObj(this.ruleForm, { profitRuleType: '00' }).then(data => {
                        this.$message.success('修改成功')
                        this.dialogFormVisible = false
                        this.loading = false
                        this.getList(this.page)
                    }).catch(() => {
                        this.loading = false
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });

        },
        businessTypeChange(e, index, row) {  //业态触发
            console.log(e)
            // this.ruleForm.ruleTypeList = []
            // this.ruleForm.businessType.forEach(item => {
            //     this.xsleList.forEach(items => {
            //         if (item == items.value) {
            //             this.ruleForm.ruleTypeList.push({ label: items.label, businessType: item, meteringUnit: '00' })
            //         }
            //     })
            // })
            // this.ruleForm.ruleTypeList = this.distinct(this.ruleForm.ruleTypeList, 'label')

            if (!row.checked) {
                row.checked = true
            } else {
                row.checked = false
            }
            let list = [...this.ruleForm.ruleTypeList1]
            if (!row.checked) {
                list[index].businessTypeDisabled = true
                list[index].commissionNum = undefined
                list[index].benefitProportion = undefined
            } else {
                list[index].businessTypeDisabled = false
            }
            this.ruleForm.ruleTypeList1 = list
        },
        meteringUnitChange(e, idx) {
            let list = [...this.ruleForm.ruleTypeList1]
            list.forEach((item, index) => {
                if (index == idx) {
                    if (!this.ruleForm.id) {  //判断是否是新增还是编辑
                        if (e == '01') {
                            item.commissionNum = undefined
                            item.disabled = true
                        } else {
                            item.benefitProportion = undefined
                            item.disabled = false
                        }
                    } else {
                        if (e == '01') {
                            item.benefitProportion = undefined
                            item.disabled = false
                        } else {
                            item.commissionNum = undefined
                            item.disabled = true
                        }
                    }
                }

            })
            this.ruleForm.ruleTypeList1 = list
            this.$forceUpdate()
        },
        distinct(arr, key) {  //数组去重
            var newobj = {}, newArr = [];
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i];
                if (!newobj[item[key]]) {
                    newobj[item[key]] = newArr.push(item);
                }
            }
            return newArr;

        },
    }
} 