import search from "./search/search.js";
import ServiceCommissionsRule from "./ServiceCommissionsRule_list/ServiceCommissionsRule.vue";

export default {
    mixins: [search],
    components: {
        ServiceCommissionsRule,
    },
    data() {
        return {
            
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.ServiceCommissionsRule.getList()      
        }
    }
}