import { tableOption } from './closeTrade.config.js'
import search from './../search/search.js';
import { workorderinfointegralpage,workorderinforecord,workorderinfodeploy,workorderinforeDeploy } from '@/api/shop/pointsMall/workOrderIntegral.js'

export default {
    name: "closeTrade",
    mixins: [search],
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            dialogVisible: false,
            ruleForm: {
                payOrderNo:'',
                paySerials: [],
                refundReason: []
            },
            rules: {},
            loadingSure: false,
            gdjlloading: false,
            gdjlloadData: [{}],
            gdjltableOption: {
                header: false,
                align: 'center',
                editBtn: false,
                delBtn: false,
                page: false,
                menu: false,
                column: [
                    {
                        label: '工单操作',
                        prop: 'status',
                        minWidth: 120,
                        dicData:[
                            {
                                label:'派单',
                                value:'00'
                            },
                            {
                                label:'拒单',
                                value:'01'
                            },
                            {
                                label:'重新派单',
                                value:'02'
                            },
                            {
                                label:'接受工单',
                                value:'03'
                            },
                            {
                                label:'缺人开始服务',
                                value:'04'
                            },
                            {
                                label:'确认已完成',
                                value:'05'
                            }
                        ]
                    },
                    {
                        label: '操作人',
                        prop: 'operatorName',
                        minWidth: 120
                    },
                    {
                        label: '操作时间',
                        prop: 'operatorTime',
                        minWidth: 120
                    },
           ] },
           searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
                
            }
            if (page) {
                this.page.current = page
            }
            workorderinfointegralpage(Object.assign({status:'05'}, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
              console.log(666, this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        getgongdanList(workorderInfoId) {
            workorderinforecord({workorderInfoId:workorderInfoId}).then(val => {
                let data = val.data.data
                this.gdjlloadData = data
            })
        },
        gongzuojilu(workorderInfoId) {
            this.dialogVisible = true;
            this.getgongdanList(workorderInfoId)
        },
        dispatchs(row) {
            if (row.status == '00') {
                this.$confirm(
                    "确定派单？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        workorderinfodeploy(Object.assign({}, { personId: row.personId },{subOrderId:row.subOrderId})).then(val => {
                            this.$emit("trigger",1) //1重新加载其他tab请求
                            this.$message({
                                type: "success",
                                message: "派单成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                workorderinforeDeploy(Object.assign({}, { personId: row.personId },{subOrderId:row.subOrderId})).then(val => {
                    this.$emit("trigger",1) //1重新加载其他tab请求
                    this.$message({
                        type: "success",
                        message: "重新派单成功!"
                    });
                })
            }
        },
        tuihuo(item) {
            this.dialogVisible = true;
            // this.ruleForm.refundAmt = item.orderAmt;
            let num = 0
           item.paySerials.forEach(item => {
                num += item.payAmt
            });
            this.ruleForm.refundAmt = num
            console.log(333, this.ruleForm.refundAmt)
            this.ruleForm.payOrderNo = item.payOrderNo;
            this.ruleForm.subOrderId = item.subOrderId;
            this.ruleForm.paySerials = item.paySerials;
            this.ruleForm.refundReason = []
            console.log(666, item)
        },
        handlSure() {
            this.ruleForm.refundReason =   this.ruleForm.refundReason.join(',')
            refund(this.ruleForm).then(val => {

            })
        }
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 