const DIC = {
    status: [{
        label: '待领取 ',
        value: '00'
    }, {
        label: '已领取 ',
        value: '01'
    }, ],


}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 300,
    column: [

        {
            label: '工单号',
            prop: 'workorderInfoId',
            width:150,
            overHidden: true,
        },
        {
            label: '订单号',
            prop: 'subOrderId',
            width:150,
            overHidden: true,

        },
        {
            label: '积分商品',
            prop: 'goodsName',
            width:200,
            overHidden: true,
        },
        {
            label: '数量',
            prop: 'payGoodsNum',
            width: 120,
            overHidden: true,
        }, {
            label: '用户手机号',
            prop: 'phone',
            width: 120,
            overHidden: true,
        }, {
            label: '所属门店',
            prop: 'shopName',
            width:200,
            overHidden: true,
        },  {
            label: '工单状态',
            prop: 'status',
            dicData: DIC.status,
            width: 120
        },
    ]
}

