const DIC = {
    status: [{
        label: '待派单 ',
        value: '00'
    }, {
        label: '待接单 ',
        value: '01'
    }, {
        label: '已拒单 ',
        value: '02'
    }, {
        label: '待服务 ',
        value: '03'
    }, {
        label: '服务中 ',
        value: '04'
    }, {
        label: '服务完成 ',
        value: '05'
    }],


}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 300,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '工单号',
            prop: 'workorderInfoId',
            width:150,
            overHidden: true,
        },
        {
            label: '订单号',
            prop: 'subOrderId',
            width:150,
            overHidden: true,

        },
        {
            label: '服务时间',
            prop: 'serviceDate',
            width:200,
            overHidden: true,
        },
        {
            label: '收件人',
            prop: 'receiver',
            width: 120,
            overHidden: true,
        }, {
            label: '联系电话',
            prop: 'phone',
            width: 120,
            overHidden: true,
        }, {
            label: '收货地址',
            prop: 'receiveAddress',
            width:200,
            overHidden: true,
        }, {
            label: '服务人员',
            prop: 'personName',
            slot: true,
            overHidden: true,
            width: 150
        }, {
            label: '所属门店',
            prop: 'shopName',
            overHidden: true,
            width: 120
        }, {
            label: '派单时间',
            prop: 'dispatchTime',
            overHidden: true,
            width: 120
        }, {
            label: '工单状态',
            prop: 'status',
            dicData: DIC.status,
            width: 120
        },
    ]
}

