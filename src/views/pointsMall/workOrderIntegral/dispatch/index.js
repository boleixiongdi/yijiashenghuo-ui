import allList from "./all_list/all_list.vue";
import sleeping from "./sleeping/sleeping.vue";
import servering from "./servering/servering.vue";
import { paidanObj } from '@/api/shop/order/housekeeping.js'
export default {
    components: {
        allList,
        sleeping,
        servering
    },
    data() {
        return {
            dialogVisible: false,
            dialogVisibleTile: "",
            loading: false,
            ruleForm: {},
            keyName: "1",
            tab: [
                {
                    label: "全部",
                    value: "1"
                },
                {
                    label: "休息中",
                    value: "2"
                }, {
                    label: "服务中",
                    value: "3"
                }
            ]
        };
    },
    methods: {
        getList(title, data) {
            this.dialogVisible = true
            this.dialogVisibleTile = title
            this.ruleForm = data
            console.log(this.ruleForm)
        },
        callback(key) {
            this.keyName = key
            switch (this.keyName) {
                case '1':
                    this.$refs.allList.getList() //全部
                    break;
                case '2':
                    this.$refs.sleeping.getList() //休息中
                    break;
                case '3':
                    this.$refs.servering.getList() //服务中
                    break;
                default:
                    break;
            }
        },
        handleClose() {
            this.dialogVisible = false
            paidanObj(Object.assign({personId:this.$refs.allList.loadData.personId},{subOrderId:this.ruleForm.subOrderId}))
        },
        handleSure() {
            this.loading = true
            this.dialogVisible = false
        },

    }
};