export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    column: [
        {
            label: '服务人员姓名',
            prop: 'personName',
            minWidth: 120
        },
        {
            label: '当前状态',
            prop: 'serviceStatus',
            minWidth: 120
        },
       /*  {
            label: '待服务工单数',
            prop: 'gongdan',
            minWidth: 120
        }, */
        {
            label: '已占用服务时间及服务地址',
            prop: 'timeAndAddress',
            minWidth: 240
        },
    ]
}

