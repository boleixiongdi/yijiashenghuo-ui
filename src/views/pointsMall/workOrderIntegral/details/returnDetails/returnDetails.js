import comTitle from '@/views/com/com_title.vue'
import tables from './table.vue'
export default {
    components: {
        comTitle,
        tables
    },
    data() {
        return {
            info: {
                title: "基本信息",
                color:'#000',
                gutter:0,
                formList: [
                    {
                        label: "退货单编号：",
                        value: "400014002",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "退货时间：",
                        value: "2020-01-01  18:18:18",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "操作人：",
                        value: "Nadia",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "退货备注：",
                        value: "请发送顺丰快递",
                        smSpan: 24,
                        smOffset: 0
                    },
                ],
            },
            refund: {
                title: "小计与收款：",
                color:'#000',
                gutter:0,
                formList: [{
                    label: "退款金额：",
                    value: "￥100.00",
                    smSpan: 24,
                    smOffset: 0
                }, {
                    label: "退款方式：",
                    children:[
                        {
                            label:"支付宝：",
                            value:"￥50.00"
                        },
                        {
                            label:"储值卡：",
                            value:"￥50.00"
                        }
                    ],
                    smSpan: 24,
                    smOffset: 0
                },]
            }
        }
    },
    methods: {

    }

} 