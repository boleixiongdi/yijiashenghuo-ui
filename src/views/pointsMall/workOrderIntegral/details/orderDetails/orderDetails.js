import comTitle from '@/views/com/com_title.vue'
import tables from './table.vue'
import {detail } from '@/api/shop/order/housekeeping.js'
export default {

    components: {
        comTitle,
        tables
    },
    props: {
        isOnline: {
            type: String
        }
    },
    data() {
        return {
            info: {
                title: "基本信息",
                color:'#000',
                gutter:0,
                formList: [
                    {
                        label: "订单编号：",
                        value: "400014002",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "创建时间：",
                        value: "2020-01-01  18:18:18",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "联系电话：",
                        value: "159****2411",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "成交时间：",
                        value: "2020-2-2 12:20",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "交易流水号：",
                        value: "1234562411",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单来源：",
                        value: "线下",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "门店：",
                        value: "XX门店",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收银员：",
                        value: "Nadia",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单备注：",
                        value: "请发送顺丰快递",
                        smSpan: 24,
                        smOffset: 0
                    },
                ],
            },
            server: {
                title: "服务信息",
                color:'#000',
                gutter:0,
                formList: [
                    {
                        label: "配送方式：",
                        value: "上门配送",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "联系电话：",
                        value: "159****1303",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收货地址：",
                        value: "浙江省杭州市西湖区转塘街道*****几号",
                        smSpan: 7,
                        smOffset: 0
                    },
                ]
            },
            discount: {
                title: "折扣信息：",
                color:'#000',
                gutter:0,
                formList: [
                    {
                        label: "优惠券：",
                        value: "7.5折品类折扣券",
                        smSpan: 7,
                        smOffset: 0
                    },]
            },
            collection: {
                title: "小计与收款：",
                color:'#000',
                gutter:0,
                formList: [
                    {
                        label: "商品数量：",
                        value: "7.5折品类折扣券",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "定单金额：",
                        value: "￥300.00",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "配送费：",
                        value: "￥10.00",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "会员优惠金额：",
                        value: "-￥10.00",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "优惠券优惠金额：",
                        value: "-￥20.00",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "实收金额：",
                        value: "￥100.00",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "支付详情：",
                        children: [
                            {
                                label: "支付宝：",
                                value: "￥50.00",
                            }, {
                                label: "储值卡：",
                                value: "￥50.00",
                            }
                        ],
                        smSpan: 24,
                        smOffset: 0
                    },]
            }
        }
    },
    methods: {
        getDetail(){
            detail(this.$route.query.id).then(val => {
            })
        }
    },
    created() {
        this. getDetail()
    }

} 