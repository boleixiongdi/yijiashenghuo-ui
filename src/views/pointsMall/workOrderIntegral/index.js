import search from "./search/search.js";
import allOrder from "./allOrder/allOrder.vue";
import waitingOrder from "./waitingOrder/waitingOrder.vue";
import servering from "./servering/servering.vue";
import successfulTrade from "./successfulTrade/successfulTrade.vue";
import partialReturn from "./partialReturn/partialReturn.vue";
import closeTrade from "./closeTrade/closeTrade.vue";

const tabName = [
    "全部工单",
    "待领取",
    "已领取"
]
export default {
    mixins: [search],
    components: {
        allOrder,
        waitingOrder,
        servering,
        successfulTrade,
        partialReturn,
        closeTrade
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allOrder: "全部工单",
                waitingOrder: "待领取",
                successfulTrade: "已领取",

            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList() //全部订单列表
                    break;
                case '2':
                    this.$refs.waitingOrder.getList() //待派单列表
                    break;
                case '3':
                    this.$refs.successfulTrade.getList() //交易成功列表
                    break;
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allOrder.getList()       //全部订单列表
            this.$refs.waitingOrder.getList()  //待派单列表
            this.$refs.successfulTrade.getList() //交易成功列表
        },
        trigger(st) {
            if (st) {
                this.getList()
            } else {
                this.tab.allOrder = `${tabName[0]}(${this.$refs.allOrder.page.total})`
                this.tab.waitingOrder = `${tabName[1]}(${this.$refs.waitingOrder.page.total})`
                this.tab.successfulTrade = `${tabName[2]}(${this.$refs.successfulTrade.page.total})`
               
            }
            this.$forceUpdate()
        }
    }
}