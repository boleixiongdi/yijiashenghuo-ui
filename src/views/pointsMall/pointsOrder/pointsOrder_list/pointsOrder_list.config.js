export const tableOption = {
    selection: true,
    reserveSelection: false,
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [{
        label: '序号',
        prop: 'indexLabel',
        slot: true,
        width:80
    },{
            label: '订单号',
            prop: 'subOrderId',
            width:150,
            overHidden: true,
        },
        {
            label: '订单来源',
            prop: 'orderSource',
            dicData: [{
                    label: "小程序",
                    value: "00"
                },
                {
                    label: "APP",
                    value: "01"
                },
                {
                    label: "线下",
                    value: "02"
                }
            ]

        },
        {
            label: '门店',
            prop: 'shopName',
            width:150,
            overHidden: true,
        },
        {
            label: '商品数量',
            prop: 'goodsNum',


        }, {
            label: '会员',
            prop: 'personId',

        }, {
            label: '花费积分（个）',
            prop: 'integral',

        }, {
            label: '创建时间',
            prop: 'createTime',
            overHidden: true,
        }
    ]
}