import { tableOption } from './pointsOrder_list.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { integralpage } from '@/api/shop/pointsMall/pointsOrder.js'
export default {
    name: "pointsOrder_list",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            integralpage(Object.assign({}, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        details(item) {
            this.$router.push({path:'/pointsMall/pointsOrder/details',query:{id:item.subOrderId}})
        }
    }

} 