import search from "./search/search.js";
import pointsOrderList from "./pointsOrder_list/pointsOrder_list.vue";

export default {
    mixins: [search],
    components: {
        pointsOrderList,
    },
    data() {
        return {
            
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.pointsOrderList.getList()       //审核通过列表
        }
    }
}