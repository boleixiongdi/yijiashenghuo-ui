import comTitle from '@/views/com/com_title.vue'
import tables from './table.vue'
import { integralsub } from '@/api/shop/pointsMall/pointsOrder.js'
export default {

    components: {
        comTitle,
        tables
    },
    props: {

        isOnline: {
            type: String
        }
    },
    data() {
        return {
            Tabdata: [],
            info: {
                title: "基本信息",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "订单编号：",
                        value: '未填报',
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "创建时间：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "成交时间：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "交易流水号：",
                        value: '未填报',
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "会员联系电话：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单来源：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "门店：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收银员：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "订单备注：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    },
                ],
            },
            shop: {
                title: "商品信息",
                color: '#000',
                gutter: 0,
            },
            server: {
                title: "配送信息",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "配送方式：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收件人：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "联系电话：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                    {
                        label: "收货地址：",
                        value: "未填报",
                        smSpan: 7,
                        smOffset: 0
                    },
                ]
            },
            // discount: {
            //     title: "折扣信息：",
            //     color: '#000',
            //     gutter: 0,
            //     formList: [
            //         {
            //             label: "优惠券：",
            //             value: "couponName",
            //             smSpan: 7,
            //             smOffset: 0
            //         },]
            // },
            collection: {
                title: "小计：",
                color: '#000',
                gutter: 0,
                formList: [
                    {
                        label: "商品数量：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "配送费：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    }, {
                        label: "花费积分：",
                        value: "未填报",
                        smSpan: 24,
                        smOffset: 0
                    },
                ]
            }
        }
    },
    methods: {
        getDetail() {
            integralsub(this.$route.query.id).then(val => {
                let data = val.data.data

                if (data.infoDto) {
                    this.info = {
                        title: "基本信息",
                        color: '#000',
                        gutter: 0,
                        formList: [
                            {
                                label: "订单编号：",
                                value: data.infoDto.payOrderNo,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "创建时间：",
                                value: data.infoDto.createTime,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "成交时间：",
                                value: data.infoDto.orderTime,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "交易流水号：",
                                value: data.infoDto.paySerialNo,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "会员联系电话：",
                                value: data.infoDto.phoneNum,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "订单来源：",
                                value: (data.infoDto.orderSource == '00' ? '小程序' : (data.infoDto.orderSource == '01' ? 'APP' : (data.infoDto.orderSource == '02' ? '线下' : ''))),
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "门店：",
                                value: data.infoDto.shopName,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "收银员：",
                                value: data.infoDto.operatorId,
                                smSpan: 7,
                                smOffset: 0
                            },
                            {
                                label: "订单备注：",
                                value: data.infoDto.body,
                                smSpan: 24,
                                smOffset: 0
                            },
                        ],
                    }
                    this.collection = {
                        title: "小计：",
                        color: '#000',
                        gutter: 0,
                        formList: [
                            {
                                label: "商品数量：",
                                value: data.goodsDetailDto.length,
                                smSpan: 24,
                                smOffset: 0
                            },
                            // {
                            //     label: "配送费：",
                            //     value: "未填报",
                            //     smSpan: 24,
                            //     smOffset: 0
                            // }, 
                            {
                                label: "花费积分：",
                                value: data.infoDto.subOrderAmt,
                                smSpan: 24,
                                smOffset: 0
                            },]
                    }
                }

                console.log(this.collection)
            })
        }
    },
    created() {
        this.getDetail()

    }

} 