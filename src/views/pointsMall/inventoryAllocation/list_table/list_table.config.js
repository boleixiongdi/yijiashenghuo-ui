export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '商品ID',
            prop: 'goodsId',
            width:150,
            overHidden: true,
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            width:150,
            overHidden: true,
        },
        {
            label: '库存单位',
            prop: 'stockUnit',

        },
        {
            label: '库存数量',
            prop: 'stockNum',
        },
        {
            label: '已兑换数量',
            prop: 'payGoodsNum',

        },
        {
            label: '销售渠道',
            prop: 'saleScope',
            dicData:[
                {
                    label:'全部渠道',
                    value:'00'
                },
                {
                    label:'线上',
                    value:'01'
                },
                {
                    label:'线下',
                    value:'02'
                }
            ]

        }
    ]
}



export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '门店名称',
            prop: 'shopName',
        },
        {
            label: '已兑换数量',
            prop: 'payGoodsNum',

        },
        {
            label: '分配库存数量',
            prop: 'stockNum',
            slot:true
        },
    ]
}
