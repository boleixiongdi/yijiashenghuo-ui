import { tableOption, tableOption1 } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { getIntgralGoodsList, getIntgralGoodsUnitList, doStockDistribution } from '@/api/shop/pointsMall/inventoryAllocation.js'
import comTitle from "@/views/com/com_title.vue";

export default {
    name: "list_table",
    mixins: [search],
    components: {
        comTitle
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                // {
                //     title: "新增采购单",
                //     isShow: true
                // },
                // {
                //     title: "导出",
                //     isShow: true
                // }
            ],
            selectedRows: [],  //选中的数组

            dialogVisible: false,   //库存流水
            tableOption1: tableOption1,
            loading1: false,
            loadData1: [],
            searchForm: {},
            ruleForm: {},
            number: 0
        }
    },
    watch: {
        number(val) {
            if (val < 0) {
                this.$message.warning('待分配库存不能小于0')
            }
        }
    },
    mounted() {
        // this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            this.loading = true
            getIntgralGoodsList(Object.assign({}, searchForm ? searchForm : this.searchForm, this.page)).then(val => {
                let data = val.data.data
                this.loadData = data.records
                console.log(data)
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loading = false
            }).catch(() => {
                this.loading = false
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.export()
            }
        },
        kucunLs(item) {  //编辑
            this.dialogVisible = true
            this.loading1 = true
            this.ruleForm = item
            this.ruleForm.allocated = 0
            console.log(this.ruleForm)
            getIntgralGoodsUnitList({ goodsId: item.goodsId }).then(val => {
                let data = val.data.data
                this.loadData1 = data
                this.loading1 = false
                let num = 0
                this.loadData1.forEach(item => {
                    item.disabled = false
                    num += parseInt(item.stockNum)
                });
                this.ruleForm.allocated = num
                this.number = parseInt(this.ruleForm.stockNum - this.ruleForm.payGoodsNum - this.ruleForm.allocated)
                console.log(number)
            }).catch(() => {
                this.loading1 = false
            })
        },
        export() {

        },
        sure(row) {
            if (this.number > 0) {
                doStockDistribution(Object.assign(row, { goodsId: this.ruleForm.goodsId })).then(val => {
                    this.$message.success('保存成功')
                    let list = [... this.loadData1]
                    list.forEach(item => {
                        if (item.$index == row.$index) {
                            item.disabled = false
                        }
                    })
                    this.loadData1 = list
                    this.getList()
                })
            } else {
                this.$message.warning('分配库存数量超出待分配库存数量')
            }

        },
        edit(index1) {
            console.log(index1)
            let list = [...this.loadData1]
            list.forEach((item, index) => {
                if (index1 == index) {
                    item.disabled = true
                }
            })
            this.loadData1 = list
        },
        close(index1) {
            console.log(index1)
            let list = [...this.loadData1]
            list.forEach((item, index) => {
                if (index1 == index) {
                    item.disabled = false
                }
            })
            this.loadData1 = list
        },
        stockNumChange(val, row) {
            let num = 0
            this.loadData1.forEach(item => {
                num += parseInt(item.stockNum)
            });
            this.ruleForm.allocated = num
            this.number = parseInt(this.ruleForm.stockNum - this.ruleForm.payGoodsNum - this.ruleForm.allocated)
        }
    }

} 