export const tableOption = {
    selection: false,
    reserveSelection: false,
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [{
        label: '序号',
        prop: 'indexLabel',
        slot: true,
        width: 80
    }, {
        label: '商品ID',
        prop: 'goodsId',
        width:150,
        overHidden: true,
    },
    {
        label: '商品名称',
        prop: 'goodsName',
        width:150,
        overHidden: true,

    },
    {
        label: '库存单位',
        prop: 'stockUnit',

    },
    {
        label: '所需积分',
        prop: 'exchangeNum',

    },
    {
        label: '库存数量',
        prop: 'stockNum',

    }, {
        label: '已兑数量',
        prop: 'convertUnitNum',

    },
    {
        label: '商品状态',
        prop: 'shelfState',
        dicData: [{
            label: "已上架",
            value: "01"
        },
        {
            label: "已下架",
            value: "00"
        }
        ]
    },
    ]
}