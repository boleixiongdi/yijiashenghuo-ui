import { tableOption } from './pointsGoods_list.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { integralPage, changeShelf, delObj } from '@/api/shop/pointsMall/pointsGoods.js'
export default {
    name: "pointsGoods_list",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [], //选中的数组
            btn: [{
                title: "新增",
                isShow: true,
                type: "primary",
                icon: "el-icon-plus"
            },
                // {
                //     title: "上架",
                //     isShow: true
                // },
                // {
                //     title: "下架",
                //     isShow: true
                // },
                // {
                //     title: "删除",
                //     isShow: true
                // },
                // {
                //     title: "导入商品",
                //     isShow: true
                // },
            ],
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            integralPage(Object.assign({}, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addGoods()
            } else if (index == 1) {
                this.upDown(1)
            } else if (index == 2) {
                this.upDown(0)
            } else if (index == 3) {
                this.delete()
            } else if (index == 4) {
                this.import()
            }
        },
        // 新增
        addGoods() {
            this.$router.push({ path: '/pointsMall/pointsGoods/addGoods' })
        },
        //1上架 0下架
        switchs(row) {
            console.log("row.shelfState", (row.shelfState == '00'));
            if (row.shelfState == '00') {
                this.$confirm(
                    "是否上架该商品，是否继续？",
                    "提示", {
                    confirmButtonText: "确定",
                    cancelButtonText: "取消",
                    type: "warning"
                }
                )
                    .then(() => {
                        changeShelf({ shelfState: '01', goodsName: row.goodsName, barCode: row.barCode,id:row.id }).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "上架成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                changeShelf({ shelfState: '00', goodsName: row.goodsName, barCode: row.barCode ,id:row.id}).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "下架成功!"
                    });
                })
            }
        },
        // 删除
        del(row) {
            var _this = this
            this.$confirm('是否确认删除商品ID为"' + row.goodsId + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('删除成功')
            }).catch(function () { })
        },
        // 导入商品
        import() {

        },
        edit(item) {

            this.$router.push({ path: '/pointsMall/pointsGoods/addGoods', query: { id: item.goodsId } })
        },
    }

}