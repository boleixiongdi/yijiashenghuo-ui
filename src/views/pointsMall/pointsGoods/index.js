import search from "./search/search.js";
import pointsGoodsList from "./pointsGoods_list/pointsGoods_list.vue";

export default {
    mixins: [search],
    components: {
        pointsGoodsList,
    },
    data() {
        return {
            
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.pointsGoodsList.getList()      
        }
    }
}