export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    index: true,
    menu: false,
    indexLabel: '序号',
    column: [
        {
            label: '供应商名称',
            prop: 'ID',

        },
        {
            label: '业态',
            prop: 'phone',


        },
        {
            label: '商品编号',
            prop: 'name',

        },
        {
            label: '商品名称',
            prop: 'fs',


        }, {
            label: '商品图片',
            prop: 'dh',

            slot: true,
        }, {
            label: '商品类目',
            prop: 'dz',

        }, {
            label: '供应价格（元）',
            prop: 'je',

        },
    ]
}

