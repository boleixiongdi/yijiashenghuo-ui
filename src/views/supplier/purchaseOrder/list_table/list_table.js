import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
export default {
    name: "list_table",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                currentPage: 1,
                total: 0,
                pageSize: 10
            },
            btn:[
                {
                    title:"新增",
                    isShow:true
                }
            ]
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            setTimeout(() => {
                this.page.total = 200
            }, 500)
        },
        sizeChange(currentPage) {
            this.page.currentPage = currentPage
        },
        currentChange(pageSize) {
            this.page.pageSize = pageSize
        },
        handler(index) {
            if (index == 0) {
                this.addOrder()
            }
        },
        // 删除
        delete() {
            console.log(123321)
        },
        details(item) {
            this.$router.push({ path: '/member/list/details', query: item })
        },
        addOrder(){

        }
    }

} 