import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import addList from "./addList.vue";
import { fetchList, addObj, editObj, statusObj } from '@/api/shop/supplier/list.js'
export default {
    name: "list_table",
    mixins: [search],
    components: {
        addList
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadingSure: false,
            dialogTitle: 1,
            dialogVisible: false,
            loadData: [{
                zt: 0
            }, {
                zt: 1
            }],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增",
                    isShow: true
                }
            ],

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            fetchList(Object.assign({}, searchForm ? searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.$store.getters.businessTypeList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                })
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        switchs(row) {         // 停用
            if (row.status == '00') {
                this.$confirm(
                    "供应商停用，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        statusObj(Object.assign({ status: "01" }, { merchantId: row.merchantId })).then((val) => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "供应商停用成功!"
                            });

                        })

                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消供应商停用"
                        });
                    });
            } else {
                statusObj(Object.assign({ status: "00" }, { merchantId: row.merchantId })).then((val) => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "供应商启用成功!"
                    })

                })
                    ;
            }

        },
        edit(item) {  //编辑
            this.dialogVisible = true
            this.dialogTitle = 0
            this.$nextTick(() => {
                let obj = { ...item }
                this.$refs.addList.ruleForm = obj
                if (this.$refs.addList.ruleForm.businessType) {
                    if (this.$refs.addList.ruleForm.businessType.indexOf(',') > -1) {
                        this.$refs.addList.ruleForm.businessType = this.$refs.addList.ruleForm.businessType.split(',')
                    } else {
                        let list = []
                        list.push(this.$refs.addList.ruleForm.businessType)
                        this.$refs.addList.ruleForm.businessType = list
                    }
                } else {
                    this.$refs.addList.ruleForm.businessType = []
                }
                console.log(this.$refs.addList.ruleForm)
            })

        },
        supplierList(item) {   //供应商列表
            this.$router.push({
                path: '/supplier/goods/index',
                query: { suplierMerchantId: item.merchantId }
            })
        },
        addSupplier() {  //新增
            this.dialogVisible = true
            this.dialogTitle = 1
        },
        handlSure() {  //确定
            let obj = {...this.$refs.addList.ruleForm}
            if (obj.businessType) {
                obj.businessType = obj.businessType.join(',')
            }
            console.log(obj)
            this.$refs.addList.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    console.log()
                    if (obj.id) {
                        editObj(obj).then(() => {
                            this.$message.success('添加成功')
                            this.getList(this.page)
                            this.dialogVisible = false
                        })
                    } else {
                        addObj(obj).then(() => {
                            this.$message.success('添加成功')
                            this.getList(this.page)
                            this.dialogVisible = false
                        })
                    }

                } else {
                    return false;
                }
            });
        },
        handlClose() {
            this.dialogVisible = false
        }
    }

} 