const DIC = {
    status: [{
        label: '启用',
        value: '00'
    }, {
        label: '停用',
        value: '01'
    }],
    businessType: [
        {
            label: '商超',
            value: '02'
        }, {
            label: '洗衣',
            value: '03'
        }, {
            label: '家政',
            value: '04'
        }, {
            label: '咖吧',
            value: '05'
        }],

}

export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '供应商编号',
            prop: 'merchantId',
            width:180,
            overHidden: true,
        },
        {
            label: '供应商名称',
            prop: 'companyName',
            width:180,
            overHidden: true,
        },
        {
            label: '供应商地址',
            prop: 'companyAdress',
            width:180,
            overHidden: true,

        },
        {
            label: '联系人',
            prop: 'contacts',
            overHidden: true,
        },
        {
            label: '联系电话',
            prop: 'contactsNum',
            overHidden: true,

        }, {
            label: '涉及业态',
            prop: 'businessType1',
            // dicData: DIC.businessType,
            width:150,
            overHidden: true,
        }, {
            label: '状态',
            prop: 'status',
            dicData: DIC.status,

        },
    ]
}

