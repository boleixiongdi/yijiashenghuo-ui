export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '供应商名称',
            prop: 'companyName',

        },
        {
            label: '业态',
            prop: 'businessType1',


        },
        {
            label: '商品编号',
            prop: 'goodsId',

        },
        {
            label: '商品名称',
            prop: 'goodsName',


        }, {
            label: '商品图片',
            prop: 'goodsPic',
            slot: true,
        }, {
            label: '商品类目',
            prop: 'categoryName',

        }, {
            label: '供应价格（元）',
            prop: 'costPrice',

        },
        {
            label: '库存单位',
            prop: 'stockUnit',

        },
    ]
}