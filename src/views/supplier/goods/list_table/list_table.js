import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { getGoods } from '@/api/shop/supplier/list.js'
import choiceGoods from './../choiceGoods.vue';
export default {
    name: "list_table",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    components: {
        choiceGoods
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增",
                    isShow: true
                }
            ],
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            // console.log(9898,Object.assign({suplierMerchantId:this.$route.query.id}, searchForm ? searchForm : {}, this.page))
            // return
            getGoods(Object.assign({ suplierMerchantId: this.$route.query.suplierMerchantId }, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                console.log(888, data)
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }

                this.loadData = data.records
                this.loadData.forEach(item => {
                    if (item.goodsPic && item.goodsPic.indexOf(',') > -1) {
                        // item.goodsPic = item.goodsPic.substring(item.goodsPic.indexOf('/') + 1, item.goodsPic.lastIndexOf(','))
                        item.goodsPic = item.goodsPic.split(',')[0]
                    }
                })
                this.loadData.forEach(item => {
                    item.businessType3 = []
                    item.businessType2 = item.businessType.split(',')
                    item.businessType2.forEach(i => {
                        this.$store.getters.businessTypeList.forEach(m => {
                            if (i == m.value) {
                                item.businessType3.push(m.label)

                            }
                        })
                    })
                    item.businessType1 = item.businessType3.join(',')
                })
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addBtn()
            }
        },
        // 删除
        delete() {
            console.log(123321)
        },
        details(item) {
            this.$router.push({ path: '/member/list/details', query: item })
        },
        addBtn() {
            this.$refs.choiceGoods.dialogVisible = true
            this.$refs.choiceGoods.dialogVisibleTile = '供应商商品添加'
            this.$refs.choiceGoods.getList()
            this.$store.getters.businessTypeList.forEach(item => {
                //为了避免categoryId为空的时候数据量太大 所以加载个1 使列表为空
                this.$refs.choiceGoods.getPustGoods({ categoryId: "1", businessType: item.value })
            })
        }
    }

} 