import { feedbackPage, delObj, replyFeedback } from '@/api/shop/configManage/index.js'
export default {
    data() {
        const DIC = {
            status: [{
                label: '禁用',
                value: '00'
            }, {
                label: '启用',
                value: '01'
            }],
            inOutType: [{
                label: '出库',
                value: '00'
            }, {
                label: '入库',
                value: '01'
            }],
        }
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width: 80
                    },
                    {
                        label: "问题描述",
                        prop: "problemDesc"
                    },
                    {
                        label: "问题图片",
                        prop: "pic",
                        slot: true,
                        width: 100
                    },
                    {
                        label: "用户手机号",
                        prop: "phoneNum",
                    },
                    {
                        label: "提交时间",
                        prop: "submitTime",

                    },
                    {
                        label: "回复信息",
                        prop: "replyMessag",
                        overHidden: true,
                    },
                    {
                        label: "回复人",
                        prop: "operatorId",

                    },
                    {
                        label: "回复时间",
                        prop: "replyTime",

                    },
                ]
            },
            rules: {
                replyMessag: [
                    { required: true, message: '请填写回复信息，不超过300个字', trigger: 'blur' },
                ],
            },
            ruleForm: {

            },
        };
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            feedbackPage(Object.assign({}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel(item) {
            this.dialogVisible = true
            this.ruleForm = item
        },
        handlSure() {
            let obj = {
                id: this.ruleForm.id,
                replyMessag: this.ruleForm.replyMessag
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    replyFeedback(obj).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message.success('回复成功')
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
            this.ruleForm = {}
        },
        del(row) {
            this.$confirm('是否确认删除回复?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                return delObj(row.id)
            }).then(() => {
                this.getList(this.page)
                this.$message.success('删除成功')
            }).catch(() => {
            })
        }

    }
};

