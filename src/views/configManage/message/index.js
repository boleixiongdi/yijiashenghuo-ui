import { messagenoticePage, delMessagenotice, addMessagenotice, putMessagenotice, recall } from '@/api/shop/configManage/index.js'
export default {
    data() {
        const DIC = {
            pushStatus: [{
                label: '已推送',
                value: '00'
            }, {
                label: '未推送',
                value: '01'
            }],
            pushObject: [{
                label: '会员',
                value: '00'
            }, {
                label: '商户',
                value: '01'
            }],
        }
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width: 80
                    },
                    {
                        label: "消息主题",
                        prop: "theme"
                    },
                    {
                        label: "推送对象",
                        prop: "pushObject",
                        dicData: DIC.pushObject,
                    },

                    {
                        label: "推送时间",
                        prop: "pushTime",
                    },
                    {
                        label: "推送人",
                        prop: "operatorId"
                    },
                    {
                        label: "推送状态",
                        prop: "pushStatus",
                        dicData: DIC.pushStatus,
                    },

                ]
            },
            rules: {
                theme: [
                    { required: true, message: '请输入消息主题', trigger: 'blur' },
                ],
                pushObject: [
                    { required: true, message: '请选择推送对象', trigger: 'change' },
                ],
            },
            ruleForm: {

            },
            objectList: [  //推送对象
                {
                    label: '会员',
                    value: '00'
                }, {
                    label: '商户',
                    value: '01'
                }
            ],
        };
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            messagenoticePage(Object.assign({}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel() {
            this.dialogVisible = true
            this.ruleForm = {}
        },
        switchs(row) {
            this.$confirm('是否确认撤回消息主题为"' + row.theme + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'success'
            }).then(() => {
                return recall(row.id)
            }).then(() => {
                this.$message.success('撤回成功')
                this.getList()
            }).catch(() => {
                this.$message.error('撤回失败')
            })
        },
        handlSure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    if (this.ruleForm.id) {
                        putMessagenotice(Object.assign(this.ruleForm)).then(val => {
                            this.getList()
                            this.handlClose()
                            this.$message({
                                message: '修改成功',
                                type: 'success',
                            })
                        }).catch(() => {
                            this.handlClose()
                        })
                    } else {
                        addMessagenotice(Object.assign(this.ruleForm)).then(val => {
                            this.getList()
                            this.handlClose()
                            this.$message({
                                message: '新增成功',
                                type: 'success',
                            })
                        }).catch(() => {
                            this.handlClose()
                        })
                    }

                } else {
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
            this.ruleForm = {}
        },
        edit(item) {
            this.dialogVisible = true
            this.ruleForm = item
        },
        del(row) {
            this.$confirm('是否确认删除消息主题为"' + row.theme + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                return delMessagenotice(row.id)
            }).then(() => {
                this.$message.success('删除成功')
                this.getList()
            }).catch(() => {
                this.$message.error('删除失败')
            })
        },
    }
};

