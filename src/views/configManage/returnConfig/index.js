import { addRefundreason, putRefundreason, refundreasonPage, delRefundreason } from '@/api/shop/configManage/index.js'
export default {
    data() {
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width: 80
                    },
                    {
                        label: "原因",
                        prop: "refundReason"
                    },

                ]
            },
            rules: {
                refundReason: [
                    { required: true, message: '请输入原因', trigger: 'blur' },
                ],
            },
            ruleForm: {

            },
            title: ''
        };
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            refundreasonPage(Object.assign({}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel() {
            this.dialogVisible = true
            this.title = '新增'
            this.ruleForm = {}
        },
        handlSure() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    if (this.ruleForm.id) {
                        putRefundreason(Object.assign(this.ruleForm)).then(val => {
                            this.getList()
                            this.handlClose()
                            this.$message({
                                message: '新增成功',
                                type: 'success',
                            })
                        }).catch(() => {
                            this.handlClose()
                        })
                    } else {
                        addRefundreason(Object.assign(this.ruleForm)).then(val => {
                            this.getList()
                            this.handlClose()
                            this.$message({
                                message: '修改成功',
                                type: 'success',
                            })
                        }).catch(() => {
                            this.handlClose()
                        })
                    }

                } else {
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
            this.ruleForm = {}
        },
        edit(item) {
            this.dialogVisible = true
            this.title = '编辑'
            this.ruleForm = item
        },
        del(row) {
            console.log(row)
            this.$confirm('是否确认删除原因为"' + row.refundReason + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                return delRefundreason(row.id)
            }).then(() => {
                this.$message.success('删除成功')
                this.getList()
            }).catch(() => {
                this.$message.error('删除失败')
            })
        },

    }
};

