export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    index: true,
    indexLabel: '排名',
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            overHidden:true,
        },
        {
            label: '销售单量',
            prop: 'payGoodsNum',
            overHidden:true,
        },
    ]
}

