import search from "./search/search.js";
import salesAnalysis from "./salesAnalysis/index.vue";

export default {
    mixins: [search],
    components: {
        salesAnalysis,
    },
    data() {
        return {
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.salesAnalysis.getList()       //数据中心列表
        },
    }
}