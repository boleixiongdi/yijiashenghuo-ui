import { tableOption } from './index.config.js'
import { mallfrontpageinfoLaundryList, mallfrontpageinfoUpdateEnable } from '@/api/shop/mallHome/laundry/list.js'
export default {
    name: "mallHome",
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            mallfrontpageinfoLaundryList().then(val => {
                this.loadData = val.data.data
            })
        },
        edit(row) {
            switch (row.areaType) {
                case '00':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '09':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '10':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '11':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '12':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '13':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '洗衣' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                    break;
                default:
                    break;
            }
        },
        switchChange(val, row) {
            const obj = { ...row }
            this.loading = true
            if (val == '00') {
                obj.enabled = '00'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('启用成功')
                    this.loading = false
                    this.getList()
                }).catch(()=>{
                    this.loading = false
                })
            } else {
                obj.enabled = '01'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('停用成功')
                    this.loading = false
                    this.getList()
                }).catch(()=>{
                    this.loading = false
                })
            }
        },

    }

} 