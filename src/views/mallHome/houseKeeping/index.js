import { tableOption } from './index.config.js'
import { mallfrontpageinfoHouseList, mallfrontpageinfoUpdateEnable } from '@/api/shop/mallHome/houseKeeping/list.js'
export default {
    name: "mallHome",
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            mallfrontpageinfoHouseList().then(val => {
                this.loadData = val.data.data
            })
        },
        edit(row) {
            switch (row.areaType) {
                case '00':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '家政' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '14':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '家政' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '15':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '家政' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '16':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '家政' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                case '17':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '家政' + row.areaName, id: row.id, businessType: row.businessType } })
                    break;
                default:
                    break;
            }
        },
        switchChange(val, row) {
            const obj = { ...row }
            this.loading = true
            if (val == '00') {
                obj.enabled = '00'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('启用成功')
                    this.loading = false
                    this.getList()
                }).catch(() => {
                    this.loading = false
                })
            } else {
                obj.enabled = '01'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('停用成功')
                    this.loading = false
                    this.getList()
                }).catch(() => {
                    this.loading = false
                })
            }
        },

    }

} 