export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menuWidth: 100,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        // {
        //     label: '商品规格',
        //     prop: 'stockUnit',
        // },
        {
            label: '商品价格(元)',
            prop: 'sellingPrice',
        },
    ]
}

