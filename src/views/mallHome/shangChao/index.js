import { tableOption } from './index.config.js'
import { mallfrontpageinfo, mallfrontpageinfoUpdateEnable } from '@/api/shop/mallHome/shangChao/list.js'
export default {
    name: "shangChao",
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            mallfrontpageinfo().then(val => {
                this.loadData = val.data.data
            })
        },
        edit(row) {
            switch (row.areaType) {
                case '00':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '01':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '02':
                    this.$router.push({ path: '/wel/mallHome/shangChao/coupon', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '03':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '04':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '05':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '06':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '07':
                    this.$router.push({ path: '/wel/mallHome/shangChao/pointsExchange', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                case '08':
                    this.$router.push({ path: '/wel/mallHome/shangChao/banner', query: { areaType: row.areaType, areaName: '商城' + row.areaName, id: row.id } })
                    break;
                default:
                    break;
            }
        },
        switchChange(val, row) {
            const obj = { ...row }
            this.loading = true
            if (val == '00') {
                obj.enabled = '00'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('启用成功')
                    this.loading = false
                    this.getList()
                }).catch(() => {
                    this.loading = false
                })
            } else {
                obj.enabled = '01'
                mallfrontpageinfoUpdateEnable(obj).then(val => {
                    this.$message.success('停用成功')
                    this.loading = false
                    this.getList()
                }).catch(() => {
                    this.loading = false
                })
            }
        },

    }

} 