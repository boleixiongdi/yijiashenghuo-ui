export const tableOption = {
    header: false,
    headerAlign: 'center',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    defaultExpandAll: false,
    column: [
        // {
        //     label: '序号',
        //     prop: 'id',
        //     width: 80
        // },
        {
            label: '区域位置',
            prop: 'areaName',

        },
        {
            label: '启用状态',
            prop: 'enabled',
            slot: true
        },
    ]
}

