import { tableOption } from './index.config.js'
import { mapGetters } from 'vuex'
import { mallfrontpageconfig, delMallfrontpageconfig } from '@/api/shop/mallHome/shangChao/list.js'
export default {
    name: "banner",
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                // addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                // delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                // editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                current: 1,
                total: 0,
                size: 10
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增",
                    isShow: true
                },
            ]
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            mallfrontpageconfig(Object.assign({}, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    current: data.current,
                    total: data.total,
                    size: data.size,
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    if (item.bannerPic.indexOf(',') > -1) {
                        item.bannerPic = item.bannerPic.split(',')[0]
                    }
                })
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addSplm()
            }
        },
        addSplm() {
            this.$router.push({ path: '/wel/mallHome/shangChao/banner/addBanner', query: { areaType: this.$route.query.areaType } })
        },
        del(row) {
            var _this = this
            this.$confirm('是否确认删除banner名称"' + row.bannerName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delMallfrontpageconfig(row.id)
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('删除成功')
            }).catch(function () {
            })
        },
        edit(row) {
            this.$router.push({ path: '/wel/mallHome/shangChao/banner/addBanner', query: { areaType: this.$route.query.areaType, bannerId: row.bannerId } })
        }
    }

} 