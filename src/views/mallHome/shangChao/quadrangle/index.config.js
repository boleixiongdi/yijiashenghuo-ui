export const tableOption = {
    header: false,
    headerAlign: 'center',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    defaultExpandAll: true,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: 'banner名称',
            prop: 'bannerName',

        },
        {
            label: 'banner图片',
            prop: 'bannerPic',
            slot: true
        },
        {
            label: '跳转方式',
            prop: 'skipType',
            dicData: [{
                label: '图文页',
                value: '00'
            }, {
                label: '商品列表',
                value: '01'
            }]
        },
        {
            label: '生效时间',
            prop: 'begainTime',
            slot: true

        },
        {
            label: '状态',
            prop: 'status',
            dicData: [{
                label: '生效',
                value: '00'
            }, {
                label: '商品列表',
                value: '失效'
            }]
        },
    ]
}

