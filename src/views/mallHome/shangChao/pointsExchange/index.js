import cropperModal from "@/components/cropper/index.vue";
import { upLoad } from '@/api/shop/upLoad.js'
import { mallfrontpageinfoUpdatePic, putMallfrontpageinfo, getMallfrontpageinfo } from "@/api/shop/mallHome/shangChao/list.js";

export default {
    components: {
        cropperModal
    },
    data() {
        return {
            labelWidth: "150px",
            disabled: false,
            ruleForm: {

            },
            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false,
            imgUrlList: [],

            rejectedreasonList: [],
            loadingSure: false

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            getMallfrontpageinfo(this.$route.query.id).then(val => {
                this.ruleForm = val.data.data

                // let pic = []            //图片回显（11/10更改）
                // if (this.ruleForm.picUrl) {
                //     if (this.ruleForm.picUrl.indexOf(',') > -1) {
                //         pic = this.ruleForm.picUrl.split(',')
                //         pic.forEach(item => {
                //             this.fileList.push({ url: process.url + item, name: '商品图片' })
                //             this.imgUrlList.push({ url: item, name: '商品图片' })
                //         })
                //     } else {
                //         this.fileList.push({ url: process.url + this.ruleForm.picUrl, name: '商品图片' })
                //         this.imgUrlList.push({ url: this.ruleForm.picUrl, name: '商品图片' })
                //     }
                // } else {
                //     this.ruleForm.picUrl = ""
                // }
            })
        },
        // submitSave() {   //提交并保存
        //     let urlList = [...this.imgUrlList]
        //     if (urlList.length) {  //图片处理
        //         let imgList = []
        //         urlList.forEach(item => {
        //             if (item.url.indexOf(process.url) > -1) {  //截取路径
        //                 item.url = item.url.replace(process.url, '')
        //             }
        //             imgList.push(item.url)
        //         })
        //         this.ruleForm.picUrl = imgList.join(',')
        //     }
        //     this.$refs['ruleForm'].validate((valid) => {
        //         if (valid) {
        //             if (this.ruleForm.picUrl) {
        //                 this.loadingSure = true
        //                 // if (this.ruleForm.id) {
        //                 //     mallfrontpageinfoUpdatePic(Object.assign({}, this.ruleForm, obj)).then(() => {
        //                 //         this.quxiao('ruleForm')
        //                 //     }).catch(() => {
        //                 //         this.loadingSure = false
        //                 //     })
        //                 // } else {
        //                 //     putMallfrontpageinfo(Object.assign({}, this.ruleForm, obj)).then(() => {
        //                 //         this.quxiao('ruleForm')
        //                 //     }).catch(() => {
        //                 //         this.loadingSure = false
        //                 //     })
        //                 // }

        //                 mallfrontpageinfoUpdatePic(Object.assign({}, this.ruleForm)).then(() => {
        //                     this.quxiao('ruleForm')
        //                 }).catch(() => {
        //                     this.loadingSure = false
        //                 })
        //             } else {
        //                 this.$message.warning('请上传商品图片')
        //             }
        //         } else {
        //             console.log('error submit!!');
        //             return false;
        //         }
        //     });
        // },
        submitSave() {   //提交并保存（11/10更改）
            mallfrontpageinfoUpdatePic(Object.assign({}, this.ruleForm)).then(() => {
                this.quxiao('ruleForm')
            }).catch(() => {
                this.loadingSure = false
            })
        },

        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
            this.loadingSure = false
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
            this.imgUrlList.splice(index, 1)
        },
        handleCropperSuccess(data) {
            upLoad(data).then(res => {
                this.imgUrlList.push(res.data.data)
                this.fileList.push({
                    uid: this.number++,
                    name: 'image.png',
                    status: 'done',
                    url: URL.createObjectURL(data)
                })
                this.$message.success('上传成功！')
            }).catch(() => {
                this.$message.warning('上传失败，请重新上传！')
            })
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 