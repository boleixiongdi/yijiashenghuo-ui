import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import { mapGetters } from 'vuex'
export default {
    mixins: [search],
    components: {
        allShop,
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "全部",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.allShop.getList()       //全部列表
        },
    }
}