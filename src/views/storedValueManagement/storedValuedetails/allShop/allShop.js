import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { getStoreDetail } from '@/api/shop/storedValueManagement/storedValueRules.js'
export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {

        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page, businessType) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (businessType) {
                this.businessType = businessType
            }
            if (page) {
                this.page.current = page
            }
            getStoreDetail(Object.assign({ businessType: this.businessType }, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                console.log(888, data)
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
    }

} 