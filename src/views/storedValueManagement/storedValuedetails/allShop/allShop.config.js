const DIC = {
    status: [{
        label: '营业中',
        value: '01'
    }, {
        label: '停业',
        value: '02'
    }],
    businessType: [{
        label: "通用",
        value: "ACC001",
    },
    {
        label: "商超",
        value: "ACC003",
    },
    {
        label: "洗衣",
        value: "ACC004",
    },
    {
        label: "家政",
        value: "ACC004",
    },
    {
        label: "咖吧",
        value: "ACC005",
    },],

}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 300,
    menu: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '流水号',
            prop: 'storedDetaileId',
            width:150,
            overHidden: true,
        },
        {
            label: '手机号',
            prop: 'phoneNum',
            width:150,
            overHidden: true,

        },
        {
            label: '储值类别',
            prop: 'accountType',
            dicData: DIC.businessType
        },
        {
            label: '储值变化类型',
            prop: 'operationType',
            width:150,
            dicData: [
                {
                  label: "充值",
                  value: "01",
                },
                {
                  label: "消费",
                  value: "02",
                },
              ],

        },
        {
            label: '储值变化（元）',
            prop: 'operationNum',
            width:150,
            overHidden: true,

        },
        {
            label: '储值规则',
            prop: 'storedRuleName',

            width:150,
            overHidden: true,
        },
        {
            label: '支付方式',
            prop: 'payType',
            dicData: [
                {
                    label: "支付宝",
                    value: "01"
                },
                {
                    label: "微信",
                    value: "02"
                }
            ]


        },
        {
            label: '支付金额（元）',
            prop: 'payAmt',
            width:150,
            overHidden: true,

        },
        {
            label: '渠道类型',
            prop: 'storedSource',
            dicData: [
                {
                    label: "线上",
                    value: "00"
                }
                ,
                {
                    label: "线下",
                    value: "01"
                }
            ],
            width:150,
            overHidden: true,
        },

        {
            label: '门店',
            prop: 'shopName',
            overHidden: true,

        },
        {
            label: '操作人',
            prop: 'operatorId',
            overHidden: true,
        },
        {
            label: '变动时间',
            prop: 'createTime',
            width:150,
            overHidden: true,

        },
    ]
}

