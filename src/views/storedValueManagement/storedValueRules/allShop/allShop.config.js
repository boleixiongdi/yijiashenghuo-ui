const DIC = {
    isenabled: [{
        label: '启用',
        value: '01'
    }, {
        label: '停用',
        value: '00'
    }],
    businessType: [
    //     {
    //     label: '全部业务',
    //     value: '01'
    // }, 
    {
        label: '商超',
        value: '02'
    }, {
        label: '洗衣',
        value: '03'
    }, {
        label: '家政',
        value: '04'
    }, {
        label: '咖吧',
        value: '05'
    }],

}


export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 120,

    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '储值名称',
            prop: 'storedRuleName',
            width:150,
            overHidden: true,

        },
        {
            label: '储值规则说明',
            prop: 'storedRuleExplain',
            width:150,
            overHidden: true,

        },
        {
            label: '储值比例',
            prop: 'storedRuleBl',
            slot:true
        },
        {
            label: '规则有效期',
            prop: 'beginTime1',
            slot: true,
            width:200,
            overHidden: true,
        },
        {
            label: '渠道类型',
            prop: 'storedChannelType',
            dicData: [
                {
                    label: "线上",
                    value: "00"
                }
                ,
                {
                    label: "线下",
                    value: "01"
                }
            ]
        },

        {
            label: '规则状态',
            prop: 'isenabled',
            dicData: DIC.isenabled,

        },
        {
            label: '创建人',
            prop: 'operatorId',
        },
        {
            label: '创建时间',
            prop: 'createTime',
            width:200,
            overHidden: true,
        },
    ]
}

