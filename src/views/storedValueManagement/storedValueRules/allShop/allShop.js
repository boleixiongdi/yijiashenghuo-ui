import { tableOption } from './allShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { storedrule, putObj, addObj, updateStatus } from '@/api/shop/storedValueManagement/storedValueRules.js'
export default {
    name: "allShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {

        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            status: false,
            selectedRows: [],//选中的数组
            xsleList: [
                // {
                //     label: '全部业务',
                //     value: '01'
                // },
                {
                    label: '商超',
                    value: '02'
                }, {
                    label: '洗衣',
                    value: '03'
                }, {
                    label: '家政',
                    value: '04'
                }, {
                    label: '咖吧',
                    value: '05'
                }
            ],
            businessType: '',
            searchForm: {},

            title: 1,
            dialogFormVisible: false,
            loading: false,
            ruleForm: {
                businessType: [],
                ruleTypeList: [

                ]
            },
            rules: {
                storedRuleName: [
                    { required: true, message: '请输入规则名称', trigger: 'blur' },
                ],
                businessType: [
                    { type: 'array', required: true, message: '请至少选择一个涉及业态', trigger: 'change' }
                ],
                storedChannelType: [
                    { required: true, message: '请选择渠道类型', trigger: 'change' }
                ],
                isenabled: [
                    { required: true, message: '请选择状态', trigger: 'change' }
                ],
                storedRuleRecharge: [
                    { required: true, message: '请输入充值金额', trigger: 'blur' },
                ],
                storedRuleProportion: [
                    { required: true, message: '请输入赠送金额', trigger: 'blur' },
                ]
            },
            qdType: [
                {
                    label: '线上',
                    value: '00'
                }, {
                    label: '线下',
                    value: '01'
                }
            ],
            gzyxqList: [  //规则有效期
                {
                    label: "永久",
                    value: 0
                },
                {
                    label: "自定义时间",
                    value: 1
                }
            ],
            gzyxqShow: false,
            searchForm: {}
        }
    },
    filters: {
        businessType(val) {
            let obj = ''
            switch (val) {
                case '01':
                    obj = '通用'
                    break;
                case '02':
                    obj = '商超'
                    break;
                case '03':
                    obj = '洗衣'
                    break;
                case '04':
                    obj = '家政'
                    break;
                case '05':
                    obj = '咖吧'
                    break;
            }
            return obj
        }

    },
    methods: {
        getList(searchForm, page, businessType) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (businessType) {
                this.businessType = businessType
            }
            if (page) {
                this.page.current = page
            }
            storedrule(Object.assign({ businessType: this.businessType }, this.searchForm, this.page)).then(val => {
                let data = val.data.data
                console.log(888, data)
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                // console.log(this.loadData)
                // this.loadData.forEach(item => {
                //     item.businessType3 = []
                //     item.businessType2 = item.businessType.split(',')
                //     item.businessType2.forEach(i => {
                //         this.xsleList.forEach(m => {
                //             if (i == m.value) {
                //                 item.businessType3.push(m.label)

                //             }
                //         })
                //     })
                //     item.businessType1 = item.businessType3.join(',')
                // })
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        edit(item) {
            this.dialogFormVisible = true
            this.title = 0
            this.ruleForm = item
            if (this.ruleForm.beginTime || this.ruleForm.endTime) {  //规则有效期
                this.ruleForm.gzyxq = 1
                this.gzyxqShow = true
                this.ruleForm.gzyxqsj = [this.ruleForm.beginTime, this.ruleForm.endTime]
            } else {
                this.ruleForm.gzyxq = 0
            }
            console.log(this.ruleForm)
        },
        addLevel() {
            this.dialogFormVisible = true
            this.title = 1
            this.ruleForm = {}
        },
        handleSave() {
            let obj = { ...this.ruleForm }
            console.log(this.gzyxqShow)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loading = true
                    // , status: !this.status ? '00' : '01' 
                    addObj(Object.assign({ businessType: this.businessType }, obj)).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '创建成功',
                            type: 'success',
                            duration: 2000
                        })
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            })
        },
        handleUpdate() {
            let obj = { ...this.ruleForm }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loading = true
                    putObj(Object.assign({ businessType: this.businessType, status: !this.status ? '00' : '01' }, this.ruleForm)).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '修改成功',
                            type: 'success',
                            duration: 2000
                        })
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogFormVisible = false
            this.ruleForm = {}
            this.loading = false
            this.ruleForm.gzyxq = 1
            this.gzyxqShow = false
        },
        gzyxqChange(e) {  //规则有效期
            if (e == 0) {
                this.gzyxqShow = false
            } else {
                this.gzyxqShow = true
            }
        },
        gzyxqsjChange(e) {
            if (e) {
                this.ruleForm.beginTime = this.formDate(e[0]);
                this.ruleForm.endTime = this.formDate(e[1]);
            } else {
                delete this.ruleForm.gzyxqsj
            }
            this.$forceUpdate()
        },
        statusChange(e) {
            console.log(e)
            if (e) {
                updateStatus({ businessType: this.businessType }).then(() => {
                    this.$message.success('开启成功')
                }).catch(() => {
                    this.$message.success('开启失败')
                    this.status = false
                })
            }
        }
        // upDown(row) {
        //     if (row.isenabled == '01') {
        //         this.$confirm(
        //             "确定要启动该规则，是否继续？",
        //             "提示",
        //             {
        //                 confirmButtonText: "确定",
        //                 cancelButtonText: "取消",
        //                 type: "warning"
        //             }
        //         )
        //             .then(() => {
        //                 putObj(Object.assign(row, { isenabled: '02' })).then(val => {
        //                     this.getList()
        //                     this.$message({
        //                         type: "success",
        //                         message: "下线成功!"
        //                     });
        //                 })
        //             })
        //             .catch(() => {
        //                 this.$message({
        //                     type: "info",
        //                     message: "已取消"
        //                 });
        //             });
        //     } else {
        //         putObj(Object.assign(row, { isenabled: '01' })).then(val => {
        //             this.getList()
        //             this.$message({
        //                 type: "success",
        //                 message: "上线成功!"
        //             });
        //         })
        //     }
        // }
    }

} 