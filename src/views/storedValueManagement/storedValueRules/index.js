import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import { mapGetters } from 'vuex'
export default {
    mixins: [search],
    components: {
        allShop,
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allShop: "全部",
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allShop1.getList({}, 1, '01') //全部列表
                    break;
                case '2':
                    this.$refs.allShop2.getList({}, 1, '02') //营业中列表
                    break;
                case '3':
                    this.$refs.allShop3.getList({}, 1, '03') //下线中列表
                    break;
                case '4':
                    this.$refs.allShop4.getList({}, 1, '04') //下线中列表
                    break;
                case '5':
                    this.$refs.allShop5.getList({}, 1, '05') //下线中列表
                    break;
                default:
                    break;
            }
        },
        getList() {
            this.$refs.allShop1.getList({}, 1, '01')       //全部列表
            this.$refs.allShop2.getList({}, 1, '02')       //全部列表
            this.$refs.allShop3.getList({}, 1, '03')       //全部列表
            this.$refs.allShop4.getList({}, 1, '04')       //全部列表
            this.$refs.allShop5.getList({}, 1, '05')       //全部列表
        },
    }
}