import { tableOption } from './close.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { shopPurchaseinfoList, putObj, addObj } from '@/api/shop/platformProcurementInventory/storeOrderDetails.js'

export default {
    name: "close",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.current = page
            }
            shopPurchaseinfoList(Object.assign({ status: '03' },searchForm?searchForm:{}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        // 删除
        del() {
            console.log(123321)
        },
        details(item) {
            this.$router.push({ path: '/platformProcurementInventory/storeOrderDetails/orderDetails', query: { purchaseInfoId: item.purchaseInfoId,status:'03' } })
        },
        peidan() {

        }
    }

} 