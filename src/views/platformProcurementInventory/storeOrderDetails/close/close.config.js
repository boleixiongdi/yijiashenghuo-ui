export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '门店采购单号',
            prop: 'purchaseInfoId',
            width:180,
            overHidden: true,
        },
        {
            label: '单据状态',
            prop: 'status',
            dicData: [{
                label: '待配送',
                value: '00'
            }, {
                label: '配送中',
                value: '01'
            }, {
                label: '已完成',
                value: '02'
            }, {
                label: '已取消',
                value: '03'
            }]
        },
        {
            label: '采购总金额(元)',
            prop: 'purchaseInfoAmt',

        },
        {
            label: '采购总数量',
            prop: 'purchaseInfoNum'

        }, {
            label: '采购商品种数',
            prop: 'goodsCateNum'

        }, {
            label: '门店名称',
            prop: 'shopName',
            overHidden: true,

        }, {
            label: '制单人',
            prop: 'operatorId'

        }, {
            label: '制单时间',
            prop: 'createTime',
            overHidden: true,

        }, {
            label: '备注',
            prop: 'remarks',
            overHidden: true,

        },
    ]
}

