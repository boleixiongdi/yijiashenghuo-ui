import search from "./search/search.js";
import allShop from "./allShop/allShop.vue";
import openShop from "./openShop/openShop.vue";
import stopShop from "./stopShop/stopShop.vue";
import end from "./end/end.vue";
import close from "./close/close.vue";
import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";

import { mapGetters } from 'vuex'
const tabName = [
    "待配单",
    "待配送",
    "配送中",
    "已完成",
    "已取消"
]
export default {
    mixins: [search],
    components: {
        allShop,
        openShop,
        stopShop,
        end,
        close,
        comTitle,
        buyingIfor
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.shop_store_add, false),
                editBtn: this.vaildData(this.permissions.shop_store_edit, false),
            }
        }
    },
    data() {
        return {
            keyName: "2",
            tab: {
                allShop: "待配单",
                openShop: "待配送",
                stopShop: "配送中",
                end: "已完成",
                close: "已取消"
            },
            dialogVisible: false,
            loadingSure: false,

            ruleForm: {

            },
            mendian: [],  //选择的门店
            mendianList: [],   //门店list
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            console.log(this.searchForm)
            switch (this.keyName) {
                case '1':
                    // this.$refs.allShop.getList(this.searchForm,1) //待配单列表
                    break;
                case '2':
                    this.$refs.openShop.getList(this.searchForm,1) //待配送列表
                    break;
                case '3':
                    this.$refs.stopShop.getList(this.searchForm,1) //配送中列表
                case '4':
                    this.$refs.end.getList(this.searchForm,1) //已完成列表
                case '5':
                    this.$refs.close.getList(this.searchForm,1) //已取消列表
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            // this.$refs.allShop.getList()       //待配单列表
            this.$refs.openShop.getList(this.searchForm,1)  //待配送列表
            this.$refs.stopShop.getList(this.searchForm,1) //配送中列表
            this.$refs.end.getList(this.searchForm,1) //已完成列表
            this.$refs.close.getList(this.searchForm,1) //已取消列表
        },
        trigger() {
            // this.tab.allShop = `${tabName[0]}(${this.$refs.allShop.page.total})`
            this.tab.openShop = `${tabName[1]}(${this.$refs.openShop.page.total})`
            this.tab.stopShop = `${tabName[2]}(${this.$refs.stopShop.page.total})`
            this.tab.end = `${tabName[3]}(${this.$refs.end.page.total})`
            this.tab.close = `${tabName[4]}(${this.$refs.close.page.total})`
        },
        addMendian() {
            this.dialogVisible = true
            this.mendian = []   //取出门店
            this.mendianList = []
            if (this.keyName == "1") {
                //待配单列表
                this.$refs['allShop'].selectedRows.forEach(i => {
                    this.mendian.push(i.id)
                    this.mendianList.push({ label: i.name, value: i.id })
                })
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //待配送列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //配送中列表
            } else if (this.keyName == "4") {
                this.$refs.end.getList() //已完成列表
            } else if (this.keyName == "5") {
                this.$refs.close.getList() //已取消列表
            }
        },
        drShangpin() {
            if (this.keyName == "1") {
                this.$refs.allShop.getList()       //待配单列表
            } else if (this.keyName == "2") {
                this.$refs.openShop.getList()  //待配送列表
            } else if (this.keyName == "3") {
                this.$refs.stopShop.getList() //配送中列表
            } else if (this.keyName == "4") {
                this.$refs.end.getList() //已完成列表
            } else if (this.keyName == "5") {
                this.$refs.close.getList() //已取消列表
            }
        },
        handlClose() {
            this.dialogVisible = false
        },
        handlSure() {
            console.log('选择门店',this.mendian)
            console.log('门店采购商品明细',this.$refs['buyingIfor'].selectedRows)
        },
        addStore() {  //创建平台采购单
            this.$router.push({ path: "/platformProcurementInventory/platformOrderList/addOrder" })
        },
    }
}