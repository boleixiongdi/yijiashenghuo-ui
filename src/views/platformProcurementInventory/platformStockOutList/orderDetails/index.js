import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./../buyingIfor/buyingIfor.vue";
import cropperModal from "@/components/cropper/index.vue";
import { fileBase64 } from "@/util/util";

export default {
    components: {
        comTitle,
        buyingIfor,
        cropperModal
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                yewu: []
            },
            rules: {
                mendian: [{ required: true, message: '请输入门店名称', trigger: 'blur' }],
                gysdz: [{ required: true, message: '请选择门店区域', trigger: 'change' }],
                guishu: [{ required: true, message: '请选择门店归属', trigger: 'change' }],
                yewu: [{ required: true, message: '请选择所属业务', trigger: 'change' }],
                jingdu: [{ required: true, message: '请输入经度', trigger: 'blur' }],
                weidu: [{ required: true, message: '请输入纬度', trigger: 'blur' }],
            },
            adress: [  //地址
                {
                    value: "zhinan",
                    label: "山东",
                    children: [
                        {
                            value: "yizhi",
                            label: "青岛",
                            children: [
                                {
                                    value: "cexiangdaohang",
                                    label: "崂山"
                                }
                            ]
                        }
                    ]
                }
            ],
            mdguishuList: [ //门店归属
                {
                    value: 0,
                    label: "平台",
                },
                {
                    value: 1,
                    label: "分公司",
                }, {
                    value: 2,
                    label: "代理商",
                }
            ],
            yewuList: [  //业务
                {
                    label: "商超",
                    value: 0
                },
                {
                    label: "咖吧",
                    value: 1
                },
                {
                    label: "洗衣",
                    value: 2
                },
                {
                    label: "家政",
                    value: 3
                }
            ],
            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false
        }
    },
    methods: {
        save() {   //保存
            console.log(this.ruleForm)
            if (this.ruleForm.sptm == 0) {
                this.rules.sptm1[0].required = false; //商品条码
            } else {
                this.rules.sptm1[0].required = true;
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleCropperSuccess(data) {
            //图片裁切完成保存
            fileBase64(data).then(result => {
                this.imageUrl = result.result;
                this.fileList.push({
                    uid: this.number++,
                    name: "image.png",
                    status: "done",
                    url: this.imageUrl
                });
            });
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 