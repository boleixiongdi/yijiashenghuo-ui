export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menuWidth:100,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '退货箱数',
            prop: 'conversionNum',
            slot: true,
            width: 240
        },
        {
            label: '退货数量',
            prop: 'outNum',

        },
        {
            label: '采购价/元/件',
            prop: 'costPrice',


        }, {
            label: '小计/元',
            prop: 'goodSubtotal',
            slot: true,
            width: 240

        },
    ]
}

