export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    menuWidth:100,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            slot: true
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '预计入库量',
            prop: 'outNum',
        },
        {
            label: '已确认入库量',
            prop: 'confirmOutNum',
        },

        {
            label: '待确认入库量',
            prop: 'waitConfirmOutNum',
            // slot:true
        }
    ]
}

