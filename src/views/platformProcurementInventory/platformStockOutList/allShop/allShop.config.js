export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '出库单号',
            prop: 'outStockId',
            width: 180,
            overHidden: true,
        },
        {
            label: '来源类型',
            prop: 'outStockType',
            width: 180,
            overHidden: true,
            dicData: [{
                label: '门店采购-平台出库',
                value: '00'
            }, {
                label: '门店采购-门店退货出库',
                value: '01'
            }, {
                label: '平台主动调拨出库到门店',
                value: '02'
            }, {
                label: '平台采购-平台退货出库',
                value: '03'
            }, {
                label: '平台自建平台退货出库',
                value: '04'
            }, {
                label: '门店自建门店退货出库',
                value: '05'
            }]
        },
        {
            label: '出库状态',
            prop: 'status',
            dicData: [{
                label: '待出库 ',
                value: '00'
            }, {
                label: '部分出库',
                value: '01'
            }, {
                label: '已出库',
                value: '02'
            }, {
                label: '作废',
                value: '03'
            }, {
                label: '已完成',
                value: '04'
            }]


        },
        {
            label: '预计出库量',
            prop: 'outNum',
            width: 150,
            overHidden: true,

        }, {
            label: '已确认出库量',
            prop: 'confirmOutNum',
            width: 150,
            overHidden: true,

        }, {
            label: '待确认出库量',
            prop: 'waitConfirmOutNum',
            width: 150,
            slot:true,
            overHidden: true,

        }, {
            label: '创单人',
            prop: 'operatorId',
            overHidden: true,
        }, {
            label: '门店名称',
            prop: 'shopName',
            overHidden: true,
        },
        {
            label: '创单时间',
            prop: 'createTime',
            overHidden: true,
        },
    ]
}

