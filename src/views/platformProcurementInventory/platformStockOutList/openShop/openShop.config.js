export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '出库单号',
            prop: 'outStockId',
            width: 180,
            overHidden: true,
        },
        {
            label: '出库状态',
            prop: 'status',
            dicData: [{
                label: '待出库 ',
                value: '00'
            }, {
                label: '部分出库',
                value: '01'
            }, {
                label: '已出库',
                value: '02'
            }, {
                label: '作废',
                value: '03'
            }, {
                label: '已完成',
                value: '04'
            }]

        },
        {
            label: '退货原因',
            prop: 'rejectedReason'

        },
        {
            label: '预计出库量',
            prop: 'outNum',
            width: 150,
            overHidden: true,

        }, {
            label: '已确认出库量',
            prop: 'confirmOutNum',
            width: 150,
            overHidden: true,

        }, {
            label: '待确认出库量',
            prop: 'waitConfirmOutNum',
            width: 150,
            overHidden: true,
            slot:true

        }, {
            label: '创单人',
            prop: 'operatorId',
            overHidden: true,
        }, {
            label: '供应商',
            prop: 'companyName',
            overHidden: true,
        },
        {
            label: '创单时间',
            prop: 'createTime',
            overHidden: true,
        },
    ]
}

