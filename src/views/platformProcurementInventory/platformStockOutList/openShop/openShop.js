import { tableOption } from './openShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import {
    platformRefundOutStockList, platformRefundOutStockConfirm
} from "@/api/shop/platformProcurementInventory/platformStockOutList.js";
export default {
    name: "openShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{
                id: 1,
            }],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            daiChuKu: 0,
            selectedRows: [],//选中的数组
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            platformRefundOutStockList(Object.assign({ status: "00" })).then(val => {
                this.daiChuKu = val.data.data.total
                this.$emit('trigger')
            })
            platformRefundOutStockList(Object.assign({}, this.searchForm, this.page)).then(val => {

                let data = val.data.data
                console.log("123123123",data);
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                // this.loadData.forEach(item => {
                //     item.waitConfirmOutNum = parseFloat(item.inNum ? item.inNum : 0) - parseFloat(item.confirmInNum ? item.confirmInNum : 0)
                // })
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        // 删除
        wancheng(item) {
            var _this = this
            this.$confirm('确定要完成出库单号为"' + item.outStockId + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return platformRefundOutStockConfirm({ outStockId: item.outStockId })
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('出库成功')
            }).catch(function () {
            })
        },
        details(item) {
            let obj = {
                outStockId: item.outStockId
            }
            this.$router.push({ path: '/platformProcurementInventory/platformStockOutList/generalWarehouseReturnDetails', query: obj })
        },
        peidan(item) {
            let obj = {
                outStockId: item.outStockId
            }
            this.$router.push({ path: '/platformProcurementInventory/platformStockOutList/generalWarehouseReturnOperation', query: obj })
        },
        addMendian() {
            this.$router.push({ path: "/platformProcurementInventory/platformStockOutList/generalWarehouseReturnAdd" })
        }
    }

} 