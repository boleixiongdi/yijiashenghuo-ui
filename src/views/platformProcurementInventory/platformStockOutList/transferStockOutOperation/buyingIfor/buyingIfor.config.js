export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    menuWidth:100,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            slot: true,
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '总仓库存',
            prop: 'stockNum',
        },
        {
            label: '预计出库量',
            prop: 'outNum',
        },
        {
            label: '已确认出库量',
            prop: 'confirmOutNum',
        },
        {
            label: '待确认出库量',
            prop: 'waitConfirmOutNum',
            // slot:true
        },
        {
            label: '实际出库量',
            prop: 'updateNum',
            slot:true
        }
    ]
}

