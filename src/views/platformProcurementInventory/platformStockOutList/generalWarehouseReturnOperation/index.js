import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";
import cropperModal from "@/components/cropper/index.vue";
import { fileBase64 } from "@/util/util";
import { platformRefundOutStockDetail, platformRefundOutStockOperate } from '@/api/shop/platformProcurementInventory/platformStockOutList.js'

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop,
        cropperModal
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                shopList: [],
            },
            form: {},
            rules: {
                outStockTime: [
                    { required: true, required: true, message: '请选择出库时间', trigger: 'change' },
                ],
                remarks: [
                    { required: true, required: true, message: '请填写备注', trigger: 'change' },
                ]
            },
            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false,
            loadingSure:false
        }
    },
    filters: {
        outStockType(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '采购出库'
                    break;
                case '02':
                    obj = '平台出库'
                    break;
            }
            return obj
        }

    },
    mounted() {
        let obj = this.$route.query
        this.getList(obj)
    },
    methods: {
        getList(obj) {
            platformRefundOutStockDetail(obj).then(val => {
                let data = val.data.data
                console.log(data)
                this.form = data
                this.form.goods.forEach(item => {
                    item.updateNum = 0
                })
            })
        },
        submitSave() {   //提交并保存
            let obj = [...this.$refs['buyingIfor'].loadData]
            console.log(Object.assign({}, this.ruleForm, { goods: obj }))
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (!obj.some(item => {  //采购数量不能大于总仓库存
                        return !(item.updateNum <= item.stockNum)
                    })) {
                        this.loadingSure = true
                        platformRefundOutStockOperate(Object.assign({}, this.ruleForm, { goods: obj }, this.form)).then(() => {
                            this.quxiao('ruleForm')
                        }).catch(()=>{
                            this.loadingSure = false
                        })
                    } else {
                        obj.forEach(item => {
                            if (item.stockNum < item.updateNum) {
                                console.log(item)
                                this.$message.warning('商品名称为“ ' + item.goodsName + ' ”总仓库存不足，请重新确认实际出库量')
                            }
                        })
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
            this.loadingSure = false
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '添加直送门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            this.$refs.buyingIfor.dataList = []
            data.chioseVal.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    mendian: i
                })
            });
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleCropperSuccess(data) {
            //图片裁切完成保存
            fileBase64(data).then(result => {
                this.imageUrl = result.result;
                this.fileList.push({
                    uid: this.number++,
                    name: "image.png",
                    status: "done",
                    url: this.imageUrl
                });
            });
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 