import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";
import cropperModal from "@/components/cropper/index.vue";
import { fileBase64 } from "@/util/util";
import {
    platformToShopOutStockAdd, merchantinfofetchList, rejectedreason
} from "@/api/shop/platformProcurementInventory/platformStockOutList.js";

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop,
        cropperModal
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {

            },
            rules: {
                outStockTime: [
                    { required: true, message: '请选择出库日期', trigger: 'change' },
                ],
                outStockReason: [
                    { required: true, message: '请选择出库原因', trigger: 'change' },
                ]
            },
            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false,
            supplierMerchantIdList: [], //供应商

            shopList: [], //选择的所有门店
            rejectedreasonList: [],

            loadingSure: false
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            merchantinfofetchList().then((val) => {
                //供应商
                console.log(val);
                let data = val.data.data;
                this.supplierMerchantIdList = data.map((item) => {
                    return {
                        value: item.merchantId,
                        label: item.companyName,
                    };
                });
            });
            rejectedreason('00').then(val => {  //出库原因
                console.log(val)
                let data = val.data.data
                this.rejectedreasonList = data.map(item => {
                    return {
                        label: item.inOutReason,
                        value: item.inOutReasonId
                    }
                })

            })
        },
        submitSave() {   //提交并保存
            let obj = [...this.$refs['buyingIfor'].dataList]
            console.log("obj", obj)
            console.log(Object.assign({ shopList: obj }, this.ruleForm))
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (obj[0].goods.length) {  //必须选择门店
                        if (!obj.some(item => {
                            return item.goods.some(ele => {
                                return !(ele.conversionNum <= ele.stockNum)
                            })
                        })) {
                            this.loadingSure = true
                            platformToShopOutStockAdd(Object.assign({ shopList: obj }, this.ruleForm)).then(() => {
                                this.quxiao('ruleForm')
                            }).catch(() => {
                                this.loadingSure = false
                            })
                        } else {
                            obj.forEach(item => {  //判断具体哪个门店下数据超出
                                if (item.goods.some(ele => { //找到具体商品不足的门店
                                    return !(ele.conversionNum <= ele.stockNum)
                                })) {
                                    item.goods.forEach(row => { //对商品不足的门店 具体到哪个商品不足
                                        if (row.stockNum < row.conversionNum) {
                                            this.$message.warning('门店为“ ' + item.shopName + ' ”' + '下商品名称为“ ' + row.goodsName + ' ”总仓库存不足，请重新确认出库箱数')
                                        }
                                    })

                                }
                            })
                        }
                    } else {
                        this.$message.warning("请选择门店商品");
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {
            }
            this.loadingSure = false
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '选择门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            console.log('data.chioseShops',data.chioseShops)
            this.$refs.buyingIfor.dataList = []
            this.$refs.buyingIfor.total = []

            if (data.chioseShops.length == 0) {  //门店为空时  初始化
                data.chioseShops = [{}]
            }

            data.chioseShops.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    shopName: i.shopName,
                    shopId: i.shopId,
                    goods: []
                })
                this.$refs.buyingIfor.total.push({ number: 0, price: 0 })
            });

            console.log('trigger', this.$refs.buyingIfor.total)
            this.shopList = this.$refs.buyingIfor.dataList
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        },
        supplierIdChange(val) {
            this.supplierMerchantIdList.forEach(item => {
                if (item.value == val) {
                    this.ruleForm.supplierName = item.label
                }
            })
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
        },
        handleCropperSuccess(data) {
            //图片裁切完成保存
            fileBase64(data).then(result => {
                this.imageUrl = result.result;
                this.fileList.push({
                    uid: this.number++,
                    name: "image.png",
                    status: "done",
                    url: this.imageUrl
                });
            });
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 