export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menuWidth:100,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '总仓库存',
            prop: 'stockNum',
        },
        {
            label: '出库箱数',
            prop: 'conversionNum',
            slot: true,
            width: 240
        },
        {
            label: '出库数量',
            prop: 'goodsNum',

        },
        {
            label: '采购价/元/件',
            prop: 'costPrice',


        }, {
            label: '小计/元',
            prop: 'goodSubtotal',
            slot: true,
            width: 240

        },
    ]
}

