import { tableOption } from './buyingIfor.config.js'
import { toDecimal2 } from '@/util/util'
import choiceGoods from './../choiceGoods.vue'
export default {
    name: "buyingIfor",
    components: {
        choiceGoods
    },
    // props: {
    //     ShopOrGoods: {
    //         type: Boolean,
    //         default: true
    //     }
    // },
    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            activeNames: '1',
            btn: [
                {
                    title: "选择商品",
                    isShow: true,
                    size: "mini"
                },
                // {
                //     title: "批量导入",
                //     isShow: true,
                //     size: "mini"
                // }
            ],
            total: [{number:0,price:0}],
            dataList: [{ goods: [] }],
            _index1: 0
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.currentPage = page
            }
            setTimeout(() => {
                this.page.total = 100
            }, 500)
        },
        del(row,index) {
            this.dataList[index].goods.splice(row.$index, 1)
            
            // this.total.price -= row.costPrice
            // this.total.number = this.dataList[this._index1].goods.length
            this.onConversionNumChange(1);
        },
        onConversionNumChange:function(e){
            console.log("e",e);
            if (e) {
                this.dataList.forEach(ele=>{
                    ele.goods.forEach(good=>{
                        good.conversionNum = parseInt(good.conversionNum);
                        //计算转换数量
                        good.goodsNum = good.conversionNum*good.convertUnitNum;
                        //计算小计
                        good.goodSubtotal = good.goodsNum*good.costPrice;
                        console.log('good',good)
                    })
                })
                this.dataList = this.dataList
                this.$forceUpdate()
                this.cacaulateSum();
            }
            
        },
        handler(index, index1) {
            if (index == 0) {
                this.choiceGoodsBtn(index1)
            } else if (index == 1) {
                this.plExport()
            }
        },
        choiceGoodsBtn(index1) {   //index1  某门店在数组位置
            this._index1 = index1
            this.$refs.choiceGoods[index1].dialogVisibleTile = '选择门店名《 ' + this.dataList[index1].shopName + ' 》的商品'
            if (this.dataList[index1].shopId) {
                this.$refs.choiceGoods[index1].dialogVisible = true
                this.$refs.choiceGoods[index1].getList(this.dataList[index1].shopId,index1)
            } else {
                this.$message.warning('请选择门店')
            }

        },
        plExport() {

        },
        trigger(data) {   //选择完商品后触发
            this.dataList[this._index1].goods = []
            console.log(data)
            data.chioseGoods.forEach(i => {
                i.conversionNum = 0
                i.goodSubtotal = 0
                this.dataList[this._index1].goods.push(i)
                // this.total.price += i.costPrice
            });
            
            this.onConversionNumChange(1); 
            this.$forceUpdate()
            this.dataList[this._index1].goods = this.distinct(this.dataList[this._index1].goods, 'goodsId') //去重

            // this.total.number = this.dataList[this._index1].goods.length
            console.log(this.dataList)
        },
        //计算商品数量 和总计金额
        cacaulateSum(){
           this.dataList.forEach((ele,index1)=>{
            var sumAmt = 0
               ele.goods.forEach(good=>{
               sumAmt+=good.goodSubtotal;    
               })
               console.log("index",index1)
            //    this.total.push({price : sumAmt,number : ele.goods.length})
                        this.total[index1].price = sumAmt
                        this.total[index1].number=ele.goods.length
           })
           console.log('cacaulateSum this.total',this.total)
        //    this.total.price = sumAmt
        //    this.total.number   = this.dataList[this._index1].goods.length
   }
    }

} 