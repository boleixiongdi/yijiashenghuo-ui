export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            slot: true
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '预计出库量',
            prop: 'outNum',
        },
        {
            label: '已确认出库量',
            prop: 'confirmOutNum',
        },

        {
            label: '待确认出库量',
            prop: 'waitConfirmOutNum',
            slot:true
        }
    ]
}

