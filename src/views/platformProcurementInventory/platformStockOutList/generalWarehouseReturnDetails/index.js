import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import { platformRefundOutStockDetail } from '@/api/shop/platformProcurementInventory/platformStockOutList.js'

export default {
    components: {
        comTitle,
        buyingIfor,
    },
    data() {
        return {
            status: '01',
            labelWidth: "130px",
            disabled: false,
            ruleForm: {

            },
            rules: {

            },
        }
    },
    filters: {
        outStockType(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '采购出库'
                    break;
                case '02':
                    obj = '平台出库'
                    break;
            }
            return obj
        },
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '待出库'
                    break;
                case '01':
                    obj = '部分出库'
                    break;
                case '02':
                    obj = '已出库'
                    break;
                case '03':
                    obj = '作废'
                    break;
                case '04':
                    obj = '已完成'
                    break;
            }
            return obj
        },

    },
    mounted() {
        let obj = this.$route.query
        this.getList(obj)
    },
    methods: {
        getList(obj) {
            platformRefundOutStockDetail(obj).then(val => {
                let data = val.data.data
                console.log(data)
                this.ruleForm = data
            })
        },
        save() {   //保存
            console.log(this.ruleForm)
            if (this.ruleForm.sptm == 0) {
                this.rules.sptm1[0].required = false; //商品条码
            } else {
                this.rules.sptm1[0].required = true;
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
    },

} 