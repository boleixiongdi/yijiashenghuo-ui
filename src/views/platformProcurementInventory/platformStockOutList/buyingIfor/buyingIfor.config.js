export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu:false,
    selection: true,
    reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'bh',
            slot: true

        },
        {
            label: '库存单位',
            prop: 'dw',
        },
        {
            label: '采购数量',
            prop: 'qy',

        },
        {
            label: '采购箱',
            prop: 'hy',


        },

        {
            label: '采购价/元/件',
            prop: 'fs',


        }, {
            label: '小计/元',
            prop: 'dh',



        },
        {
            label: '总仓库存量',
            prop: 'asdasfs',


        },
        {
            label: '库存状况',
            prop: 'fasasdfs',


        },
    ]
}

