export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
            slot: true,
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        {
            label: '预计入库量',
            prop: 'inNum',
        },
        {
            label: '已确认入库量',
            prop: 'confirmInNum',
        },
        {
            label: '待确认入库量',
            prop: 'stayInNum',
            // slot:true
        },
        {
            label: '实际入库量',
            prop: 'actualInNum',
            slot:true
        }
    ]
}

