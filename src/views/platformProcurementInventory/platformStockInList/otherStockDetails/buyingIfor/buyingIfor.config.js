export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    column: [
        {
            label: '交易流水号',
            prop: 'paySerialNo',

        },
        {
            label: '子订单号',
            prop: 'subOrderId',
        },
        {
            label: '订单来源',
            prop: 'orderSource',
            dicData: [
                {
                    label: '小程序',
                    value: '00'
                }, {
                    label: 'APP',
                    value: '01'
                }, {
                    label: '线下',
                    value: '02'
                },
            ]
        },
        {
            label: '门店',
            prop: 'shopName',
        },
        {
            label: '商品数量',
            prop: 'goodsNum',
        },
        {
            label: '交易时间',
            prop: 'orderTime',
        },
        {
            label: '花费积分',
            prop: 'subOrderAmt',

        }
    ]
}

