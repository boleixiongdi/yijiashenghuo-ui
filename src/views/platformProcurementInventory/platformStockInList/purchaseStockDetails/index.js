import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import { platformPurchaseInStockInfoDetail } from '@/api/shop/platformProcurementInventory/platformStockInList.js'

export default {
    components: {
        comTitle,
        buyingIfor,
    },
    data() {
        return {
            status: '01',
            labelWidth: "130px",
            disabled: false,
            ruleForm: {

            },
            rules: {

            },
        }
    },
    filters: {
        distributionStatus(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '待配送'
                    break;
                case '01':
                    obj = '配送中'
                    break;
                case '02':
                    obj = '已完成'
                    break;
                case '03':
                    obj = '已取消'
                    break;
            }
            return obj
        },
        inStockStatus(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '待入库'
                    break;
                case '01':
                    obj = '部分入库'
                    break;
                case '02':
                    obj = '已入库'
                    break;
            }
            return obj
        },

    },
    mounted() {
        let obj = this.$route.query
        this.getList(obj)
    },
    methods: {
        getList(obj) {
            platformPurchaseInStockInfoDetail(obj).then(val => {
                let data = val.data.data
                this.ruleForm = data.inStockDetailDto
                this.ruleForm.inStockGoodsDetailDto = data.inStockGoodsDetailDto
            })
        },
        save() {   //保存
            console.log(this.ruleForm)
            if (this.ruleForm.sptm == 0) {
                this.rules.sptm1[0].required = false; //商品条码
            } else {
                this.rules.sptm1[0].required = true;
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
    },

} 