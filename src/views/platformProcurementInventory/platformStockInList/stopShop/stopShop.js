import { tableOption } from './stopShop.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
// import {
//     platformInstockList
// } from "@/api/shop/platformProcurementInventory/platformStockInList.js";
import {
    platformInstockList,
    shopPurchaseRefundPlatformInStockConfirm,
    alreadyInstockPlatform
} from "@/api/shop/platformProcurementInventory/platformStockInList.js";
export default {
    name: "stopShop",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{
                id: 1,
            }],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            daiRuKu: 0,
            selectedRows: [],//选中的数组
            searchForm:{}
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            platformInstockList(Object.assign({ type: '02', status: '00' })).then(val => {
                this.daiRuKu = val.data.data.total
                this.$emit('trigger')
            })
            platformInstockList(Object.assign({ type: '02' }, this.searchForm, this.page)).then(val => {

                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                console.log(555, data)
                this.loadData = data.records
                this.loadData.dqrNum = parseFloat(this.loadData.inNum) - parseFloat(this.loadData.confirmInNum)
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        wancheng(item) {
            console.log("item", item)
            let obj = { inStockId: item.inStockId, purchaseInfoId: item.purchaseInfoId, platformInStockGoodsDetailList: item.shopInStockNewAddGoodslist }

            this.$confirm('此操作将完成入库单号为:' + item.inStockId + ', 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                alreadyInstockPlatform(obj).then(result => {
                    console.log(result);
                    this.getList()
                })
            }).catch(() => {
                this.$message({
                    type: 'info',
                    message: '已取消'
                });
            });
        },
        ruku(item) {
            // var _this = this
            // this.$confirm('是否要入库单据编号为"' + item.inStockId + '"的数据项?', '警告', {
            //     confirmButtonText: '确定',
            //     cancelButtonText: '取消',
            //     type: 'warning'
            // }).then(function () {
            //     return shopPurchaseRefundPlatformInStockConfirm(item.inStockId)
            // }).then(() => {
            //     this.getList()
            //     _this.$message.success('入库成功')
            // }).catch(function () {
            // })
            console.log("item", item);
            let obj = { inStockId: item.inStockId, barCode: item.barCode, goodsName: item.goodsName }
            this.$router.push({ path: "/platformProcurementInventory/platformStockInList/goinOrder", query: obj })
        },
        details(item) {
            let obj = { inStockId: item.inStockId, barCode: item.barCode, goodsName: item.goodsName, status: item.status }
            this.$router.push({ path: '/platformProcurementInventory/platformStockInList/otherStockDetails', query: obj })
        },
    }

} 