export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: '单据编号',
            prop: 'inStockId',
            width: 180,
            overHidden: true,
        },
        {
            label: '入库类型',
            prop: 'inStockType',
            width: 180,
            overHidden: true,
            dicData: [{
                label: '采购入库 ',
                value: '00'
            }, {
                label: ' 门店退货入库',
                value: '01'
            }, {
                label: '其他入库',
                value: '02'
            }]

        },
        {
            label: '单据状态',
            prop: 'status',
            dicData: [{
                label: '待配送',
                value: '00'
            }, {
                label: '配送中',
                value: '01'
            }, {
                label: '已完成',
                value: '02'
            }, {
                label: '已取消',
                value: '03'
            }]

        },
        {
            label: '预计入库量',
            prop: 'inNum',
            width: 150,
            overHidden: true,

        }, {
            label: '已确认入库量',
            prop: 'confirmInNum',
            width: 150,
            overHidden: true,

        }, {
            label: '待确认入库量',
            prop: 'dqrNum',
            slot: true,
            width: 150,
            overHidden: true,

        }, {
            label: '创单人',
            prop: 'operatorId',
            overHidden: true,
        }, {
            label: '门店名称',
            prop: 'shopName',
            overHidden: true,
        },
        {
            label: '创单时间',
            prop: 'inStockTime',
            overHidden: true,
        },
    ]
}

