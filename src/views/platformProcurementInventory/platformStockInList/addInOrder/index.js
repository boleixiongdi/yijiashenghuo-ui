import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";
import cropperModal from "@/components/cropper/index.vue";
import { fileBase64 } from "@/util/util";
import {
    platFormInstockGoodsNewAdd,
    rejectedreason
} from "@/api/shop/platformProcurementInventory/platformStockInList.js";
import { upLoad } from '@/api/shop/upLoad.js'

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop,
        cropperModal
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {

            },
            rules: {
                inStockTime: [
                    { required: true, required: true, message: '请选择入库时间', trigger: 'change' },
                ],
                inStockReason: [
                    { required: true, message: '请选择入库原因', trigger: 'change' },
                ],
                // value: [
                //     { required: true, message: '请选择入库类型', trigger: 'change' },
                // ],
            },
            imageUrl: "",
            loading: false, //上传
            optionsData: {
                title: "商品图片", //弹框表头
                img: "", //裁切图片地址
                autoCrop: true, //是否默认生成截图框
                autoCropWidth: 326, //默认生成截图框宽度
                autoCropHeight: 412, //默认生成截图框高度
                fixedBox: false, //固定截图框大小 不允许改变
                thumbnailName: "衣家生活"
            },
            number: 0,
            fileList: [],
            dialogImageUrl: "", //放大图片
            dialogVisibleImg: false,
            imgUrlList: [],

            rejectedreasonList: [],
            loadingSure:false

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            rejectedreason('01').then(val => {  //入库原因
                console.log(val)
                let data = val.data.data
                this.rejectedreasonList = data.map(item => {
                    return {
                        label: item.inOutReason,
                        value: item.inOutReasonId
                    }
                })

            })
        },
        submitSave() {   //提交并保存
            let obj = { ...this.$refs['buyingIfor'].dataList[0] }
            console.log(Object.assign({}, this.ruleForm, obj))

            let urlList = [...this.imgUrlList]
            if (urlList.length) {  //图片处理
                let imgList = []
                urlList.forEach(item => {
                    if (item.url.indexOf(process.url) > -1) {  //截取路径
                        item.url = item.url.replace(process.url, '')
                    }
                    imgList.push(item.url)
                })
                this.ruleForm.inStockCert = imgList.join(',')
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (obj.shopInStockNewAddGoodsDetail.length) {
                        this.loadingSure = true
                        platFormInstockGoodsNewAdd(Object.assign({}, this.ruleForm, obj)).then(() => {
                            this.quxiao('ruleForm')
                        }).catch(()=>{
                            this.loadingSure = false
                        })
                    } else {
                        this.$message.warning('请选择商品')
                    }
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
            this.loadingSure = false
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '添加直送门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            this.$refs.buyingIfor.dataList = []
            data.chioseVal.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    mendian: i
                })
            });
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        },
        handleChange(file) {
            //上传触发
            // console.log(file)
            let { options } = this;
            // console.log(file)
            // fileBase64(file.file.originFileObj).then(result => {
            let target = Object.assign({}, options, {
                img: file.url,
                files: file
            });
            this.$refs["cropperModal"].edit(target);
            // })
        },
        beforeUpload(file) {
            const isJpgOrPng =
                file.type === "image/jpeg" || file.type === "image/png";
            if (!isJpgOrPng) {
                this.$message.error("图片格式不支持！");
            }
            const isLt1M = file.size / 1024 / 1024 < 1;
            if (!isLt1M) {
                this.$message.error("图片大小限制在1MB内!");
            }
            return isJpgOrPng && isLt1M;
        },
        handlePictureCardPreview(file) {
            this.dialogVisible = true;
            this.dialogImageUrl = file.url
        },
        delUplod(file) {
            this.hasFmt = false   //图片验证开启
            const index = this.fileList.indexOf(file);
            const newFileList = this.fileList.slice();
            newFileList.splice(index, 1);
            this.fileList = newFileList;
            this.imgUrlList.splice(index, 1)
        },
        handleCropperSuccess(data) {
            upLoad(data).then(res => {
                this.imgUrlList.push(res.data.data)
                this.fileList.push({
                    uid: this.number++,
                    name: 'image.png',
                    status: 'done',
                    url: URL.createObjectURL(data)
                })
                this.$message.success('上传成功！')
            }).catch(() => {
                this.$message.warning('上传失败，请重新上传！')
            })
            this.$refs.lmtbUpload.clearValidate(); // 关闭图片校验
            this.hasFmt = true   //图片验证开启
        },
    },

} 