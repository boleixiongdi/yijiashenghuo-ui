import { tableOption } from './buyingIfor.config.js'
import { toDecimal2 } from '@/util/util'
import { platformRefundInStockInfoDetail } from '@/api/shop/platformProcurementInventory/platformStockInList.js'

export default {
    name: "buyingIfor",
    components: {

    },
    props: {
        loadData: {
            type: Array,
            // default: function () { return [] }
            default: () => []
        },
    },
    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            activeNames: '1',
            btn: [
                // {
                //     title: "选择商品",
                //     isShow: true,
                //     size: "mini"
                // },
                // {
                //     title: "批量导入",
                //     isShow: true,
                //     size: "mini"
                // }
            ],
            total: {
                number: 0, //商品数量
                price: 0 //总价格
            },
            dataList: [{}],
            _index1: 0,
            selectedRows: []
        }
    },
    methods: {
        del() {

        },
        handler(index, index1) {
            if (index == 0) {
                this.choiceGoodsBtn(index1)
            } else if (index == 1) {
                this.plExport()
            }
        },
        choiceGoodsBtn(index1) {   //index1  某门店在数组位置
            this._index1 = index1
            this.$refs.choiceGoods[index1].dialogVisibleTile = '选择 ' + this.$parent.$parent.model.gysmc + ' 商品'
            this.$refs.choiceGoods[index1].dialogVisible = true
        },
        plExport() {

        },
        trigger(data) {   //选择完商品后触发
            this.dataList[this._index1].loadData = new Array()
            data.chioseVal.forEach(i => {
                this.dataList[this._index1].loadData.push({
                    bh: i
                })
            });
            this.$forceUpdate()
            console.log(this.dataList)
        },
        selectionChange(list) {
            this.selectedRows = list
        },
    }

} 