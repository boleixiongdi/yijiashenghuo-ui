export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width: 80
        },
        {
            label: '入库单号',
            prop: 'inStockId',
            width: 180,
            overHidden: true,
        },
        {
            label: '入库类型',
            prop: 'inStockType',
            width: 180,
            overHidden: true,
            dicData: [{
                label: '门店采购门店入库 ',
                value: '00'
            }, {
                label: '门店采购-门店退货平台入库',
                value: '01'
            }, {
                label: '平台采购平台入库',
                value: '02'
            }, {
                label: '门店主动退货-平台入库',
                value: '03'
            }, {
                label: '门店自建入库',
                value: '04'
            }, {
                label: '平台自建入库',
                value: '05'
            }, {
                label: '平台主动出库到门店-门店入库',
                value: '06'
            }, {
                label: '平台主动出库到门店-门店部分入库-平台入库',
                value: '07'
            }]
        },
        {
            label: '入库状态',
            prop: 'status',
            dicData: [{
                label: '待入库',
                value: '00'
            }, {
                label: '部分入库',
                value: '01'
            }, {
                label: '已完成',
                value: '02'
            }]
        },
        {
            label: '配送状态',
            width: 110,
            overHidden: true,
            prop: 'distributionStatus',
            dicData: [{
                label: '待配送',
                value: '00'
            }, {
                label: '配送中',
                value: '01'
            }, {
                label: '配送完成',
                value: '02'
            }, {
                label: '平台采购作废',
                value: '03'
            }]
        },
        {
            label: '预计入库量',
            prop: 'inNum',
            width: 150,
            overHidden: true,


        }, {
            label: '已确认入库量',
            prop: 'confirmInNum',
            width: 150,
            overHidden: true,

        }, {
            label: '待确认入库量',
            prop: 'dqrNum',
            slot: true,
            width: 150,
            overHidden: true,
        }, {
            label: '创单人',
            prop: 'operatorId',
            overHidden: true,
        }, {
            label: '供应商',
            prop: 'companyName',
            overHidden: true,
        },
        {
            label: '创单时间',
            prop: 'createTime',
            overHidden: true,
        },
    ]
}

