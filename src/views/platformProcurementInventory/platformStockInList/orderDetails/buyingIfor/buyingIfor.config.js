export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menu: false,
    // selection: true,
    // reserveSelection: false,
    column: [
        {
            label: '商品名称',
            prop: 'bh',
            slot: true

        },
        {
            label: '库存单位',
            prop: 'dw',
        },
        {
            label: '预计入库量',
            prop: 'qy',
        },
        {
            label: '已确认入库量',
            prop: 'hy',
        },

        {
            label: '待确认入库量',
            prop: 'fs',

        }
    ]
}

