import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import { platformgoodscategorytree } from '@/api/shop/platformProcurementInventory/platformStockInList.js'

export default {
    components: {
        comTitle,
        buyingIfor,
    },
    data() {
        return {
            status: true,
            labelWidth: "130px",
            disabled: false,
            ruleForm: {
                yewu: []
            },
            rules: {

            },
        }
    },
    mounted() {

    },
    methods: {
        getList() {

        },
        save() {   //保存
            console.log(this.ruleForm)
            if (this.ruleForm.sptm == 0) {
                this.rules.sptm1[0].required = false; //商品条码
            } else {
                this.rules.sptm1[0].required = true;
            }
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    alert('submit!');
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
    },

} 