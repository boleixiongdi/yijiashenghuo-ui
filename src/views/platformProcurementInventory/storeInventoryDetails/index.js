import search from "./search/search.js";
import listTable from "./list_table/list_table.vue";
import { shopTree } from '@/api/shop/platformProcurementInventory/storeInventoryDetails.js'


export default {
    mixins: [search],
    components: {
        listTable,
    },
    data() {
        return {
            searchInfo: {},
            data: [],
            filterText:'',
            defaultProps: {
                children: 'children',
                label: 'companyName',
                value:'shopId'
            }
        }
    },
    watch: {
        filterText(val) {
            this.$refs.tree.filter(val);
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.$refs.listTable.getList({}, 1, {})       //会员列表
            shopTree({ shopId: '' }).then(val => {
                console.log('shopTree', val)
                this.data = val.data.data
            })
        },
        handleNodeClick(data) {
            console.log('handleNodeClick', data);
            // this.$refs.listTable.shopTree = data
            this.$refs.listTable.getList(undefined,1,{shopId:data.shopId})
        },
        filterNode(value, data) {
            console.log(data)
            if (!value) return true;
            return data.companyName.indexOf(value) !== -1;
        }
    }
}