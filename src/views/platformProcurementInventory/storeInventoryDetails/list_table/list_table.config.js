export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    selection: false,
    reserveSelection: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            solt: true,
            width:150,
            overHidden: true,
        },
        {
            label: '库存单位',
            prop: 'stockUnit',

        },
        {
            label: '商品类目',
            prop: 'categoryName',
            overHidden: true,

        },
        {
            label: '实物库存',
            prop: 'stockNum',

        },
        {
            label: '占用库存',
            prop: 'freezeStockNum',


        }, {
            label: '可用库存',
            prop: 'availableStock',
        }, {
            label: '采购待入库存',
            prop: 'purchaseInNum',
            width:150,

        }, {
            label: '采购价/元',
            prop: 'costPrice',

        }, {
            label: '总采购金额/元',
            prop: 'purchaseGoodsAmt',
            width:150,

        },
    ]
}



export const tableOption1 = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menu: false,
    column: [
        {
            label: '关联单号',
            prop: 'goodsStockId',
        },
        {
            label: '创建时间',
            prop: 'operationTime',

        },
        {
            label: '单据类型',
            prop: 'billStatus',
            dicData: [
                {
                    label: '采购入库',
                    value: '00'
                },
                {
                    label: '采购退货入库',
                    value: '01'
                },
                {
                    label: '配送出库',
                    value: '02'
                }, {
                    label: '门店退货入库',
                    value: '03'
                },
                {
                    label: '门店销售',
                    value: '04'
                }, {
                    label: '门店退货',
                    value: '05'
                }, {
                    label: '门店盘盈入库',
                    value: '06'
                }, {
                    label: '门店盘亏出库',
                    value: '07'
                }, {
                    label: '平台推送到门店入库',
                    value: '08'
                }

            ]
        },
        {
            label: '出库量',
            prop: 'operatioNum',

        },
        {
            label: '剩余量',
            prop: 'stockNum',
        }, {
            label: '采购单价/元',
            prop: 'costPrice',
        }, {
            label: '操作人',
            prop: 'operatorId',

        }
    ]
}