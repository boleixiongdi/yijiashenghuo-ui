import { tableOption, tableOption1 } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { getGoodsStockRecordPage, getStockRecordDetailList } from '@/api/shop/platformProcurementInventory/storeInventoryDetails.js'

export default {
    name: "list_table",
    mixins: [search],
    components: {
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                // {
                //     title: "新增采购单",
                //     isShow: true
                // },
                // {
                //     title: "导出",
                //     isShow: true
                // }
            ],
            selectedRows: [],  //选中的数组

            dialogVisible: false,   //库存流水
            tableOption1: tableOption1,
            loading1: false,
            loadData1: [],
            searchForm: {},
            shopTree:{}
        }
    },
    methods: {
        getList(searchForm, page,shopTree) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            if (shopTree) {
                this.shopTree = shopTree
            }
            console.log('getGoodsStockRecordPage',this.shopTree)
            getGoodsStockRecordPage(Object.assign({}, this.searchForm,this.shopTree,this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList(undefined,undefined,this.shopTree)
        },
        currentChange(current) {
            this.page.current = current
            this.getList(undefined,undefined,this.shopTree)
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.export()
            }
        },
        kucunLs(item) {  //编辑
            this.dialogVisible = true
            getStockRecordDetailList({ goodsId: item.goodsId, shopId: item.shopId }).then(val => {
                console.log(val)
                let data = val.data.data
                this.loadData1 = data
            })
        },
        export() {

        }
    }

} 