export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    page: false,
    menuWidth:120,
    column: [
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '库存单位',
            prop: 'stockUnit',
        },
        // {
        //     label: '总仓库存',
        //     prop: 'stockNum',

        // },
        {
            label: '采购箱',
            prop: 'convertUnit',
            slot:true,
            width:240
        },
        {
            label: '采购数量',
            prop: 'goodsNum',

        },
        {
            label: '采购价/元/件',
            prop: 'costPrice',


        }, {
            label: '小计/元',
            prop: 'goodSubtotal',
            slot:true,
            width:240

        },
    ]
}

