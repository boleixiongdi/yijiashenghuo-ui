import { tableOption } from './buyingIfor.config.js'
import { toDecimal2 } from '@/util/util'
import choiceGoods from './../choiceGoods.vue'
export default {
    name: "buyingIfor",
    components: {
        choiceGoods
    },
    props: {
        ShopOrGoods: {
            type: Boolean,
            default: true
        }
    },
    computed: {
        price: function () {
            return '¥' + toDecimal2(this.total.price)  //总价格
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            activeNames: '1',
            btn: [
                {
                    title: "选择商品",
                    isShow: true,
                    size: "mini"
                },
                // {
                //     title: "批量导入",
                //     isShow: true,
                //     size: "mini"
                // }
            ],
            total: {
                number: 0, //商品数量
                price: 0 //总价格
            },
            dataList: [{ shopPurchaseGoodsList: [] }],
            _index1: 0
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
            }
            if (page) {
                this.page.currentPage = page
            }
            setTimeout(() => {
                this.page.total = 100
            }, 500)
        },
        onConversionNumChange: function (e) {
            console.log("e", e);
            if (e) {
                this.dataList.forEach(ele => {
                    ele.shopPurchaseGoodsList.forEach(good => {

                        console.log('good', good)
                        good.conversionNum = parseInt(good.conversionNum);
                        //计算转换数量
                        good.goodsNum = good.conversionNum * good.convertUnitNum;
                        //计算小计
                        good.goodSubtotal = good.goodsNum * good.costPrice;
                    })
                })
                this.dataList = this.dataList
                this.$forceUpdate()
                this.cacaulateSum();
            }
            this.total.number = this.dataList[this._index1].shopPurchaseGoodsList.length
        },
        del(row) {
            this.dataList[this._index1].shopPurchaseGoodsList.splice(row.$index, 1)
            // this.total.price -= row.costPrice
            // this.total.number   = this.dataList[this._index1].shopPurchaseGoodsList.length
            this.onConversionNumChange(1);
        },
        handler(index, index1) {
            if (index == 0) {
                this.choiceGoodsBtn(index1)
            } else if (index == 1) {
                this.plExport()
            }
        },
        choiceGoodsBtn(index1) {   //index1  某门店在数组位置
            this._index1 = index1
            this.$refs.choiceGoods[index1].dialogVisibleTile = '选择供应商名《 ' + this.$parent.$parent.model.supplierName + ' 》的商品'
            if (this.$parent.$parent.model.supplierName) {
                this.$refs.choiceGoods[index1].dialogVisible = true
                this.$refs.choiceGoods[index1].getList(this.$parent.$parent.model.supplierId)
            } else {
                this.$message.warning('请选择供应商')
            }

        },
        plExport() {

        },
        trigger(data) {   //选择完商品后触发
            this.dataList[this._index1].shopPurchaseGoodsList = []
            console.log(data)
            data.chioseGoods.forEach(i => {
                i.conversionNum = 0
                i.goodSubtotal = 0
                this.dataList[this._index1].shopPurchaseGoodsList.push(i)
                // this.total.price += i.costPrice
            });
            this.dataList[0].shopPurchaseGoodsList = this.distinct(this.dataList[0].shopPurchaseGoodsList, 'goodsId') //去重
            this.onConversionNumChange();
            this.$forceUpdate()
            // this.total.number   = this.dataList[this._index1].shopPurchaseGoodsList.length
            // console.log(this.dataList)
        },
        //计算商品数量 和总计金额
        cacaulateSum() {
            var sumAmt = 0
            this.dataList.forEach(ele => {
                ele.shopPurchaseGoodsList.forEach(good => {
                    sumAmt += good.goodSubtotal;
                })
            })
            this.total.price = sumAmt
        }
    }

} 