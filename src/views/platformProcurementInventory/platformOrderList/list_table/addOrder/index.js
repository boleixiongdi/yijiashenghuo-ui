import comTitle from "@/views/com/com_title.vue";
import buyingIfor from "./buyingIfor/buyingIfor.vue";
import choiceShop from "./choiceShop.vue";
import { merchantinfofetchList, savePlatformPurchaseinfo } from "@/api/shop/platformProcurementInventory/platformOrderList.js";

export default {
    components: {
        comTitle,
        buyingIfor,
        choiceShop
    },
    data() {
        return {
            labelWidth: "120px",
            disabled: false,
            ruleForm: {
                lx: 1,
            },
            rules: {
                supplierId: [
                    { required: true, message: '请选择供应商', trigger: 'change' },
                ],
                beginTime: [
                    { required: true, message: '请选择配送日期', trigger: 'change' },
                ],
            },
            supplierMerchantIdList: [                //供应商名称

            ],

            mendianList: [  //门店list

            ],
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            merchantinfofetchList().then(val => {
                console.log(val)
                let data = val.data.data
                this.supplierMerchantIdList = data.map(item => {
                    return {
                        value: item.merchantId,
                        label: item.companyName,
                    }
                })
            })
        },
        submitSave() {   //提交并保存
            let obj = { ...this.$refs['buyingIfor'].dataList[0] }
            console.log(Object.assign({}, this.ruleForm, obj))
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (obj.shopPurchaseGoodsList.length) {
                        // if (!obj.shopPurchaseGoodsList.some(item => {  //采购数量不能大于总仓库存
                        //     return !(item.goodsNum <= item.stockNum)
                        // })) {
                        //     savePlatformPurchaseinfo(Object.assign({}, this.ruleForm, obj)).then(() => {
                        //         this.quxiao('ruleForm')
                        //     })
                        // } else {
                        //     obj.shopPurchaseGoodsList.forEach(item => {
                        //         console.log(item)
                        //         if (item.stockNum < item.goodsNum) {
                        //             this.$message.warning('商品名称为“ ' + item.goodsName + ' ”库存不足，请重新输入采购箱数量')
                        //         }
                        //     })
                        // }
                        savePlatformPurchaseinfo(Object.assign({}, this.ruleForm, obj)).then(() => {
                            this.quxiao('ruleForm')
                        })
                    } else {
                        this.$message.warning('请选择商品')
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        quxiao(ruleForm) {  //取消
            this.$router.go(-1)
            this.$refs[ruleForm].resetFields();
            this.ruleForm = {

            }
        },
        addStore() {
            this.$refs.choiceShop.dialogVisibleTile = '添加直送门店'
            this.$refs.choiceShop.dialogVisible = true
        },
        trigger(data) {
            this.$refs.buyingIfor.dataList = []
            data.chioseVal.forEach(i => {
                this.$refs.buyingIfor.dataList.push({   //将门店push 到采购信息中去
                    mendian: i
                })
            });
        },
        pslxChange(e) {           //总仓还是门店
            this.$refs.buyingIfor.dataList = []
            if (e == 1) {
                this.$refs.buyingIfor.dataList.push({})
            } else {
                this.$refs.buyingIfor.dataList = []
            }
        },
        supplierIdChange(val) {
            this.$refs.buyingIfor.dataList = [{ shopPurchaseGoodsList: [] }]
            this.supplierMerchantIdList.forEach(item => {
                if (item.value == val) {
                    this.ruleForm.supplierName = item.label
                }
            })
        }
    },

} 