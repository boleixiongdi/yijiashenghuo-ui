export const tableOption = {
    // header: false,
    refreshBtn: false,
    excelBtn: true,
    addBtn: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '平台采购单',
            prop: 'purchaseInfoId',
            width:190,
            overHidden: true,
        },
        {
            label: '单据状态',
            prop: 'status',
            dicData: [{
                label: "待配送",
                value: "00",
              },
              {
                label: "配送中",
                value: "01",
              },
              {
                label: "已完成",
                value: "02",
              },
              {
                label: "已取消",
                value: "03",
              },]
        },
        {
            label: '采购总金额(元)',
            prop: 'purchaseInfoAmt',
            width:150,

        },
        {
            label: '采购总数量',
            prop: 'purchaseInfoNum',
            width:150,

        },
        {
            label: '供应商',
            prop: 'companyName',
            overHidden: true,

        }, {
            label: '制单人',
            prop: 'operatorId',
        }, {
            label: '制单时间',
            prop: 'createTime',
            overHidden: true,

        }, {
            label: '供应商配送单',
            prop: 'distributionInfoId',
            width:150,
            overHidden: true,

        }, {
            label: '采购入库单',
            prop: 'inStockId',
            width:150,
            overHidden: true,

        }, {
            label: '备注',
            prop: 'remarks',
            overHidden: true,

        },
    ]
}

