import { tableOption } from './list_table.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'

import {
    platformPurchaseinfoList,
    delObj
} from "@/api/shop/platformProcurementInventory/platformOrderList.js";
export default {
    name: "list_table",
    mixins: [search],
    components: {
    },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            btn: [
                {
                    title: "新增采购单",
                    isShow: true
                },
                // {
                //     title: "导出",
                //     isShow: true
                // }
            ],
            searchForm:{}

        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                console.log(searchForm)
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            console.log('platformPurchaseinfoList',searchForm)
            this.loading = true
            platformPurchaseinfoList(Object.assign({}, this.searchForm, this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.loading = false
            }).catch(() => {
                this.loading = false
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        handler(index) {
            if (index == 0) {
                this.addSupplier()
            }
        },
        del(item) {         // 取消
            var _this = this
            this.$confirm('是否取消采购单号为"' + item.purchaseInfoId + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(item.id)
            }).then(() => {
                this.getList()
                _this.$message.success('取消成功')
            }).catch(function () {
            })
        },
        details(item) {  //详情
            this.$router.push({ path: "/platformProcurementInventory/platformOrderList/orderDetails", query: { purchaseInfoId: item.purchaseInfoId,status:item.status } })
        },
        addSupplier() {  //新增
            this.$router.push({ path: "/platformProcurementInventory/platformOrderList/addOrder" })
        },
    }

} 