import { rejectedreasontchList, putObj, addObj } from '@/api/shop/platformProcurementInventory/returnConfig.js'
export default {
    data() {
        const DIC = {
            status: [{
                label: '禁用',
                value: '00'
            }, {
                label: '启用',
                value: '01'
            }],
            inOutType: [{
                label: '出库',
                value: '00'
            }, {
                label: '入库',
                value: '01'
            }],
        }
        return {
            dialogVisible: false,
            loading: false,
            loadingSure: false,
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            loadData: [],
            tableOption: {
                header: false,
                align: "center",
                editBtn: false,
                delBtn: false,
                menu: true,
                column: [
                    {
                        label: '序号',
                        prop: 'indexLabel',
                        slot: true,
                        width:80
                    },
                    {
                        label: "原因",
                        prop: "inOutReason"
                    },
                    {
                        label: "出入库",
                        prop: "inOutType",
                        dicData: DIC.inOutType,
                    },
                    {
                        label: "状态",
                        prop: "status",
                        dicData: DIC.status,
                    },
                    {
                        label: "创建时间",
                        prop: "createTime",
                    },
                    {
                        label: "备注",
                        prop: "remarks"
                    },

                ]
            },
            rules: {
                inOutReason: [
                    { required: true, message: '请输入原因', trigger: 'blur' },
                ],
                inOutType: [
                    { required: true, message: '请选择原因类型', trigger: 'change' },
                ],
            },
            ruleForm: {

            },
            jflxList: [  //原因类型
                {
                    label: '出库',
                    value: '00'
                }, {
                    label: '入库',
                    value: '01'
                }
            ],
        };
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            rejectedreasontchList(Object.assign({}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        addLevel() {
            this.dialogVisible = true
        },
        switchs(row) {
            const list = { ...row }
            if (list.status == '01') {
                this.$confirm(
                    "是否禁用配置原因？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        putObj(Object.assign(list, { status: '00' })).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "禁用成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                putObj(Object.assign(list, { status: '01' })).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "启用成功!"
                    });
                })
            }
        },
        handlSure() {

            console.log(this.ruleForm)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    this.loadingSure = true
                    addObj(Object.assign({ status: '01' }, this.ruleForm)).then(val => {
                        this.getList()
                        this.handlClose()
                        this.$message({
                            title: '成功',
                            message: '创建成功',
                            type: 'success',
                            duration: 2000
                        })
                    }).catch(() => {
                        this.handlClose()
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            })
        },
        handlClose() {
            this.dialogVisible = false
            this.loadingSure = false
            this.ruleForm = {}
        },

    }
};

