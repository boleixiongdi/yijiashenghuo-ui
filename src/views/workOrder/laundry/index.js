import search from "./search/search.js";
import allOrder from "./allOrder/allOrder.vue";
import waitingOrder from "./waitingOrder/waitingOrder.vue";
import servering from "./servering/servering.vue";

import factoryCollection from "./factoryCollection/factoryCollection.vue";
import raisingClothes from "./raisingClothes/raisingClothes.vue";
import outClothes from "./outClothes/outClothes.vue";
import selfClothes from "./selfClothes/selfClothes.vue";

import successfulTrade from "./successfulTrade/successfulTrade.vue";
import partialReturn from "./partialReturn/partialReturn.vue";
import closeTrade from "./closeTrade/closeTrade.vue";
import returnClosed from "./returnClosed/returnClosed.vue";
import watingReceived from "./watingReceived/watingReceived.vue";

const tabName = [
    "待派单",
    "待取衣",
    "门店收衣",
    "送衣中",
    "已完成",
    "退款审核",
    '退款关闭',
    '待接单',
    "待工厂取衣",
    "洗涤中",
    "待送衣",
    "自提取衣"
]
export default {
    mixins: [search],
    components: {
        allOrder,
        waitingOrder,
        servering,
        factoryCollection,
        raisingClothes,
        outClothes,
        selfClothes,
        successfulTrade,
        partialReturn,
        closeTrade,
        returnClosed,
        watingReceived
    },
    data() {
        return {
            keyName: "1",
            tab: {
                allOrder: "待派单",
                waitingOrder: "待取衣",
                servering: "门店收衣",
                factoryCollection: '待工厂取衣',
                raisingClothes: '洗涤中',
                outClothes: '待送衣',
                selfClothes: '自提取衣',
                successfulTrade: "送衣中",
                partialReturn: "已完成",
                closeTrade: "退款审核",
                returnClosed: "退款关闭",
                watingReceived: "待接单"
            },
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList() //待派单列表
                    break;
                case '2':
                    this.$refs.waitingOrder.getList() //待取衣列表
                    break;
                case '3':
                    this.$refs.servering.getList() //门店收衣列表
                    break;
                case '9':
                    this.$refs.factoryCollection.getList() //待工厂取衣
                    break;
                case '10':
                    this.$refs.raisingClothes.getList() //洗涤中
                    break;
                case '11':
                    this.$refs.outClothes.getList() //送衣中
                    break;
                case '12':
                    this.$refs.selfClothes.getList() //自提取衣
                    break;

                case '4':
                    this.$refs.successfulTrade.getList() //送衣中列表
                    break;
                case '5':
                    this.$refs.partialReturn.getList() //已完成列表
                    break;
                case '6':
                    this.$refs.closeTrade.getList() //退款审核列表
                    break;
                case '7':
                    this.$refs.returnClosed.getList() //退款关闭列表
                    break;
                case '8':
                    this.$refs.watingReceived.getList() //待接单列表
                    break;
                default:
                    break;
            }
            this.trigger()
        },
        getList() {
            this.$refs.allOrder.getList()       //待派单列表
            this.$refs.waitingOrder.getList()  //待取衣列表
            this.$refs.servering.getList() //门店收衣列表
            this.$refs.factoryCollection.getList() //待工厂取衣
            this.$refs.raisingClothes.getList() //洗涤中
            this.$refs.outClothes.getList() //送衣中
            this.$refs.selfClothes.getList() //自提取衣
            this.$refs.successfulTrade.getList() //送衣中列表
            this.$refs.partialReturn.getList() //已完成列表
            this.$refs.closeTrade.getList() //退款审核列表
            this.$refs.returnClosed.getList() //退款关闭列表
            this.$refs.watingReceived.getList() //待接单列表
        },
        trigger(st) {
            if (st) {
                this.getList()
            } else {
                // this.tab.allOrder = `${tabName[0]}(${this.$refs.allOrder.page.total})`
                // this.tab.waitingOrder = `${tabName[1]}(${this.$refs.waitingOrder.page.total})`
                // this.tab.servering = `${tabName[2]}(${this.$refs.servering.page.total})`
                // this.tab.successfulTrade = `${tabName[3]}(${this.$refs.successfulTrade.page.total})`
                // this.tab.partialReturn = `${tabName[4]}(${this.$refs.partialReturn.page.total})`
                // this.tab.closeTrade = `${tabName[5]}(${this.$refs.closeTrade.page.total})`
                // this.tab.returnClosed = `${tabName[6]}(${this.$refs.returnClosed.page.total})`
                // this.tab.watingReceived = `${tabName[7]}(${this.$refs.watingReceived.page.total})`

                // this.tab.factoryCollection = `${tabName[8]}(${this.$refs.factoryCollection.page.total})`
                // this.tab.raisingClothes = `${tabName[9]}(${this.$refs.raisingClothes.page.total})`
                // this.tab.outClothes = `${tabName[9]}(${this.$refs.outClothes.page.total})`
                // this.tab.selfClothes = `${tabName[9]}(${this.$refs.selfClothes.page.total})

                this.tab.allOrder = `${tabName[0]}(${this.$refs.allOrder.page.total})`
                this.tab.waitingOrder = `${tabName[1]}(${this.$refs.waitingOrder.page.total})`
                this.tab.servering = `${tabName[2]}(${this.$refs.servering.page.total})`
                this.tab.successfulTrade = `${tabName[3]}(${this.$refs.successfulTrade.page.total})`
                this.tab.partialReturn = `${tabName[4]}(${this.$refs.partialReturn.page.total})`
                this.tab.closeTrade = `${tabName[5]}(${this.$refs.closeTrade.page.total})`
                this.tab.returnClosed = `${tabName[6]}(${this.$refs.returnClosed.page.total})`
                this.tab.watingReceived = `${tabName[7]}(${this.$refs.watingReceived.page.total})`

                this.tab.factoryCollection = `${tabName[8]}(${this.$refs.factoryCollection.page.total})`
                this.tab.raisingClothes = `${tabName[9]}(${this.$refs.raisingClothes.page.total})`
                this.tab.outClothes = `${tabName[10]}(${this.$refs.outClothes.page.total})`
                this.tab.selfClothes = `${tabName[11]}(${this.$refs.selfClothes.page.total})`


            }
            this.$forceUpdate()
        }
    }
}