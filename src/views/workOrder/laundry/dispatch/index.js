
import sleeping from "./sleeping/sleeping.vue";
import servering from "./servering/servering.vue";
// import { paidanObj } from '@/api/shop/order/housekeeping.js'
export default {
    components: {
        sleeping,
        servering
    },
    data() {
        return {
            dialogVisible: false,
            dialogVisibleTile: "",
            loading: false,
            ruleForm: {},
            keyName: "2",
            tab: [
                {
                    label: "可接单",
                    value: "2"
                }, {
                    label: "停止接单",
                    value: "3"
                }
            ]
        };
    },
    methods: {
        getList(title, data) {
            this.dialogVisible = true
            this.dialogVisibleTile = title
            this.ruleForm = data
            setTimeout(() => {
                this.$refs.servering.getList(this.ruleForm)
                this.$refs.sleeping.getList(this.ruleForm)
            }, 100)

        },
        callback(key) {
            this.keyName = key
            switch (this.keyName) {
                case '1':
                    break;
                case '2':
                    this.$refs.servering.getList(this.ruleForm) //服务中
                    break;
                case '3':
                    this.$refs.sleeping.getList(this.ruleForm) //休息中
                    break;
                default:
                    break;
            }
        },
        handleClose() {
            this.dialogVisible = false
            this.ruleForm = {}
        },
        handleSure() {
            this.loading = true
            this.dialogVisible = false
            // paidanObj(Object.assign({ personId: '313' }, { subOrderId: this.ruleForm.subOrderId })).then(() => {
            //     this.$message.success('派单成功')
            //     this.loading = false
            // }).catch(() => {
            //     this.$message.error('派单失败')
            //     this.loading = false
            // })
        },

    }
};