import { tableOption } from './closeTrade.config.js'
import search from './../search/search.js';
import refund from './../../refund.vue';
import { workorderinfoLaundryPage, workorderList } from '@/api/shop/workorder/laundry.js'


export default {
    name: "closeTrade",
    mixins: [search],
    components: {
        refund
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [{}],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },

            dialogVisible: false,

            rules: {},
            loadingSure: false,
            searchForm: {},


            gdjlloading: false,
            gdjlloadData: [],
            gdjltableOption: {
                header: false,
                align: 'center',
                editBtn: false,
                delBtn: false,
                page: false,
                menu: false,
                column: [
                    {
                        label: '工单操作',
                        prop: 'workorderStatus',
                        minWidth: 120,
                        slot: true
                    },
                    {
                        label: '操作人',
                        prop: 'operatorId',
                        minWidth: 120
                    },
                    {
                        label: '操作时间',
                        prop: 'operateTime',
                        minWidth: 120
                    },
                ]
            },

        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.currentPage = page
            }

            workorderinfoLaundryPage(Object.assign({ subOrderStatus: '09' }, this.searchForm ? this.searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        gongzuojilu(item) {
            this.dialogVisible = true;
            this.gdjlloading = true
            this.gdjlloadData = []

            workorderList({ workOrderInfoId: item.workOrderInfoId }).then(val => {
                let data = val.data.data
                this.gdjlloadData = data
                console.log(data)
                this.gdjlloading = false
            }).catch(() => {
                this.gdjlloading = false
            })
        },
        refund(item) {
            this.$refs.refund.show(item)
        },
        triggerFn() {
            this.$emit("trigger", 1) //1重新加载其他tab请求
        },
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 