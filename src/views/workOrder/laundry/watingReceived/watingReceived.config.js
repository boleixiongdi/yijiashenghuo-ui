const DIC = {
    orderSource: [
        {
            label: "线下",
            value: "02",
        },
        {
            label: "APP",
            value: "01",
        },
        {
            label: "小程序",
            value: "00",
        },
    ],
    deliveryType: [
        {
            label: "无需配送",
            value: "00",
        },
        {
            label: "上门配送",
            value: "01",
        },
        {
            label: "自提",
            value: "02",
        },
    ],
    pickupType: [
        {
            label: "上门取衣",
            value: "01",
        },
        {
            label: "到店",
            value: "02",
        },
    ],
    workorderStatus: [
        {
            label: '待派单',
            value: '00'
        }, {
            label: '待取衣',
            value: '01'
        }, {
            label: '门店收衣',
            value: '02'
        }, {
            label: '待工厂取衣',
            value: '03'
        },
        {
            label: '洗涤中',
            value: '04'
        }, {
            label: '待送衣',
            value: '05'
        }, {
            label: '自提取衣',
            value: '06'
        }, {
            label: '送衣中',
            value: '07'
        }, {
            label: '已完成',
            value: '08'
        }, {
            label: '退款审核',
            value: '09'
        }, {
            label: '退款关闭',
            value: '10'
        }, {
            label: '待接单',
            value: '11'
        }
    ]

}
export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 250,
    column: [
        {
            label: '总订单号',
            prop: 'payOrderNo',
            width: 170
        },
        {
            label: '订单号',
            prop: 'subOrderId',
            width: 170
        },
        {
            label: '商品数量',
            prop: 'goodsNum',
            width: 120

        },
        {
            label: '会员',
            prop: 'phoneNum',
            width: 120
        },
        {
            label: '取衣方式',
            prop: 'pickupType',
            width: 120,
            dicData: DIC.pickupType,
        },
        {
            label: '配送方式',
            prop: 'deliveryType',
            width: 120,
            dicData: DIC.deliveryType,
        },
        {
            label: '取衣预约时间',
            prop: 'pickupTime',
            width: 120,
        },
        {
            label: '收货人',
            prop: 'receiver',
            width: 120
        },
        {
            label: '联系电话',
            prop: 'phone',
            width: 120,
        },
        {
            label: '收货地址',
            prop: 'receiveAdress',
            width: 120,
            overHidden: true,
        },
        {
            label: '订单金额（元）',
            prop: 'subOrderAmt',

            width: 120
        }, {
            label: '实付金额（元）',
            prop: 'paySerials',
            slot: true,

            width: 150
        }, {
            label: '订单来源',
            prop: 'orderSource',
            dicData: DIC.orderSource,
            width: 120
        }, {
            label: '门店',
            prop: 'shopName',
            overHidden: true,
            width: 120
        }, {
            label: '创建时间',
            prop: 'createTime',
            width: 120,
            overHidden: true,
        },
        {
            label: '工单状态',
            prop: 'workorderStatus',
            dicData: DIC.workorderStatus,
            width: 120
        },
    ]
}

