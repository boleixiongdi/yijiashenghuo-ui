const DIC = {
    subOrderStatus: [{
        label: '支付成功',
        value: '00'
    }, {
        label: '支付失败',
        value: '01'
    }, {
        label: '待支付',
        value: '02'
    }, {
        label: '支付中',
        value: '03'
    }, {
        label: '作废、关闭',
        value: '04'
    }, {
        label: '退款中',
        value: '05'
    }, {
        label: '退款完成',
        value: '06'
    }, {
        label: '退款失败',
        value: '07'
    }, {
        label: '退款待审核中',
        value: '08'
    }],
    orderSource: [
        {
            label: "线下",
            value: "02",
        },
        {
            label: "APP",
            value: "01",
        },
        {
            label: "小程序",
            value: "00",
        },
    ],
    deliveryType: [
        {
            label: "无需配送",
            value: "00",
        },
        {
            label: "上门配送",
            value: "01",
        },
        {
            label: "自提",
            value: "02",
        },
    ],
    workorderStatus: [
        {
            label: '待派单',
            value: '00'
        }, {
            label: '待自提',
            value: '01'
        }, {
            label: '待取货',
            value: '02'
        }, {
            label: '服务中',
            value: '03'
        },
        {
            label: '已完成',
            value: '04'
        }, {
            label: '退款审核',
            value: '05'
        }, {
            label: '退款关闭',
            value: '06'
        }, {
            label: '待接单',
            value: '07'
        }
    ]

}
export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 250,
    column: [
        {
            label: '总订单号',
            prop: 'payOrderNo',
            width: 170
        },
        {
            label: '订单号',
            prop: 'subOrderId',
            width: 170
        },
        {
            label: '商品数量',
            prop: 'goodsNum',
            width: 120

        },
        {
            label: '会员',
            prop: 'phoneNum',
            width: 120
        },
        {
            label: '配送方式',
            prop: 'deliveryType',
            width: 120,
            dicData: DIC.deliveryType,
        },
        {
            label: '收货人',
            prop: 'receiver',
            width: 120
        },
        {
            label: '联系电话',
            prop: 'phone',
            width: 120,
        },
        {
            label: '收货地址',
            prop: 'receiveAdress',
            width: 120,
            overHidden: true,
        },
        {
            label: '订单金额（元）',
            prop: 'subOrderAmt',

            width: 120
        }, {
            label: '实付金额（元）',
            prop: 'paySerials',
            slot: true,

            width: 150
        }, {
            label: '订单来源',
            prop: 'orderSource',
            dicData: DIC.orderSource,
            width: 120
        }, {
            label: '门店',
            prop: 'shopName',
            overHidden: true,
            width: 120
        }, {
            label: '创建时间',
            prop: 'createTime',
            width: 120,
            overHidden: true,
        },
        {
            label: '支付状态',
            prop: 'subOrderStatus',
            dicData: DIC.subOrderStatus,
            width: 120
        },
        {
            label: '工单状态',
            prop: 'workorderStatus',
            dicData: DIC.workorderStatus,
            width: 120
        },
    ]
}

