import { tableOption } from './waitingOrder.config.js'
import search from './../search/search.js';
import { workorderinfoMallPage, workorderList , workorderinfoMallPickUpWall} from '@/api/shop/workorder/SuperCoffeeBar.js'

export default {
    name: "waitingOrder",
    mixins: [search],
    components: {

    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },

            dialogVisible: false,
            ruleForm: {
                payOrderNo: '',
                paySerials: [],
                refundReason: []
            },
            rules: {},
            loadingSure: false,
            searchForm: {},


            gdjlloading: false,
            gdjlloadData: [],
            gdjltableOption: {
                header: false,
                align: 'center',
                editBtn: false,
                delBtn: false,
                page: false,
                menu: false,
                column: [
                    {
                        label: '工单操作',
                        prop: 'workorderStatus',
                        minWidth: 120,
                        slot:true
                    },
                    {
                        label: '操作人',
                        prop: 'operatorId',
                        minWidth: 120
                    },
                    {
                        label: '操作时间',
                        prop: 'operateTime',
                        minWidth: 120
                    },
                ]
            },
        }
    },
    methods: {
        getList(searchForm, page) {
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.currentPage = page
            }

            workorderinfoMallPage(Object.assign({ workorderStatus: '01' }, this.searchForm ? this.searchForm : {}, this.page)).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
                this.$emit("trigger")
                console.log(this.loadData)
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        ziTi(item) {
            this.$confirm('请核查用户和订单商品。', '用户确认自提', {
                confirmButtonText: '已自提',
                cancelButtonText: '取消',
                type: 'success'
            }).then(() => {
                return workorderinfoMallPickUpWall({subOrderId:item.subOrderId,workorderStatus:'04'})
            }).then(() => {
                this.triggerFn()
                this.$message.success('自提成功')
            }).catch(() => {
            })
        },
        gongzuojilu(item) {
            this.dialogVisible = true;
            this.gdjlloading = true
            this.gdjlloadData = []
            workorderList({ workOrderInfoId: item.workOrderInfoId }).then(val => {
                let data = val.data.data
                this.gdjlloadData = data
                this.gdjlloading = false
            }).catch(() => {
                this.gdjlloading = false
            })
        },
    },
    filters: {
        status(val) {
            let obj = ''
            switch (val) {
                case '00':
                    obj = '支付宝'
                    break;
                case '01':
                    obj = '微信'
                    break;
                case '02':
                    obj = '储值卡'
                    break;
            }
            return obj
        }

    },

} 