import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getSearchList() {
            if (this.keyName == '1') {
                this.$refs.allOrder.getList(this.searchForm, 1)         
            } else if (this.keyName == '2') {
                this.$refs.waitingOrder.getList(this.searchForm, 1)    
            } else if (this.keyName == '3') {
                this.$refs.servering.getList(this.searchForm, 1)        
            } else if (this.keyName == '4') {
                this.$refs.successfulTrade.getList(this.searchForm, 1)  
            } else if (this.keyName == '5') {
                this.$refs.partialReturn.getList(this.searchForm, 1)    
            } else if (this.keyName == '6') {
                this.$refs.closeTrade.getList(this.searchForm, 1)       
            } else if (this.keyName == '7') {
                this.$refs.returnClosed.getList(this.searchForm, 1)       
            } else if (this.keyName == '8') {
                this.$refs.watingReceived.getList(this.searchForm, 1)       
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },

    }

} 