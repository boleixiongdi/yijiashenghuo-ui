import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getSearchList() {
            switch (this.keyName) {
                case '1':
                    this.$refs.allOrder.getList(this.searchForm, 1) //待派单列表
                    break;
                case '2':
                    this.$refs.waitingOrder.getList(this.searchForm, 1) //待服务列表
                    break;
                case '3':
                    break;
                case '4':
                    this.$refs.successfulTrade.getList(this.searchForm, 1) //服务中列表
                    break;
                case '5':
                    this.$refs.partialReturn.getList(this.searchForm, 1) //已完成列表
                    break;
                case '6':
                    this.$refs.closeTrade.getList(this.searchForm, 1) //退款审核列表
                    break;
                case '7':
                    this.$refs.returnClosed.getList(this.searchForm, 1) //退款关闭列表
                    break;
                case '8':
                    this.$refs.watingReceived.getList(this.searchForm, 1) //待接单列表
                    break;
                default:
                    break;
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },

    }

} 