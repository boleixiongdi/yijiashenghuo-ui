const DIC = {
    orderSource: [
        {
            label: "线下",
            value: "02",
        },
        {
            label: "APP",
            value: "01",
        },
        {
            label: "小程序",
            value: "00",
        },
    ],
    workorderStatus: [
        {
            label: '待派单',
            value: '00'
        }, {
            label: '待服务',
            value: '01'
        }, {
            label: '服务中',
            value: '02'
        }, {
            label: '已完成',
            value: '03'
        },
        {
            label: '退款审核',
            value: '04'
        }, {
            label: '退款关闭',
            value: '05'
        }, {
            label: '待接单',
            value: '06'
        }
    ]

}
export const tableOption = {
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    menuWidth: 250,
    column: [
        {
            label: '总订单号',
            prop: 'payOrderNo',
            width: 170
        },
        {
            label: '订单号',
            prop: 'subOrderId',
            width: 170
        },
        {
            label: '商品数量',
            prop: 'goodsNum',
            width: 120

        },
        {
            label: '会员',
            prop: 'phoneNum',
            width: 120
        },
        {
            label: '服务预约时间',
            prop: 'serviceDate',
            width: 120,
        },
        {
            label: '收货人',
            prop: 'receiver',
            width: 120
        },
        {
            label: '联系电话',
            prop: 'phone',
            width: 120,
        },
        {
            label: '收货地址',
            prop: 'receiveAdress',
            width: 120,
            overHidden: true,
        },
        {
            label: '订单金额（元）',
            prop: 'subOrderAmt',

            width: 120
        }, {
            label: '实付金额（元）',
            prop: 'paySerials',
            slot: true,

            width: 150
        }, {
            label: '订单来源',
            prop: 'orderSource',
            dicData: DIC.orderSource,
            width: 120
        }, {
            label: '门店',
            prop: 'shopName',
            overHidden: true,
            width: 120
        }, {
            label: '创建时间',
            prop: 'createTime',
            width: 120,
            overHidden: true,
        },
        {
            label: '工单状态',
            prop: 'workorderStatus',
            dicData: DIC.workorderStatus,
            width: 120
        },
    ]
}

