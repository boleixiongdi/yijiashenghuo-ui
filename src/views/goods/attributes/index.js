import { tableOption } from './index.config.js'
import { mapGetters } from 'vuex'
import { goodsattributefetchList, addObj, updateByIsEnabled, updateById, delObj, putObj } from '@/api/shop/goods/attributes.js'

export default {
    name: "toBeSubmitted",
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                current: 1,
                total: 0,
                size: 10
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增属性",
                    isShow: true
                },
            ],

            title: 1,
            dialogVisible: false,
            loading1: false,
            labelWidth: "120px",
            rules: {
                goodsAttributeName: [{ required: true, message: '请输入属性名称', trigger: 'blur' }]
            },
            ruleForm: {},
            shuXing: [{}],
            disabled: false
        }
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            goodsattributefetchList(Object.assign({},this.page)).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    current: data.current,
                    total: data.total,
                    size: data.size,
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addSplm()
            }
        },
        addKchs(item) {    //库存换算新增
            if (this.disabled) {  //说明这是修改
                this.shuXing.push({ flag: true });
            } else {
                this.shuXing.push({});
            }

            console.log(item)
        },
        editKchs(item) {  //库存换算删除
            let index = this.shuXing.indexOf(item);
            if (index !== -1) {
                this.shuXing.splice(index, 1);
            }
            if (this.disabled) {  //说明这是修改删除
                this.ruleForm.deleteGoodsAttributeValues.push(item.goodsAttributeValue)
            }
        },
        // 新增
        addSplm() {
            this.dialogVisible = true
            this.title = 1
            this.ruleForm = {}
            this.shuXing = [{}]
            this.disabled = false
        },
        // 删除
        del(row) {
            var _this = this
            this.$confirm('是否确认删除属性名称为"' + row.goodsAttributeName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.goodsAttributeId)
            }).then(() => {
                this.getList(this.page)
                _this.$message.success('删除成功')
            }).catch(function () {
            })
        },
        edit(row) {
            this.dialogVisible = true
            this.title = 0
            this.disabled = true
            this.ruleForm = row
            let list
            this.ruleForm.deleteGoodsAttributeValues = []
            list = this.ruleForm.goodsAttributeValues.split(',')
            this.shuXing = []
            list.forEach(item => {
                this.shuXing.push({ goodsAttributeValue: item, disabled: true })
            })
        },
        handleSave() {
            
            let list = []
            this.shuXing.forEach(item => {
                list.push(item.goodsAttributeValue)
            })
            this.ruleForm.goodsAttributeValues = list
            console.log(this.ruleForm.goodsAttributeValues[0] == undefined)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.ruleForm.goodsAttributeValues[0] != undefined) {
                        addObj(Object.assign(this.ruleForm, { operatorId: this.$store.getters.userInfo.ecoUserNo })).then(() => {
                            this.getList()
                            this.dialogVisible = false
                            this.$message({
                                title: '成功',
                                message: '创建成功',
                                type: 'success',
                                duration: 2000
                            })
                        })
                    }
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
            console.log(this.ruleForm)

        },
        handleEdit() {
            this.ruleForm.addDoodsAttributeValues = []
            this.shuXing.forEach(item => {
                if (item.flag) {
                    this.ruleForm.addDoodsAttributeValues.push(item.goodsAttributeValue)
                }
            })
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.ruleForm.addDoodsAttributeValues[0] != undefined) {
                        putObj(this.ruleForm).then(() => {
                            this.getList()
                            this.dialogVisible = false
                            this.$message({
                                title: '成功',
                                message: '修改成功',
                                type: 'success',
                                duration: 2000
                            })
                        })
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });

        },
    }

} 