export const tableOption = {
    header: false,
    headerAlign: 'center',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    defaultExpandAll: true,
    column: [
        // {
        //     label: '属性ID',
        //     prop: 'goodsAttributeId',

        // },
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '属性名称',
            prop: 'goodsAttributeName',

        },
        {
            label: '属性值',
            prop: 'goodsAttributeValues',

        },

    ]
}

