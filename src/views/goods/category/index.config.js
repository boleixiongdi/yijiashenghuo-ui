export const tableOption = {
    header: false,
    headerAlign: 'center',
    menuAlign: 'left',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    index: true,
    indexLabel:"序号",
    defaultExpandAll:true,
    column: [
        {
            label: '类目名称',
            prop: 'categoryName',
            align: 'left',
            width: 200
        },
        {
            label: '图标',
            prop: 'categoryIcon',
            slot: true,
        },
        {
            label: '排序',
            prop: 'sortNum',

        },
        {
            label: '前台显示',
            prop: 'flag',
            slot: true,
        }

    ]
}

