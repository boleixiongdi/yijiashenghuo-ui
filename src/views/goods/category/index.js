import { tableOption } from './index.config.js'
import addCategory from './addCategory/index.vue'
import { mapGetters } from 'vuex'
import { platformgoodscategoryfetchList, delObj, putObj } from '@/api/shop/goods/category.js'
import { handleImg } from '@/api/shop/upLoad.js'

export default {
    name: "toBeSubmitted",
    components: { addCategory },
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                // addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                current: 1,
                total: 0,
                size: 5
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增类目",
                    isShow: true
                },
            ],
            dialogVisibleTile: "新增类目",
            businessType: '02'
        }
    },
    created() {
        this.getList()
    },
    methods: {
        getList() {
            platformgoodscategoryfetchList(Object.assign({ businessType: this.businessType },this.page)).then((val) => {
                let data = val.data.data
                this.page = {
                    current: data.current,
                    total: data.total,
                    size: data.size,
                }
                this.loadData = data.records
                this.loadData.forEach(item => {
                    if (item.children.length) { //判断是否删除
                        item.delFlag = true
                    } else {
                        item.delFlag = false
                    }

                    if (item.showFlag == '01') {  //判断按钮开关
                        item.flag = true
                    } else {
                        item.flag = false
                    }
                    item.children.forEach(items => {
                        if (items.showFlag == '01') {
                            items.flag = true
                        } else {
                            items.flag = false
                        }
                    })

                });
                console.log(this.loadData)
            })

        },
        goBusiness(key) {
            this.businessType = key
            this.getList()
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addSplm()
            }
        },
        // 新增
        addSplm() {
            this.$nextTick(() => {
                this.$refs.addCategory.dialogVisible = true
                this.$refs.addCategory.dialogVisibleTile = '新增类目'
                this.$refs.addCategory.businessType = this.businessType
                this.$refs.addCategory.getCategory({})
            })
        },
        edit(row) {
            this.$nextTick(() => {
                this.$refs.addCategory.dialogVisible = true
                this.$refs.addCategory.dialogVisibleTile = '编辑类目'
                this.$refs.addCategory.businessType = this.businessType
                this.$refs.addCategory.getCategory(row)

               
                
            })
        },
        // 删除
        del(row) {
            console.log(row)
            this.$confirm('是否确认删商品类目除为"' + row.categoryName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                this.$message.success('删除成功')
                this.getList()
            }).catch(() => {
                this.$message.error('删除失败')
            })
        },

        switchChange(val, row) {
            const obj = { ...row }
            if (val) {
                obj.showFlag = '01'
                putObj(obj).then(val => {
                    this.$message.success('前台显示成功')
                    this.getList() 
                })
            } else {
                obj.showFlag = '02'
                putObj(obj).then(val => {
                    this.$message.success('前台隐藏成功')
                    this.getList() 
                })
            }
            // const newData = [...this.loadData]
            // newData.forEach(item => {
            //     if (item.id == row.id) {  //判断按钮开关
            //         item.flag = val
            //     }
            //     item.children.forEach(items => {
            //         if (items.id == row.id) {
            //             items.flag = val
            //         }
            //     })
            // })
            // this.loadData = newData
        },
    }

} 