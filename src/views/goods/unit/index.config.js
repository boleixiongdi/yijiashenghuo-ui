export const tableOption = {
    header: false,
    headerAlign: 'center',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    defaultExpandAll: true,
    column: [
        // {
        //     label: 'ID',
        //     prop: 'stockUnitId',
         
        // },
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '名称',
            prop: 'stockUnitName',
         
        },
    ]
}

