export const tableOption = {
    header: false,
    headerAlign: 'center',
    align: 'center',
    editBtn: false,
    delBtn: false,
    border: true,
    defaultExpandAll:true,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: 'ID',
            prop: 'typeId',
        },
        {
            label: '类型名称',
            prop: 'typeName',
        },
        {
            label: '关联商品属性',
            prop: 'goodsAttributeName',
        },

    ]
}

