import { tableOption } from './index.config.js'
import { mapGetters } from 'vuex'
import { goodstypefetchList, addObj, updateGoodsgoodsTpyeId, delObj, putObj } from '@/api/shop/goods/type.js'
import { goodsattributefetchList } from '@/api/shop/goods/attributes.js'

export default {
    name: "toBeSubmitted",
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增类型",
                    isShow: true
                },
            ],
            title: 1,
            dialogVisible: false,
            loading1: false,
            labelWidth: "120px",
            rules: {
                typeName: [{ required: true, message: '请输入类型名称，不超过15个字', trigger: 'blur' }]
            },
            ruleForm: {
                goodsTypeDetail: []
            },



            loadDataShuXing: [],  //属性新增表格
            goodsAttributeList: [], //渲染属性
            goodsAttributeYuanList: [],//属性data
            tableOptionShuXing: {
                header: false,
                headerAlign: 'center',
                align: 'center',
                editBtn: false,
                delBtn: false,
                border: true,
                index: false,
                defaultExpandAll: false,
                column: [
                    {
                        label: '名称',
                        prop: 'typeName',
                        slot: true
                    },
                    {
                        label: '属性值',
                        prop: 'goodsAttributeValues',
                        slot: true
                    },

                ]
            },
            disabled: false
        }
    },
    created() {
        this.getList()
        goodsattributefetchList().then(val => {
            let data = val.data.data
            this.goodsAttributeYuanList = data.records
            this.goodsAttributeList = data.records.map(item => {
                return {
                    value: item.goodsAttributeId,
                    label: item.goodsAttributeName,
                    disabled: false
                }
            })
        })
    },
    methods: {
        getList() {
            goodstypefetchList(this.page).then(val => {
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addSplm()
            }
        },
        // 新增
        addSplm() {
            this.dialogVisible = true
            this.title = 1
            this.ruleForm = {
                goodsTypeDetail: []
            }
            this.disabled = false
            this.goodsAttributeList.forEach(items => {
                items.disabled = false
            })
            this.tianjia()
        },
        // 删除
        del(row) {
            var _this = this
            this.$confirm('是否确认删除类型名称为"' + row.typeName + '"的数据项?', '警告', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(function () {
                return delObj(row.id)
            }).then(() => {
                _this.getList()
                _this.$message.success('删除成功')
            }).catch(function () {
            })
        },
        edit(row) {
            this.dialogVisible = true
            this.title = 0
            console.log(row)
            this.ruleForm.goodsTypeDetail = []
            this.ruleForm = row
            this.disabled = true
            updateGoodsgoodsTpyeId(row.typeId).then(val => {
                console.log(val)
                let data = val.data.data

                this.ruleForm.goodsTypeDetail = data.map(item => {
                    return {
                        goodsAttributeId: item.goodsAttributeId,
                        goodsAttributeValues: item.goodsAttributeValue,
                        goodsAttributeName: item.goodsAttributeName,
                    }
                })
                this.ruleForm.goodsTypeDetail.forEach((item, index) => {
                    this.goodsAttributeIdChange(item.goodsAttributeId, index)
                })
                console.log(this.ruleForm)
                this.$forceUpdate()
            })
        },
        handleSave() {
            console.log(this.ruleForm)
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.ruleForm.goodsTypeDetail.length) {
                        if (!this.ruleForm.goodsTypeDetail.some(item => {  //判断是否有空属性
                            return item.goodsAttributeId == undefined
                        })) {
                            addObj(Object.assign(this.ruleForm, { operatorId: this.$store.getters.userInfo.ecoUserNo })).then(() => {
                                this.getList()
                                this.$message({
                                    message: '创建成功',
                                    type: 'success',
                                })
                                this.quXiao()
                            })
                        }else{
                            this.$message({
                                message: '请选择属性名称',
                                type: 'warning',
                            })
                        }

                    } else {
                        this.$message({
                            title: '警告',
                            message: '请选择至少一种商品属性',
                            type: 'warning',
                            duration: 2000
                        })
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });


        },
        handleEdit() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    if (this.ruleForm.goodsTypeDetail.length) {

                        if (!this.ruleForm.goodsTypeDetail.some(item => {
                            return item.goodsAttributeId == undefined
                        })) {
                            putObj(Object.assign(this.ruleForm, { operatorId: this.$store.getters.userInfo.ecoUserNo })).then(() => {
                                this.getList()

                                this.$message({

                                    message: '修改成功',
                                    type: 'success',
                                })
                                this.quXiao()
                            })
                        }else{
                            this.$message({
                                message: '请选择属性名称',
                                type: 'warning',
                            })
                        }

                    } else {
                        this.$message({
                            title: '警告',
                            message: '请选择至少一种商品属性',
                            type: 'warning',
                            duration: 2000
                        })
                    }

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        lmmcChange() {

        },
        tianjia() {
            this.ruleForm.goodsTypeDetail.push({ $index: this.ruleForm.goodsTypeDetail.length })
        },
        close(row) {
            this.ruleForm.goodsTypeDetail.splice(row.$index, 1)
            this.goodsAttributeList.forEach(items => {   //移除哪个将哪个放开
                if (row.goodsAttributeId == items.value) {
                    items.disabled = false
                }
            })
        },
        goodsAttributeIdChange(val, idx) {  //关联属性值
            console.log(this.ruleForm.goodsTypeDetail)
            this.goodsAttributeYuanList.forEach(item => {
                if (item.goodsAttributeId == val) {
                    this.ruleForm.goodsTypeDetail.forEach((items, index) => {
                        if (index == idx) {
                            items.goodsAttributeValues = item.goodsAttributeValues
                            items.goodsAttributeName = item.goodsAttributeName
                        }
                    })
                }
            });
            this.ruleForm.goodsTypeDetail.forEach(item => {  //将选择过的禁用掉
                this.goodsAttributeList.forEach(items => {
                    if (item.goodsAttributeId == items.value) {
                        items.disabled = true
                    }
                })
            })
        },
        quXiao() {
            this.dialogVisible = false
            this.ruleForm = {
                goodsTypeDetail: []
            }
            this.disabled = false
            this.goodsAttributeList.forEach(items => {   //移除哪个将哪个放开
                items.disabled = false
            })
        }
    }

} 