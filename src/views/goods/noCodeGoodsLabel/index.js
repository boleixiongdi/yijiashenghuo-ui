import { tableOption } from './index.config.js'
import { mapGetters } from 'vuex'
import { fetchList, addObj, putObj, delObj } from '@/api/shop/goods/noCodeGoodsLabel.js'

export default {
    name: "toBeSubmitted",
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.dal_goodstock_add, false),
                delBtn: this.vaildData(this.permissions.dal_goodstock_del, false),
                editBtn: this.vaildData(this.permissions.dal_goodstock_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "新增标签",
                    isShow: true
                },
            ],

            title: 1,
            dialogVisible: false,
            loading1: false,
            labelWidth: "120px",
            rules: {
                labelName: [{ required: true, message: '请输入单位名称', trigger: 'blur' }]
            },
            ruleForm: {},
        }
    },
    created() {
        this.getList()
    },
    methods: {
        getList() {
            fetchList(this.page).then(val => {
                console.log(val)
                let data = val.data.data
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records
            })

        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.addSplm()
            }
        },
        // 新增
        addSplm() {
            this.dialogVisible = true
            this.title = 1
            this.ruleForm = {}
        },
        // 删除
        del(row) {
            this.$confirm('是否确认删除标签名称为"' + row.labelName + '"的数据项?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                delObj(row.id).then(() => {
                    this.getList()
                    this.$message({
                        message: '删除成功',
                        type: 'success',
                    })
                })
            })
        },
        edit(row) {
            this.dialogVisible = true
            this.title = 0
            this.ruleForm = row
        },
        handleSave() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    addObj(Object.assign(this.ruleForm, { operatorId: this.$store.getters.userInfo.ecoUserNo })).then(() => {
                        this.getList()
                        this.dialogVisible = false
                        this.$message({
                            title: '成功',
                            message: '新增成功',
                            type: 'success',
                            duration: 2000
                        })
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        handleEdit() {
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    putObj(this.ruleForm).then(() => {
                        this.getList()
                        this.dialogVisible = false
                        this.$message({
                            title: '成功',
                            message: '修改成功',
                            type: 'success',
                            duration: 2000
                        })
                    })
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        lmmcChange() {

        },
    }

} 