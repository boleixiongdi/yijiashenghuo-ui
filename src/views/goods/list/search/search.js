import search from "./search.vue";
export default {
    name: "search",
    components: { search },
    data() {
        return {
            searchForm: {},
        }
    },
    methods: {
        getPlatformgoodscategorytree(businessType){
            this.$refs["search"].getList(businessType)
        },
        getSearchList() {
            if (this.keyName == '02') {
                this.$refs.approved.getList(this.searchForm, 1, this.businessType, this.keyName)         //审核通过列表
            } else if (this.keyName == '00') {
                this.$refs.toBeSubmitted.getList(this.searchForm, 1, this.businessType, this.keyName)     //待提交列表
            } else if (this.keyName == '01') {
                this.$refs.toBereviewed.getList(this.searchForm, 1, this.businessType, this.keyName)        //待审核列表
            } else if (this.keyName == '03') {
                this.$refs.rejected.getList(this.searchForm, 1, this.businessType, this.keyName)  //已驳回列表
            }
        },
        search() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearch() {
            this.$refs["search"] && (this.searchForm = this.$refs["search"].getSearchForm());
            this.getSearchList()
        },
        resetSearchTab(){
            this.$refs["search"].resetSearch()
        }
    }

} 