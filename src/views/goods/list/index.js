import search from "./search/search.js";
import approved from "./approved/approved.vue";
import toBeSubmitted from "./toBeSubmitted/toBeSubmitted.vue";
import toBereviewed from "./toBereviewed/toBereviewed.vue";
import rejected from "./rejected/rejected.vue";

const tabName = [
    "审核通过",
    "待提交",
    "待审核",
    "已驳回",
]
export default {
    mixins: [search],
    components: {
        approved,
        toBeSubmitted,
        toBereviewed,
        rejected
    },
    data() {
        return {
            keyName: this.$store.getters.goodsTab,
            tab: {
                approved: "审核通过",
                toBeSubmitted: "待提交",
                toBereviewed: "待审核",
                rejected: "已驳回",
            },
            businessType: this.$store.getters.goodsBusiness,
        }
    },
    mounted() {
        this.getList()
        this.getPlatformgoodscategorytree(this.businessType)//触发搜索类目
    },
    methods: {
        callback() {
            switch (this.keyName) {
                case '02':
                    this.$refs.approved.getList(this.searchForm, 1, this.businessType, this.keyName) //审核通过列表
                    break;
                case '00':
                    this.$refs.toBeSubmitted.getList(this.searchForm, 1, this.businessType, this.keyName) //待提交列表
                    break;
                case '01':
                    this.$refs.toBereviewed.getList(this.searchForm, 1, this.businessType, this.keyName) //待审核列表
                    break;
                case '03':
                    this.$refs.rejected.getList(this.searchForm, 1, this.businessType, this.keyName) //已驳回列表
                    break;
                default:
                    break;
            }
            this.trigger()
            this.$store.commit('SET_GOODSTAB', this.keyName)
        },
        goBusiness(key) {
            this.businessType = key
            if (this.businessType == '02') {
                this.getList()
            } else if (this.businessType == '03') {

                this.getList()
            } else if (this.businessType == '04') {

                this.getList()
            } else if (this.businessType == '05') {

                this.getList()
            }
            this.$store.commit('SET_GOODSBUSINESS', this.businessType)
            this.getPlatformgoodscategorytree(this.businessType)  //触发搜索类目
            this.resetSearchTab()
        },
        getList() {
            this.$refs.approved.getList({}, 1, this.businessType)       //审核通过列表
            this.$refs.toBeSubmitted.getList({}, 1, this.businessType)  //待提交列表
            this.$refs.toBereviewed.getList({}, 1, this.businessType) //待审核列表
            this.$refs.rejected.getList({}, 1, this.businessType) //已驳回列表
        },
        trigger(st) {
            if (st) {
                this.getList()
            } else {
                this.tab.approved = `${tabName[0]}(${this.$refs.approved.page.total})`
                this.tab.toBeSubmitted = `${tabName[1]}(${this.$refs.toBeSubmitted.page.total})`
                this.tab.toBereviewed = `${tabName[2]}(${this.$refs.toBereviewed.page.total})`
                this.tab.rejected = `${tabName[3]}(${this.$refs.rejected.page.total})`
            }
            this.$forceUpdate()
        },
        addShangpin() {
            // if (this.keyName == "1") {
            //     this.$refs.approved.getList()       //审核通过列表
            // } else if (this.keyName == "2") {
            //     this.$refs.toBeSubmitted.getList()  //待提交列表
            // } else if (this.keyName == "3") {
            //     this.$refs.toBereviewed.getList() //待审核列表
            // } else if (this.keyName == "4") {
            //     this.$refs.rejected.getList() //已驳回列表
            // }
            this.$router.push({
                path: '/goods/list/addGoods', query: {
                    businessType: this.businessType
                }
            })
        },
        drShangpin() {
            if (this.keyName == "1") {
                this.$refs.approved.getList()       //审核通过列表
            } else if (this.keyName == "2") {
                this.$refs.toBeSubmitted.getList()  //待提交列表
            } else if (this.keyName == "3") {
                this.$refs.toBereviewed.getList() //待审核列表
            } else if (this.keyName == "4") {
                this.$refs.rejected.getList() //已驳回列表
            }
        }
    }
}