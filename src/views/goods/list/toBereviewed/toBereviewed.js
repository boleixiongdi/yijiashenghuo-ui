import { tableOption } from './toBereviewed.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { platformgoodsinfofetchList, doAudit } from '@/api/shop/goods/list.js'

export default {
    name: "toBereviewed",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                currentPage: 1,
                total: 0,
                pageSize: 10
            },
            selectedRows: [],//选中的数组
            btn: [
                // {
                //     title: "通过",
                //     isShow: true
                // },
                // {
                //     title: "驳回",
                //     isShow: true
                // },
                // {
                //     title: "导出",
                //     isShow: true
                // },
            ],
            businessType: '',
            auditState: '01',
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page, businessType) {
            console.log('待审核', this.auditState)
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            if (businessType) {
                this.businessType = businessType
            }
            // auditState :审核通过  //businessType 业态
            platformgoodsinfofetchList(Object.assign({ businessType: this.businessType, auditState: this.auditState }, this.searchForm, this.page)).then(val => {
                let data = val.data.data;
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records;
                this.loadData.forEach(item => {
                    if (item.goodsPic) {
                        if (item.goodsPic.indexOf(',') > -1) {
                            item.goodsPic = item.goodsPic.split(',')[0]
                            // item.goodsPic.split(',')[0]
                        }
                    }
                })
                this.$emit('trigger')
                console.log(this.loadData);
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.pass()
            } else if (index == 1) {
                this.reject()
            } else if (index == 2) {
                this.export()
            }
        },
        pass(item) {
            this.$confirm('是否通过商品名称为' + item.goodsName, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                doAudit({ id: item.id, auditState: '02' }).then(() => {
                    this.$emit("trigger",1) //1重新加载其他tab请求
                    this.$message({
                        message: '通过成功',
                        type: 'success',
                    })
                })
            })
        },
        rejected(item) {
            this.$prompt('驳回原因', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                inputType: 'textarea',
                inputValidator: (val) => {
                    if (val === null) {
                        return true;//初始化的值为null，不做处理的话，刚打开MessageBox就会校验出错，影响用户体验
                    }
                    return !(val.length == 301)
                },
                inputErrorMessage: '字数限制在300字以内',
                inputPlaceholder: '字数限制在300字以内'
            }).then(({ value }) => {
                doAudit({ id: item.id, auditState: '03', rejectReason: value }).then(() => {
                    this.$emit("trigger",1) //1重新加载其他tab请求
                    this.$message({
                        title: '成功',
                        message: '驳回成功',
                        type: 'success',
                        duration: 2000
                    })
                })

            })
        },
        // 导出
        export() {

        },
        edit(item) {
            this.$router.push({ path: '/goods/list/addGoods', query: Object.assign({ businessType: this.businessType, id: item.id }) })
        },
    }

} 