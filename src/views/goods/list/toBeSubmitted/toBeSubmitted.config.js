
const DIC = {
    vaild: [{
        label: '停售',
        value: '00'
    }, {
        label: '在售',
        value: '01'
    }]
}

export const tableOption = {
    selection: false,
    reserveSelection: false,
    // selectable:(row,index)=>{
    //     return row.ispush!='01';
    //   },
    header: false,
    align: 'center',
    editBtn: false,
    delBtn: false,
    column: [
        {
            label: '序号',
            prop: 'indexLabel',
            slot: true,
            width:80
        },
        {
            label: '商品ID',
            prop: 'goodsId',
            width:150,
            overHidden:true,
        },
        {
            label: '条形码',
            prop: 'barCode',
            overHidden:true,
            width:150,
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            overHidden:true,

        },
        {
            label: '供应商名称',
            prop: 'suplierInfoList',
            slot: true,
            overHidden:true,
            width:150,

        },
        {
            label: '商品图片',
            prop: 'goodsPic',
            slot: true,
            overHidden:true,
        },
        {
            label: '商品类目',
            prop: 'categoryName',
            overHidden:true,

        }, {
            label: '库存单位',
            prop: 'stockUnit',
            overHidden:true,

        }, {
            label: '库存数量',
            prop: 'stockNum',
            overHidden:true,

        }, {
            label: '成本价（元）',
            prop: 'costPrice',
            overHidden:true,
            width:150,

        },
        {
            label: '销售价（元）',
            prop: 'sellingPrice',
            overHidden:true,
            width:150,

        },
        {
            label: '促销价（元）',
            prop: 'promotePrice',
            overHidden:true,
            width:150,

        },
        //  {
        //     label: '商品状态',
        //     prop: 'goodsStatus',

        //     dicData: DIC.vaild,
        //     overHidden:true,
        // },
    ]
}

