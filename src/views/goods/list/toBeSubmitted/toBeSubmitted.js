import { tableOption } from './toBeSubmitted.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { platformgoodsinfofetchList, submitApprove } from '@/api/shop/goods/list.js'
export default {
    name: "toBeSubmitted",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            btn: [
                // {
                //     title: "提交审核",
                //     isShow: true
                // },
                // {
                //     title: "导出",
                //     isShow: true
                // },
            ],
            businessType: '',
            auditState: '00',
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page, businessType) {
            console.log('待提交', this.auditState)
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            if (businessType) {
                this.businessType = businessType
            }

            // auditState :审核通过  //businessType 业态
            platformgoodsinfofetchList(Object.assign({ businessType: this.businessType, auditState: this.auditState }, this.searchForm , this.page)).then(val => {
                let data = val.data.data;
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records;
                this.loadData.forEach(item => {
                    if (item.goodsPic) {
                        if (item.goodsPic.indexOf(',') > -1) {
                            item.goodsPic = item.goodsPic.split(',')[0]
                            // item.goodsPic.split(',')[0]
                        }
                    }
                })
                this.$emit('trigger')
                console.log(this.loadData);
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.submitShenHe()
            } else if (index == 1) {
                this.export()
            }
        },
        // 提交审核
        submitShenHe() {

        },
        // 删除
        del(row) {
            this.$confirm('此操作将永久删除, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                delObj(row.id).then(() => {
                    this.getList()
                    this.$notify({
                        title: '成功',
                        message: '删除成功',
                        type: 'success',
                        duration: 2000
                    })
                })
            })
        },
        // 导出
        export() {

        },
        edit(item) {
            this.$router.push({ path: '/goods/list/addGoods', query: Object.assign({ businessType: this.businessType, id: item.id }) })
        },
        pass(item) {
            this.$confirm('是否提交审核商品名称为' + item.goodsName, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                submitApprove({ id: item.id}).then(() => {
                    this.$emit("trigger",1) //1重新加载其他tab请求
                    this.$message({
                        message: '提交成功',
                        type: 'success',
                    })
                })
            })
        },
    }

} 