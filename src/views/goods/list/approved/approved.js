import { tableOption } from './approved.config.js'
import search from './../search/search.js';
import { mapGetters } from 'vuex'
import { platformgoodsinfofetchList, delObj, changeSaleState, pushToShop } from '@/api/shop/goods/list.js'

export default {
    name: "approved",
    mixins: [search],
    computed: {
        ...mapGetters(['permissions']),
        permissionList() {
            return {
                addBtn: this.vaildData(this.permissions.cms_categoty_add, false),
                delBtn: this.vaildData(this.permissions.cms_categoty_del, false),
                editBtn: this.vaildData(this.permissions.cms_categoty_edit, false)
            }
        }
    },
    data() {
        return {
            tableOption: tableOption,
            loading: false,
            loadData: [],
            page: {
                total: 0,
                size: 10,
                current: 1,
            },
            selectedRows: [],//选中的数组
            btn: [
                {
                    title: "推送到门店",
                    isShow: true
                },
                // {
                //     title: "在售",
                //     isShow: true
                // },
                // {
                //     title: "停售",
                //     isShow: true
                // },
                // {
                //     title: "导出",
                //     isShow: true
                // },
            ],
            businessType: '',
            auditState: '02',
            searchForm: {}
        }
    },
    methods: {
        getList(searchForm, page, businessType) {
            console.log('审核通过', this.auditState)
            if (searchForm) {
                this.searchForm = searchForm
            }
            if (page) {
                this.page.current = page
            }
            if (businessType) {
                this.businessType = businessType
            }
            // auditState :审核通过  //businessType 业态
            platformgoodsinfofetchList(Object.assign({ businessType: this.businessType, auditState: this.auditState }, this.searchForm , this.page)).then(val => {
                let data = val.data.data;
                this.page = {
                    total: data.total,
                    size: data.size,
                    current: data.current
                }
                this.loadData = data.records;
                this.loadData.forEach(item => {
                    if (item.goodsPic) {
                        if (item.goodsPic.indexOf(',') > -1) {
                            item.goodsPic = item.goodsPic.split(',')[0]
                            // item.goodsPic.split(',')[0]
                        }
                    }
                })
                this.$emit('trigger')
                console.log(this.loadData);
            })
        },
        sizeChange(size) {
            this.page.size = size
            this.getList()
        },
        currentChange(current) {
            this.page.current = current
            this.getList()
        },
        selectionChange(list) {
            this.selectedRows = list
        },
        handler(index) {
            if (index == 0) {
                this.publishToStore()
            } else if (index == 1) {
                this.onSale()
            } else if (index == 2) {
                this.stopSale()
            } else if (index == 3) {
                this.export()
            }
        },
        // 发布到门店
        publishToStore() {
            console.log(this.selectedRows)
            if (this.selectedRows.length) {
                let list = []
                this.selectedRows.forEach(item => {
                    list.push(item.id)
                })
                this.$confirm('是否批量推送到门店?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    pushToShop(list).then(() => {
                        this.getList()
                        this.$message({
                            message: '推送成功',
                            type: 'success',
                        })
                    })
                })
            } else {
                this.$message({
                    message: '请选择需要推送的门店',
                    type: 'warning',
                })
            }

        },
        // 在售
        onSale() {

        },
        // 停售
        stopSale() {

        },
        switchs(row) {
            const list = { ...row }
            if (list.goodsStatus == '01') {
                this.$confirm(
                    "确定停售该商品，是否继续？",
                    "提示",
                    {
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        type: "warning"
                    }
                )
                    .then(() => {
                        changeSaleState({ saleState: '00', id: list.id }).then(val => {
                            this.getList()
                            this.$message({
                                type: "success",
                                message: "停售成功!"
                            });
                        })
                    })
                    .catch(() => {
                        this.$message({
                            type: "info",
                            message: "已取消"
                        });
                    });
            } else {
                changeSaleState({ saleState: '01', id: list.id }).then(val => {
                    this.getList()
                    this.$message({
                        type: "success",
                        message: "在售成功!"
                    });
                })
            }
        },
        // 删除
        del(row) {
            this.$confirm('此操作将永久删除, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                delObj(row.id).then(() => {
                    this.getList()
                    this.$message({
                        message: '删除成功',
                        type: 'success',
                    })
                })
            })
        },
        // 导出
        export() {

        },
        edit(item) {
            this.$router.push({ path: '/goods/list/addGoods', query: Object.assign({ businessType: this.businessType, id: item.id }) })
        },
    }

} 