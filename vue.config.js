/**
 * 配置参考:
 * https://cli.vuejs.org/zh/config/
 */
//测试环境端口地址
const url = 'http://47.111.230.4:9999'
//生产环境端口地址
// const url = 'http://47.111.230.4:8989'
// const url = 'http://localhost:9999'
const webpack = require('webpack')
module.exports = {
  lintOnSave: true,
  productionSourceMap: false,
  chainWebpack: config => {
    // 忽略的打包文件
    config.externals({
      'axios': 'axios'
    })
    const entry = config.entry('app')
    entry
      .add('babel-polyfill')
      .end()
    entry
      .add('classlist-polyfill')
      .end()
    config.plugin('provide').use(webpack.ProvidePlugin, [{
      'window.Quill': 'quill'
    }]);
  },
  // 配置转发代理
  devServer: {
    disableHostCheck: true,
    port: 8080,
    proxy: {
      '/': {
        target: url,
        ws: false, // 需要websocket 开启
        pathRewrite: {
          '^/': '/'
        },
        changeOrigin: true
      }, '/auth': {
        target: url,
        changeOrigin: true,     // target是域名的话，需要这个参数，
        ws: true,
        pathRewrite: {
          '^/auth': '/auth'
        }
      },
      '/admin': {
        target: url,
        changeOrigin: true,     // target是域名的话，需要这个参数，
        ws: true,
        pathRewrite: {
          '^/admin': '/admin'
        }
      },
      '/code': {
        target: url,
        changeOrigin: true,     // target是域名的话，需要这个参数，
        ws: true,
        pathRewrite: {
          '^/code': '/code'
        }
      }
      // 3.5 以后不需要再配置
    }
  },
  configureWebpack: {
    externals: {
      "BMap": "BMap"
    }
  },
}
